define(function(require){

var config = require('config')
var {stateChanged, dataChanged} = require('core');
var storage = require('storage');
var {callWeb3, catchWeb3Error} = require('utils/web3');
var {promisedProperties} = require('utils');

var NODES_CATALOG = [
  {
    id: 4,
    name: "ABDK Mainnet",
    url: "https://ethereum.abdkconsulting.com",
    chainId: 1,
  },
  {
    id: 5,
    name: "ABDK Testnet (Ropsten)",
    url: "https://ethereum-testnet.abdkconsulting.com",
    chainId: 3,
  },
  {
    id: 0,
    name: "INFURA Mainnet",
    url: "https://mainnet.infura.io/" + config.INFURA_ACCESS_TOKEN,
    chainId: 1,
  },
  {
    id: 1,
    name: "INFURA Testnet (Ropsten)",
    url: "https://ropsten.infura.io/" + config.INFURA_ACCESS_TOKEN,
    chainId: 3,
  },
  {
    id: 2,
    name: "INFURA Testnet (Rinkeby)",
    url: "https://rinkeby.infura.io/" + config.INFURA_ACCESS_TOKEN,
    chainId: 4,
  },
  {
    id: 3,
    name: "INFURA Testnet (Kovan)",
    url: "https://kovan.infura.io/" + config.INFURA_ACCESS_TOKEN,
    chainId: 42,
  },
]

var knownNetworks = {
  '0xd4e56740f876aef8c010b86a40d5f56745a118d0906a34e69aec8c0db1cb8fa3': {
    type: 'mainnet',
    name: 'Mainnet',
    chainId: 1,
  },

  '0x41941023680923e0fe4d74a34bdac8141f2540e3ae90623718e47d66d1ca4a2d': {
    type: 'testnet',
    name:'Testnet #3 (Ropsten)',
    chainId: 3,
  },

  '0x0cd786a2425d16f152c658316c423e6ce1181e15c3295826d7c9904cba9ce303': {
    type:'testnet',
    name:'Testnet #2 (Morden)',
    chainId: 2,
  },

  '0xa3c565fc15c7478862d50ccd6561e3c06b24cc509bf388941c25ea985ce32cb9': {
    type:'testnet',
    name:'Kovan',
    chainId: 42,
  },
}

function detectNetwork(hash) {
  var network;
  if((network = knownNetworks[hash]) != null){
    return JSON.parse(JSON.stringify(network))
  }else{
    return {
      type: 'privatenet',
      name: 'Private net',
    }
  }
}

function nextNodeInfo(prev, info, time = new Date().getTime()){
  var isNewBlock = info.isOnline 
    && prev.block.number != null 
    && prev.block.number < info.blockNumber
  var block = info.isOnline
    ? {number: info.blockNumber, time: isNewBlock ? time : prev.block.time}
    : prev.block
  return Object.assign(info, {
    block,
    isNewBlock,
  })
}

function NodeInfo(){

  var info = {
    isOnline: false,
    block: {number: null, time: null},
  }

  function doFetchNodeInfo(){
    var getPeerCount = callWeb3(web3.net.getPeerCount);
    var getBlockNumber = callWeb3(web3.eth.getBlockNumber);
    var getSyncing = callWeb3(web3.eth.getSyncing)
    return promisedProperties({
      peerCount: getPeerCount(), 
      blockNumber: getBlockNumber(), 
      syncing: getSyncing(),
      isOnline: true,
    })
    .catch(catchWeb3Error(() => (
      {isOnline: false}
    )))
  }

  function fetchNodeInfo(){
    return doFetchNodeInfo()
    .then((newInfo) => {
      return info = nextNodeInfo(info, newInfo)
    })
  }

  function get(){
    return info
  }

  return {
    get,
    fetchNodeInfo,
  };
}

function NodeInfoUpdater(){

  var intervalId;

  function runUpdatingNodeInfo(){
    var isRequestCompleted = true
    intervalId = setInterval(function(){
      if(sessionStorage.__SKIP_UPDATE_NODE_INFO){
        return
      }
      if(!isRequestCompleted){
        return
      }
      isRequestCompleted = false
      state.nodeInfo.fetchNodeInfo().then(function(info){
        if(info.isNewBlock){
          log('new block arrived', info.block.number)
          dataChanged('newBlock')
        }
        stateChanged('nodeInfo');
        isRequestCompleted = true
      }).catch((e) => {
        isRequestCompleted = true
        throw e
      })
      ;
    }, 3000);
  }


  return {
    runUpdatingNodeInfo,
  };
}

function makeWeb3(url){
  return new Web3(new Web3.providers.HttpProvider(url))
}

function fetchNetworkInfo(url){
  var w3 = makeWeb3(url)
  return fetchNetworkInfoByW3(w3)
}

function fetchNetworkInfoByW3(w3){
  return callWeb3(w3.eth.getBlock)(0).then(function(block){
    var network = Object.assign(
      {firstBlockHash: block.hash}, 
      detectNetwork(block.hash)
    )
    return {isOnline: true, network}
  })
  .catch(catchWeb3Error(() => (
    {isOnline: false}
  )))
}

function initWeb3(){
  var url = loadNodeAddress().url;
  var web3 = makeWeb3(url);
  /* eslint-disable no-empty */
  try{ window.web3 = web3 }catch(e){}
  try{ global.web3 = web3 }catch(e){}
  /* eslint-enable no-empty */
}

function isNodeConfigured(){
  return storage.loadFromMainDB('nodeId') != null ||
    storage.loadFromMainDB('catalogNodeId') != null
}

function loadNodeAddress(){
  var nodeId = storage.loadFromMainDB('nodeId')
  var node
  if(nodeId == null){
    var catalogNodeId = storage.loadFromMainDB('catalogNodeId')
    if(catalogNodeId == null){
      throw new Error('node is not configured')
    }
    node = NODES_CATALOG.find(({id}) => id == catalogNodeId)
  } else {
    var recentNodes = loadRecentNodes()
    node = recentNodes.find(node => node.id === nodeId)
  }
  if(node == null){
    throw new Error('Could not find node')
  }
  return node
}

function loadRecentNodes(){
  return storage.loadFromMainDB('recentNodes') || []
}

function loadNodeConfiguration(){
  var isConfigured = isNodeConfigured();
  if(!isConfigured){
    return {isNodeConfigured: isConfigured}
  }else{
    return {
      isNodeConfigured: isConfigured,
      nodeId: JSON.parse(storage.loadFromMainDB('nodeId')),
      catalogNodeId: JSON.parse(storage.loadFromMainDB('catalogNodeId')),
      recentNodes: loadRecentNodes(),
    }
  }
}

function saveNodeToRecents(node){
  log('save node to recents', node);
  var recent = loadRecentNodes() 
  var recentNode = _.findWhere(recent, {
    url: node.url
  })
  var id
  if(recentNode != null){
    recent = recent.filter(n => n != recentNode)
    id = recentNode.id
  }else{
    id = Math.max(
      0,
      Math.max(...recent.map(node => node.id))
    ) + 1
  }
  recent.unshift({
    id: id,
    url: node.url,
    chainId: node.chainId,
    firstBlockHash: node.firstBlockHash,
    type: node.type,
    name: node.name,
  })
  storage.saveToMainDB({
    nodeId: id,
    recentNodes: recent,
  })
  storage.saveToMainDB('catalogNodeId', null)
}

function saveCatalogNode(catalogNodeId){
  storage.saveToMainDB('catalogNodeId', catalogNodeId)
  storage.saveToMainDB('nodeId', null)
}

function getChainId(){
  return loadNodeAddress().chainId
}

return {
  nextNodeInfo,
  detectNetwork,
  initWeb3,
  NodeInfo,
  NodeInfoUpdater,
  fetchNetworkInfo,
  isNodeConfigured,
  loadNodeAddress,
  loadNodeConfiguration,
  saveNodeToRecents,
  NODES_CATALOG,
  saveCatalogNode,
  getChainId,
}

});
