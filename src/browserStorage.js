define(function(require){

var schema = require('schema')
var {mainDBSchema, chainDBSchema} = schema

var chainId = null

var storage = {
  isInitialized,
  init, 
  initChain, 
  loadFromMainDB, 
  saveToMainDB, 
  load, 
  save, 
  fetchIDB,
  addIDB,
  dumpDB,
  restoreDB,
  // for debug only
  clear,
};

function dumpDB(){
  var dbDump = {}
  for(var key in mainDBSchema){
    if(mainDBSchema.hasOwnProperty(key)){
      dbDump[key] = localStorage[key]
    }
  }
  var chains = loadFromMainDB('knownChains')
  chains.forEach(chainId => {
    Object.keys(chainDBSchema).forEach(key => {
      var dbKey = getKey(key, chainId)
      dbDump[dbKey] = localStorage[dbKey]
    })
  })
  // TODO hardcoded 'transactions', move to schema
  return Promise.all(chains.map(cid => 
    fetchIDB('transactions', cid, {isRaw: true}).then(txs => 
      dbDump[getKey('transactions', cid)] = JSON.stringify(txs)
    )
  )).then(() => dbDump)
}

function restoreDB(db){
  var chains = JSON.parse(db.knownChains || '[]')
  return Promise.all(chains.map(cid => {
    // TODO hardcoded 'transactions', move to schema
    var key = getKey('transactions',cid)
    var txs = JSON.parse(db[key] || '[]')
    schema.createChainIndexedDB(cid)
    delete db[key]
    return addAllIDBRaw('transactions', txs, cid)
  })).then(() => {
    Object.assign(localStorage, db)
  })
}


function isInitialized(){
  return localStorage.SCHEMA_VERSION != null
}

function init(){
  var {createDB, migrations} = schema
  log('init browserStorage')
  if(!isInitialized()){
    createDB(storage)
    localStorage.SCHEMA_VERSION = JSON.stringify(migrations.length)
    log('created db, version is', migrations.length)
    return true
  } else {
    return migrate(migrations)
  }
}

function migrate(migrations){
  var currentVersion = JSON.parse(localStorage.SCHEMA_VERSION)
  var latestVersion = migrations.length;
  if(latestVersion > currentVersion){
    log('run migrations from ', currentVersion, 'to', latestVersion)
    for(var i = currentVersion; i < latestVersion; i++){
      migrations[i]();
    }
    localStorage.SCHEMA_VERSION = JSON.stringify(latestVersion);
    return true
  } else {
    return false
  }
}

function doSaveToMainDB(prop, val){
  // TODO validate
  localStorage[prop] = JSON.stringify(val)
}

// There is "main" db that stores that does not depend on particular
// blockchain(mainnet, testnet), for example node settings and also chain dbs,
// one for every blockchain, that store data like token addresses, that make
// sense only in context of particular blockchain
function saveToMainDB(prop, val){
  if(typeof(prop) == 'object' && val == null){
    var obj = prop
    for(var key in obj){
      if(obj.hasOwnProperty(key)){
        doSaveToMainDB(key, obj[key])
      }
    }
  } else {
    doSaveToMainDB(prop, val)
  }
}

function loadFromMainDB(prop){
  var result = localStorage[prop]
  return result == null ? null : JSON.parse(result)
}

function initChain(_chainId){
  log('init chain, chainId ==', _chainId)
  chainId = _chainId
  var knownChains = localStorage.knownChains == null
    ? []
    : JSON.parse(localStorage.knownChains)
  if(knownChains.indexOf(chainId) == -1){
    knownChains.push(chainId)
    schema.createChainDB(storage, chainId)
    log('created chain db')
    localStorage.knownChains = JSON.stringify(knownChains)
    return true
  } else {
    return false
  }
}

function getKey(prop, cid = chainId){
  return 'chain_' + cid + '_' +prop
}

function ensureInitialized(){
  if(chainId == null){
    throw new Error('not initialized')
  }
}

function ensureKnownProp(prop){
  if(chainDBSchema[prop] == null){
    throw new Error('unknown property: ' + prop)
  }
}

// save to chain db
function save(prop, val){
  ensureInitialized()
  if(typeof(prop) == 'object' && val == null){
    var obj = prop
    for(var key in obj){
      if(obj.hasOwnProperty(key)){
        save(key, obj[key])
      }
    }
  } else {
    ensureKnownProp(prop)
    var propSchema = chainDBSchema[prop]
    var validationResult = propSchema.test(val)
    if(validationResult != null){
      throw new Error('Data does not match schema: \n' +
        validationResult.join('\n') +
        '\n' + 'at prop of database: ' + prop)
    }
    localStorage[getKey(prop)] = JSON.stringify(
      propSchema.toJSON(val)
    )
  }
}

function load(prop){
  ensureInitialized()
  ensureKnownProp(prop)
  var propSchema = chainDBSchema[prop]
  var data = localStorage[getKey(prop)]
  if(data == null){
    return null
  } else {
    return propSchema.fromJSON(JSON.parse(data))
  }
}

function addIDB(prop, record){
  // TODO errors ???
  return new Promise(resolve => {
    ensureInitialized()
    ensureKnownProp(prop)
    var propSchema = chainDBSchema[prop]
    var request = indexedDB.open('chain_' + chainId, 1)
    request.onsuccess = function(){
      var db = request.result
      db
        .transaction([prop], 'readwrite')
        .objectStore(prop)
        .put(propSchema.toJSON(record))
        .onsuccess = resolve
    }
  })
}

function addAllIDBRaw(prop, records, cid){
  if(cid == null){
    ensureInitialized()
    cid = chainId
  }
  ensureKnownProp(prop)
  var request = indexedDB.open('chain_' + cid, 1)
  return new Promise(resolve => {
    request.onsuccess = function(){
      var db = request.result
      var tx = db.transaction([prop], 'readwrite')
      var store = tx.objectStore(prop)
      // notice - we don't convert propSchema.toJSON, because we expect it
      // already to be converted to JSON-like (that's why function name has
      // suffix 'Raw')
      records.forEach(record => {
        store.put(record)
      })
      tx.oncomplete = resolve
    }
  })
}

function fetchIDB(prop, cid, {isRaw} = {isRaw: false}){
  // TODO errors?
  if(cid == null){
    ensureInitialized()
    cid = chainId
  }
  ensureKnownProp(prop)
  var propSchema = chainDBSchema[prop]
  var request = indexedDB.open('chain_' + cid, 1)
  return new Promise(resolve => {
    request.onsuccess = function(){
      var db = request.result
      var store = db
        .transaction([prop], 'readonly')
        .objectStore(prop)
      var rows = []
      store.openCursor().onsuccess = function(e){
        var cursor = e.target.result
        if(cursor){
          var row = isRaw 
            ? cursor.value
            : propSchema.fromJSON(cursor.value)
          rows.push(row)
          cursor.continue()
        } else {
          resolve(rows)
        }
      }
    }
  })
}

function clear(){
  var cids = JSON.parse(localStorage.knownChains || '[]')
  cids.forEach(cid => {
    indexedDB.deleteDatabase('chain_' + cid)
  })
  localStorage.clear()
}

return storage

})
