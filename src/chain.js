define(function(require){

var {tokenCatalogWithDetailsPromise} = require('./tokenCatalog')

var {
  loadAccounts, 
  loadAccountsAndAddresses,
  loadAddressBook,
  fetchBalance,
} = require('account')

var {
  loadTokens, 
  fetchTotalSupply,
  fetchTokenBalance, 
  fetchAllowance,
  fetchOutstandingRewardOf,
} = require('token')

var {fetchPrices} = require('price')

var {
  promisedProperties, 
  effectiveTokenAmount, 
} = require('utils')

var { startWeb3Batching } = require('utils/web3')

var {cachedModule} = require('utils/cached')

// https://bitbucket.org/abdk/token-holder-ui-frontend/issues/2/change-tokens-that-are-shown
/*
For each user, display his balance in the following order: 

- Ether 
- Tokens with non-zero balance, manually added to the "Tokens" page, in the
  order of value, for equal value - in the order of amount 
- Tokens with non-zero balance from catalog, in the order of value, for equal
  value - in the order of amount
*/
function sortTokens(tokens){

  var tkns = loadTokens()

  function composeSortPredicates(...preds){
    return (a, b) => {
      for(var p of preds){
        var result = p(a, b)
        if(result == 0){
          continue
        } else {
          return result
        }
      }
    }
  }

  function by(fn){
    return (t1, t2) => fn(t1) - fn(t2)
  }

  var nonZeroPred = by(t => t.total.isZero() ? 0 : 1)

  var etherPred = by(t => t.token == 'ether' ? 1 : 0)

  var catalogPred = by(t => 
    tkns.find(token => t.token.address == token.address) == null ? 0 : 1
  )

  var valuePred = (a, b) => 
    (a.totalUSD.isNaN() && b.totalUSD.isNaN())
    ? 0
    : by(t => t.totalUSD.isNaN() ? 0 : t.totalUSD.toNumber())(a,b)

  // TODO: regarding decimals???
  var amountPred = by(t => t.total.toNumber())
  
  return tokens.sort(composeSortPredicates(
    etherPred,
    nonZeroPred,
    catalogPred,
    valuePred,
    amountPred
  )).reverse()

}

function fetchTokens(){
  startWeb3Batching()
  return promisedProperties({
    tokens: Promise.all(loadTokens('token').map((token) => 
      fetchTotalSupply(token.address).then(details => (
        Object.assign({}, token, details)
      ))
    )),
    prices: fetchPrices(),
  }).then(({tokens,prices}) => (
    tokens.map(token => Object.assign({}, token, {
      price: prices[token.address]
    }))
  ))
}

// loads token from storage and also tokens from catalog for which address has
// non-negative balances
function fetchEffectiveTokens(address){
  var tokens = loadTokens()
  return tokenCatalogWithDetailsPromise().then(tokenCatalog => {
    var notAddedTokens = tokenCatalog.filter(token =>
      tokens.find(t => t.address == token.address) == null
    )
    return Promise.all(notAddedTokens.map(token => 
      fetchTokenBalance(address, token.address).then(balance => (
        {token, balance}
      ))
    ))
  }).then(tokensWithBalances => {
    var filtered = tokensWithBalances.filter(({balance}) => 
      balance.greaterThan(0)
    )
    return tokens.concat(
      filtered.map(item => item.token)
    )
  })
}

function fetchTokenBalances(accountAddress, tokens){
  return Promise.all(tokens.map((token) => 
    fetchTokenBalance(accountAddress, token.address)
  ))
}

function fetchTokenAllowance(address, tokens, {allowanceOnlyForAddresses}){
  var accounts = allowanceOnlyForAddresses
    ? loadAddressBook()
    : loadAccountsAndAddresses().filter(account => account.address != address)
  return Promise.all(accounts.map((account) => {
    function fetchPairAllowance(from, to){
      return Promise.all(tokens.map((token) => (
        promisedProperties({
          token,
          from,
          to,
          value: fetchAllowance(
            token.address, 
            from,
            to
          ),
        })
      )))
    }
    return promisedProperties({
      account,
      toMe: fetchPairAllowance(account.address, address),
      byMe: fetchPairAllowance(address, account.address),
    })
  })).then(allowance => {
    var tokensWithAllowance = tokens.map((token) => {
      var totalAllowance = {
        toMe: web3.toBigNumber(0),
        byMe: web3.toBigNumber(0),
      }
      allowance.forEach(({toMe,byMe}) => {
        totalAllowance.byMe = totalAllowance.byMe.plus(
          _.findWhere(byMe, {token}).value
        ) 
        totalAllowance.toMe = totalAllowance.toMe.plus(
          _.findWhere(toMe, {token}).value
        ) 
      })
      return totalAllowance
    })
    return tokensWithAllowance
  })
}

function fetchTokenDividends(accountAddress, tokens){
  return Promise.all(tokens.map(token => {
    var dividends
    if(token.isDividend){
      var dividendPayedInToken = tokens.find(t => 
        t.address == token.dividendPayedIn
      )
      if(dividendPayedInToken == null){
        // token was removed
        dividendPayedInToken = {address: token.dividendPayedIn}
      }
      dividends = promisedProperties({
        dividendPayedInToken,
        outstandingReward: fetchOutstandingRewardOf(
          accountAddress, token.address
        ),
      })
    }else{
      dividends = null
    }
    return dividends
  }))
}

function fetchEtherToken(address){
  return promisedProperties({
    balance: fetchBalance(address),
  }).then(({tokenPrice, balance}) => ({
    token: 'ether',
    tokenPrice,
    balance,
    totalAllowance: {
      toMe: web3.toBigNumber(0),
      byMe: web3.toBigNumber(0),
    },
    dividends: null,
  }))
}

function fetchAllTokensData(address, {allowanceOnlyForAddresses, prices} = {}){
  startWeb3Batching()
  return fetchEffectiveTokens(address).then(tokens => {
    startWeb3Batching()
    return promisedProperties({
      tokens,
      etherToken: fetchEtherToken(address),
      prices: prices || fetchPrices(tokens),
      balances: fetchTokenBalances(address, tokens),
      totalAllowance: fetchTokenAllowance(address, tokens, {
        allowanceOnlyForAddresses
      }),
      dividends: fetchTokenDividends(address, tokens),
    })
  }).then(({
    tokens,
    etherToken,
    prices,
    balances,
    totalAllowance,
    dividends,
  }) => {
    var tkns =_.zip(tokens, balances, totalAllowance, dividends).map(
      ([token, balance, totalAllowance, dividends]) => {
        return {
          token,
          balance,
          totalAllowance,
          dividends: dividends == null
            ? null
            : Object.assign({}, dividends, {price: prices[token.dividendPayedIn]})
        }
      }
    )
    return [etherToken].concat(tkns).map(tokenData => {
      var tknWithPrice = Object.assign({}, tokenData, {
        tokenPrice: prices[tokenData.token == 'ether' ? 'ether' : tokenData.token.address]
      })
      return Object.assign({}, tknWithPrice, totals(tknWithPrice))
    })
  }).then(sortTokens)
}

function totals({
  token, tokenPrice, balance, totalAllowance, 
  dividends, 
}){
  var total = balance
    .plus(totalAllowance.toMe)
    .sub(totalAllowance.byMe)
  var totalUSD = effectiveTokenAmount(total, token).times(tokenPrice)
  if(dividends != null){
    totalUSD = totalUSD.plus(
      effectiveTokenAmount(
        dividends.outstandingReward, 
        dividends.dividendPayedInToken
      )
      .times(dividends.price == null ? NaN : dividends.price)
    )
  }
  return {total, totalUSD}
}

function aggregate(tokens){
  function sum(arr){
    return arr.reduce((x,y) => x.plus(y), web3.toBigNumber(0))
  }
  return {
    token: tokens[0].token,
    tokenPrice: tokens[0].tokenPrice,
    balance: sum(tokens.map(t => t.balance)),
    totalAllowance: {
      toMe: sum(tokens.map(t => t.totalAllowance.toMe)),
      byMe: sum(tokens.map(t => t.totalAllowance.byMe)),
    },
    dividends: tokens[0].dividends == null
      ? null
      : {
          dividendPayedInToken: tokens[0].dividends.dividendPayedInToken,
          outstandingReward: sum(tokens.map(t =>
            t.dividends.outstandingReward
          ))
        },
    total: sum(tokens.map(t => t.total)),
    totalUSD: sum(tokens.map(t => t.totalUSD)),
  }
}

function fetchAllTokensDataAggOnAccounts(){
  var accounts = loadAccounts()
  var tokens = loadTokens()
  return tokenCatalogWithDetailsPromise().then(tokenCatalog => {
    var notAddedTokens = tokenCatalog.filter(token =>
      tokens.find(t => t.address == token.address) == null
    )
    var allTokens = tokens.concat(notAddedTokens)
    var prices = fetchPrices(allTokens)
    return Promise.all(accounts.map(acc => 
      fetchAllTokensData(acc.address, {
        // fetch allowance only for addresses, because mutual allowances between
        // our account do not contribute to aggregated values - they are used
        // twice, with different signs, giving zero result
        allowanceOnlyForAddresses: true,
        prices,
      }))
    )
  }).then(accs => {
    var allTokens = _.uniq(_.flatten(
      accs.map(tokens => tokens.map(t => 
        t.token == 'ether' ? 'ether' : t.token.address
      ))
    ))
    return allTokens
      // every acc may have different set of tokens (see
      // fetchEffectiveTokens)
      .map(tokenAddress => 
        accs
          .map(tokens => 
            tokens.find(t => 
              tokenAddress == 'ether' && t.token == 'ether'
              || t.token.address == tokenAddress
            )
          )
          .filter(token => token != null)
      )
      .map(aggregate)
  }).then(sortTokens)
}

var module = {
  fetchTokens,
  fetchAllTokensData,
  fetchAllTokensDataAggOnAccounts,
}

// SIMPLE cache, only for development
return sessionStorage.__USE_CHAIN_CACHE
  ? cachedModule(module)
  : module

})
