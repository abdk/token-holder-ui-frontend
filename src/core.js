define(function(require){

// Global emitter for notifying react component about state changes
var emitter = new EventEmitter();

// single global var containing all app state
/* eslint-disable no-empty */
try { global.state = {}; }catch(e){}
try { window.state = {}; }catch(e){}
/* eslint-enable no-empty */

function stateChanged(ev){
  emitter.trigger(ev);
}

// subscribes react component to state changes
function connect(ev, component) {
  return React.createClass({

    onEvent(){
      this.forceUpdate();
    },

    componentWillMount(){
      emitter.on(ev, this.onEvent);
    },

    componentWillUnmount(){
      emitter.off(ev, this.onEvent);
    },

    render(){
      return React.createElement(component, this.props, this.props.children);
    }

  });
}

/*
Creates valueLink to the prop of the object. This valueLink notifies
about state changes when change is requested

see
https://facebook.github.io/react/docs/two-way-binding-helpers.html#valuelink-without-linkedstatemixin
*/

function link(obj, propName){
  return {
    value: obj[propName],

    requestChange: function(value){
      obj[propName] = value;
    },

    andThen: function(fn){
      var oldRequestChange = this.requestChange;
      return Object.assign(
        this, 
        {
          requestChange: function(val){
            oldRequestChange(val);
            fn(val);
          }
        }
      )
    }
  };
}

function dataChanged(ev){
  emitter.trigger('data:' + ev);
}

function onDataChanged(ev, cb){
  emitter.on('data:'+ev, cb)
}

function offDataChanged(ev, cb){
  emitter.off('data:'+ev, cb)
}

function route({
  dataKey, 
  path, 
  component, 
  onEnter, 
  onLeave, 
  createDataFetcher
}){

  // TODO flatMapLatest fetch ???
  var fetch

  return ({
    path, 
    component, 
    onEnter(){
      fetch = createDataFetcher.apply(null, arguments)
      if(dataKey != null){
        onDataChanged(dataKey, fetch)
      }
      onDataChanged('all', fetch)
      if(onEnter != null){
        onEnter.apply(null, arguments)
      }
    },
    onLeave(){
      if(dataKey != null){
        offDataChanged(dataKey, fetch)
      }
      offDataChanged('all', fetch)
      fetch = null
      if(onLeave != null){
        onLeave.apply(null, arguments)
      }
    },
  })
}

return { 
  stateChanged,
  dataChanged,
  onDataChanged,
  offDataChanged,
  link,
  connect,
  route,
  on: emitter.on.bind(emitter),
  off: emitter.off.bind(emitter),
};


});
