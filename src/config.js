define(function(){
  return {
    DEFAULT_LOCALE: 'en',
    CONFIRMATION_COUNT: 12,
    INFURA_ACCESS_TOKEN: 'WhddDV5MSfbY3bzY5jfQ',
    // prevent content_size_too_large error from node
    MAX_WEB3_BATCH_SIZE: 300,
    MIN_PASSWORD_LENGTH: 8,
    // KDF difficulty for private key encryption
    // when N is 2^11 it takes less than a sec to run scrypt in browser
    SCRYPT_N: Math.pow(2,11),
    GOOGLE_DRIVE_CLIENT_ID: '51497150503-39kpf50eo7r4dc70bjbsf25hb1vll1gd.apps.googleusercontent.com',
    IS_GOOGLE_DRIVE_ENABLED(){
      return window.location.protocol.indexOf('file') == -1
    },
    getGasLimit(estimatedGasLimit){
      return Math.ceil(estimatedGasLimit * 2)
    },
    TOKEN_CATALOG_URL: 'https://bitbucket.org/abdk/token-holder-ui-frontend/raw/master/tokenCatalog',
  }
});
