define(function(require){

function replacer(key, val){
  if(Number.isNaN(val)){
    return ({
      $type: 'NaN', 
    })
  }
  if(val && val.constructor){
    if(val.constructor.name == 'BigNumber'){
      return ({
        $type: 'BigNumber', 
        value: val.toString(),
      })
    }
    if(val.constructor.name == 'Date'){
      return ({
        $type: 'Date', 
        value: val.toISOString(),
      })
    }
  }
  return val
}

function reviver(key, val){
  if(val != null && val.$type != null){
    if(val.$type == 'NaN'){
      return NaN
    }
    if(val.$type == 'Date'){
      return new Date(val.value)
    }
    if(val.$type == 'BigNumber'){
      return web3.toBigNumber(val.value)
    }
    throw 'Unknown type: ' + val.$type
  } else {
    return val
  }
}

function serialize(o){
  var bignumberProto = web3.toBigNumber().__proto__
  var toJSONDate = Date.prototype.toJSON
  var toJSONBigNumber = bignumberProto.toJSON
  delete Date.prototype.toJSON
  delete bignumberProto.toJSON
  try{
    var result = JSON.stringify(o, replacer, 2)
    Date.prototype.toJSON = toJSONDate
    bignumberProto.toJSON = toJSONBigNumber
    return result
  }catch(e){
    Date.prototype.toJSON = toJSONDate
    bignumberProto.toJSON = toJSONBigNumber
    throw e
  }
}

function deserialize(str){
  return JSON.parse(str, reviver)
}

return {serialize, deserialize}

})
