define(function(require){

var {serialize, deserialize} = require('utils/serialize')

it('serialize', () => {

  var o = {
    foo: {
      a: null,
      b: new Date(1),
      c: NaN,
      d: web3.toBigNumber('2000000000000'),
      e: 'test',
      g: 12
    }
  }
  var result = deserialize(serialize(o))
  expect(result.foo.a).to.deep.equal(null)
  expect(result.foo.b).to.deep.equal(new Date(1))
  expect(result.foo.c).to.deep.equal(NaN)
  expect(result.foo.d.equals(web3.toBigNumber('2000000000000')))
    .to.equal(true)
  expect(result.foo.e).to.deep.equal('test')
  expect(result.foo.g).to.deep.equal(12)
  expect(result).to.deep.equal(o)

})

})
