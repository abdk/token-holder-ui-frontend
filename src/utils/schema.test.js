define(function(require){

var {
  Bool, Num, NumWithLimits, Str, Re, BigNum, Dt, Enum,
  Either, Optional, Arr, ObjMap, Obj, FreeFormObj
} = require('utils/schema')

var Address = Re(/^0x[\da-f]{40}$/)
var Hash = Re(/^0x[\da-f]{64}$/)

var Transactions = Arr(Obj({
  transaction: FreeFormObj({
    // don't specify it completely, just several fields
    hash: Hash,
    from: Address,
    value: BigNum,
  }),
  receipt: Optional(FreeFormObj({
    blockHash: Hash,
    from: Address,
  })),
  metadata: Obj({
    date: Dt,
    //TODO seems to be unused, remove
    status: Str,
    type: Enum(
      "transfer_ether", "approve", "transfer_token",
      "transfer_token_from", 'withdraw_reward'
    ), 
    from: Optional(Address),
    to: Optional(Address),
    tokenValue: Optional(BigNum),
    rewardPayedInTokenAddress: Optional(Address),
  }),
}))

var txs = () => [{
"transaction": {
  "blockHash": "0x3f9a63e0abaf0da6f261536766b6d04baf495051c5d1c26b4d2bb24c9e6f6b40",
  "blockNumber": 5027,
  "from": "0xb5814ba93f5dce1f101b938c2b6e5912a5ef8c42",
  "gas": 63686,
  "gasPrice": "20000000000",
  "hash": "0x6c2100187ccb712ffff75408b8af9eacea11c797812d52fe977327af13015ee1",
  "nonce": 31,
  "to": "0x22ffe1903c80879fb7b14330c4aae33e16e1ef9d",
  "transactionIndex": 0,
  "value": web3.toBigNumber(0),
  "v": "0x1c",
  "r": "0x785cd3532941f486debe50e687163d7c25bead651412dfd82786797e71d1b28",
  "s": "0x39679e9f42d4ab38c005c9dcc085974a6e919c401fe3d4d1cc61c96249cd5763"
},
"receipt": {
  "blockHash": "0x3f9a63e0abaf0da6f261536766b6d04baf495051c5d1c26b4d2bb24c9e6f6b40",
  "blockNumber": 5027,
  "contractAddress": null,
  "cumulativeGasUsed": 42457,
  "from": "0xb5814ba93f5dce1f101b938c2b6e5912a5ef8c42",
  "gasUsed": 42457,
  "logs": [
    {
      "address": "0x22ffe1903c80879fb7b14330c4aae33e16e1ef9d",
      "topics": [
        "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef",
        "0x000000000000000000000000169f43001e5f1b01bba5d63d841f34fcf9016c09",
        "0x000000000000000000000000be2787c7b5221fc9893d02b12b90b3b0e630c695"
      ],
      "data": "0x000000000000000000000000000000000000000000000000000000000000000a",
      "blockNumber": 5027,
      "transactionIndex": 0,
      "transactionHash": "0x6c2100187ccb712ffff75408b8af9eacea11c797812d52fe977327af13015ee1",
      "blockHash": "0x3f9a63e0abaf0da6f261536766b6d04baf495051c5d1c26b4d2bb24c9e6f6b40",
      "logIndex": 0,
      "removed": false
    }
  ],
  "root": "0x2cdfbbbdf9fb5d36315a9fe74afbc19768caaa0150f508b265ee9c998ffb8b65",
  "to": "0x22ffe1903c80879fb7b14330c4aae33e16e1ef9d",
  "transactionHash": "0x6c2100187ccb712ffff75408b8af9eacea11c797812d52fe977327af13015ee1",
  "transactionIndex": 0
},
"metadata": {
  "date": new Date("2017-03-31T22:14:50.688Z"),
  "status": "sent",
  "type": "transfer_token_from",
  "from": "0x169f43001e5f1b01bba5d63d841f34fcf9016c09",
  "to": "0xbe2787c7b5221fc9893d02b12b90b3b0e630c695",
  "tokenValue": web3.toBigNumber(10),
  "rewardPayedInTokenAddress": null
}
}]

function ok(val){
  expect(val).to.deep.eql(null)
}

function fail(val){
  expect(val).to.be.instanceof(Array)
  expect(val.length).to.be.above(0)
}

it('schema primitives', () => {
  fail(Obj({foo: Num}).test({foo: 'a'}))
  fail(Obj({foo: Str}).test({foo: 1}))
  fail(Obj({foo: Bool}).test({foo: 1}))
  fail(Obj({foo: Re(/aa/)}).test({foo: 'aba'}))
})

it('schema ObjMap', () => {
  var M = ObjMap(Re(/\d{3}/), Num)
  ok(M.test({
    '123' : 2,
    '456' : 3,
  }))
  fail(M.test({
    '12d3' : 2,
    '456' : 3,
  }))
  fail(M.test({
    '123' : 'aa',
    '456' : 3,
  }))
})

it('schema Enum', () => {
  var En = Enum('foo', 'bar', 'baz')
  ok(En.test('foo'))
  fail(En.test('quz'))
})

it('schema Either', () => {
  var type = Either(Re(/123/), Re(/abc/))
  ok(type.test('123'))
  ok(type.test('abc'))
  fail(type.test('quz'))
})

it('schema validate large obj', () => {
  ok(Transactions.test(txs()))

  var tx
  
  tx = txs()
  tx[0].transaction.value = 1
  fail(Transactions.test(tx))

  tx = txs()
  tx[0].metadata.type = 'foo'
  fail(Transactions.test(tx))

  tx = txs()
  tx[0].foo = 'bar'
  fail(Transactions.test(tx))

  tx = txs()
  tx[0] = {}
  fail(Transactions.test(tx))
})

it('schema parse and stringify', () => {
  var str = JSON.stringify(Transactions.toJSON(txs()))
  var txsParsed = Transactions.fromJSON(JSON.parse(str))
  expect(txsParsed).to.deep.eql(txs())
})

})
