define(function(require){

var {serialize, deserialize} = require('./serialize')

function cachedFn(name, fn){
  return function(){
    var chainCache = localStorage.chainCache
    if(chainCache != null){
      chainCache = JSON.parse(chainCache)
      if(chainCache[name] != null){
        var result = deserialize(chainCache[name])
        return Promise.resolve(result)
      }
    }
    return fn.apply(null, arguments).then(result => {
      chainCache = localStorage.chainCache
      if(chainCache != null){
        chainCache = JSON.parse(chainCache)
      } else {
        chainCache = {}
      }
      chainCache[name] = serialize(result)
      localStorage.chainCache = JSON.stringify(chainCache)
      return result
    })
  }
}

function cachedModule(module){
  var result = {}
  for(var key in module){
    if(module.hasOwnProperty(key)){
      result[key] = cachedFn(key, module[key])
    }
  }
  return result
}

return {
  cachedModule, 
  cachedFn,
}

})
