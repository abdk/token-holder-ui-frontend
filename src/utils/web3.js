define(function(require){

var {messages} = require('i18n')
var config = require('config')

var currentBatch
var batchFlusher

function addToBatch(request){
  if(currentBatch.requests.length >= config.MAX_WEB3_BATCH_SIZE){
    currentBatch.execute()
    currentBatch = web3.createBatch()
  }
  currentBatch.add(request)
}

var Utils = {

  startWeb3Batching(){
    // can be called multiple times in single tick
    if(currentBatch == null){
      currentBatch = web3.createBatch()
      // collect requests to batch in current tick, and
      // flush them on next tick
      batchFlusher = Promise.resolve().then(() => {
        currentBatch.execute()
        currentBatch = null
        batchFlusher = null
      })
    }
  },

  promisify(fn, isContract){
    return function(){
      var args = Array.prototype.slice.call(arguments, 0);
      return new Promise(function(resolve, reject){
        args.push(function(e, res){
          if(!e){
            resolve(res);
          }else{
            reject(e);
          }
        });
        if(currentBatch != null){
          var request = fn.request.apply(null, args)
          if(isContract){
            // TODO small hack - for some reason, passing blockNumber as param to
            // contractMethod does not work. I had no time to debug it. Fix later
            request.params.push('latest')
          }
          addToBatch(request)
        }else{
          fn.apply(null, args)
        }
      });
    };
  },

  callWeb3(fn, isContract = false){
    return function(...args){
      return Utils.promisify(fn, isContract)(...args).catch(e => {
        var isOffline = e.message == 'Invalid JSON RPC response: ""';
        throw {
          isWeb3Error: true,
          // Unfortunately web3 does not provide straightforward way to get
          // information about error
          isOffline,
          message: isOffline 
            ? messages.common.nodeIsOffline
            : e.message,
          originalError: e,
        }
      })
    }
  },

  wrapContractMethod(token, methodName){
    return Utils.callWeb3(token[methodName], true);
  },

  catchWeb3Error(fn){
    return function(e){
      if(e.isWeb3Error){
        return fn(e)
      } else {
        throw e;
      }
    }
  },

}

return Utils

})
