define(function(require){

var AsyncValue = notify => fetcher => {
  var status = 'empty'
  var value
  var callCount = 0
  return {
    fetch(...args){
      callCount++
      status = 'fetching'
      var currentCallCount = callCount
      fetcher(...args).then(v => {
        if(currentCallCount != callCount){
          return
        }
        value = v
        status = 'fetched'
        notify()
      })
    },
    status(){
      return status
    },
    value(){
      if(status != 'fetched'){
        throw 'Invalid call'
      }
      return value
    }
  }
}

return AsyncValue

})
