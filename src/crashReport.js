define(function(require){

var browserStorage = require('browserStorage')
var {promisedProperties} = require('utils')

var LOG_ROTATE_COUNT = 2
var MAX_RECORDS = 2000

function connectIDB(){
  return new Promise(resolve => {
    var request = indexedDB.open('log', 1)
    request.onsuccess = function(){
      resolve(request.result)
    }
    request.onupgradeneeded = function(e){
      var db = e.currentTarget.result
      // create storages for log segments
      for(var i = 0; i < LOG_ROTATE_COUNT; i++){
        db.createObjectStore('log_' + i, { autoIncrement: true })
      }
      connectIDB().then(resolve)
    }
  })
}

function setLogContext(cxt){
  localStorage.logContext = JSON.stringify(cxt)
}

function getLogContext(){
  if(localStorage.logContext == null){
    clearLogContext()
  }
  return JSON.parse(localStorage.logContext)
}

function clearLogContext(){
  setLogContext({
    activeLogSegmentNumber: 0, 
    recordCountInActiveLogSegment: 0,
  })
}

function getActiveLogSegmentName(){
  return 'log_' + getLogContext().activeLogSegmentNumber
}

function truncate(storeName){
  return connectIDB().then(function(db){
    var store = db.transaction([storeName], "readwrite").objectStore(storeName)
    store.clear()
  })
}

var prevAppendOp = Promise.resolve()

function append(record){
  return (prevAppendOp = prevAppendOp.then(() => {
    var cxt = getLogContext()
    var nextCxt
    if(cxt.recordCountInActiveLogSegment > MAX_RECORDS){
      // switch segments
      nextCxt = {
        recordCountInActiveLogSegment: 0, 
        activeLogSegmentNumber: (cxt.activeLogSegmentNumber + 1) % 2,
      }
      setLogContext(nextCxt)
      return truncate(getActiveLogSegmentName())
    } else {
      nextCxt = {
        recordCountInActiveLogSegment: cxt.recordCountInActiveLogSegment + 1, 
        activeLogSegmentNumber: cxt.activeLogSegmentNumber,
      }
      setLogContext(nextCxt)
    }
  }).then(() => {
    return connectIDB().then(function(db){
      var request = db
        .transaction([getActiveLogSegmentName()], 'readwrite')
        .objectStore(getActiveLogSegmentName())
        .put(record)
    })
  }))
}

function getAllFromSegment(storeName){
  return connectIDB().then(function(db){
    var store = db.transaction([storeName], "readwrite").objectStore(storeName)
    var rows = []
    return new Promise(resolve => {
      store.openCursor(null, 'prev').onsuccess = function(e){
        var cursor = e.target.result
        if(cursor){
          rows.push(cursor.value)
          cursor.continue()
        } else {
          resolve(rows)
        }
      }
    })
  })
}

function getLog(){
  return connectIDB().then(function(db){
    var allSegmentNames = []
    var active = getLogContext().activeLogSegmentNumber
    for(var i = 0; i < LOG_ROTATE_COUNT; i++){
      var prevNumber = (active + LOG_ROTATE_COUNT - i) % LOG_ROTATE_COUNT
      allSegmentNames.push('log_' + prevNumber)
    }
    return Promise.all(allSegmentNames.map(getAllFromSegment)).then(_.flatten)
  })
}

function clearLog(){
  clearLogContext()
  var allSegmentNames = []
  for(var i = 0; i < LOG_ROTATE_COUNT; i++){
    allSegmentNames.push('log_' + i)
  }
  return Promise.all(allSegmentNames.map(truncate))
}

function stringify(o){
  return JSON.stringify(o, null, 2)
}

function stringifySafe(o){
  try{
    return stringify(o)
  }catch(e){
    return stringify('Could not stringify ' + e.message)
  }
}

function writeLog(level, ...args){
  append({
    level,
    date: new Date().toISOString(),
    args: stringifySafe(args),
  })
}

function logStartApp(){
  var links = document.head.getElementsByTagName('link')
  var linkToSource = Array.prototype.find.call(links, l => 
    l.getAttribute('rel') == 'author'
  )
  var buildInfo = linkToSource.getAttribute('href')
  log('info', 'start app', buildInfo)
}

function initLogging(){
  window.onerror = function(msg, src, lineNum, colNum, err){
    error({
      msg, src, lineNum, colNum,
      stack: err && err.stack
    })
  }
  window.addEventListener('unhandledrejection', function(ev){
    error('unhandledrejection', ev.reason.toString(), ev.reason.stack)
  });
}

function init(){

  window.log = (...args) => {
    console.log(...args)
    writeLog('info', ...args)
  }
  window.error = (...args) => {
    console.log(...args)
    writeLog('error', ...args)
  }
  initLogging()
  logStartApp()

}

function saveCrashReport({withPrivateKeys}){
  promisedProperties({
    database: browserStorage.dumpDB(),
    log: getLog(),
  }).then(({database, log}) => {
    var chains = database.knownChains == null 
      ? [] 
      : JSON.parse(database.knownChains)
    if(!withPrivateKeys){
      // delete private keys
      chains.forEach(cid => {
        delete database['chain_' + cid + '_accountKeys']
      })
    }
    var report = JSON.stringify({
      database, 
      log,
      userAgent: window.navigator.userAgent,
    }, null, 2)
    saveAs(new Blob([report]), "tokenholder-crashreport-" + new Date().toISOString().split('T')[0]);
  })
}

return {
  init,
  saveCrashReport,
  writeLog,
  getLog,
  clearLog,
}

})
