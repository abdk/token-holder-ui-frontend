define(function(require){

var {
  Bool, Num, NumWithLimits, Str, Re, BigNum, Dt, Enum,
  Either, Optional, Arr, ObjMap, Obj, FreeFormObj
} = require('utils/schema')

var Address = Re(/^0x[\da-f]{40}$/)

var Hash = Re(/^0x[\da-f]{64}$/)

var NotEmptyString = Re(/.+/)

var Accounts = Arr(Obj({
  address: Address,
  name: NotEmptyString,
  // TODO: convert to date
  date: NotEmptyString,
}))

var AddressBook = Accounts

var AccountKeys = ObjMap(Address, FreeFormObj({
  address: NotEmptyString,
  crypto: FreeFormObj({}),
}))

var Tokens = Arr(Obj({
  address: Address,
  name: NotEmptyString,
  // TODO make mandatory, 0 is default
  decimals: Optional(NumWithLimits(0)),
  exchangeSymbol: Optional(NotEmptyString),
  dividendPayedIn: Either(
    Re(/^ether$/),
    Optional(Address)
  ),
  // TODO make mandatory
  isDividend: Optional(Bool),
  // TODO run migration to remove
  totalSupply: Optional(BigNum),
  // TODO run migration to remove
  price: Optional(Num),
}))

var Transaction = Obj({
  transaction: FreeFormObj({
    // don't specify it completely, just several fields
    hash: Hash,
    from: Address,
    value: Optional(BigNum),
  }),
  receipt: Optional(FreeFormObj({
    blockHash: Hash,
  })),
  metadata: Obj({
    date: Dt,
    //TODO seems to be unused, remove
    status: Str,
    type: Enum(
      "transfer_ether", "approve", "transfer_token",
      "transfer_token_from", 'withdraw_reward'
    ), 
    from: Optional(Address),
    to: Optional(Address),
    tokenValue: Optional(BigNum),
    rewardPayedInTokenAddress: Optional(Address),
  }),
})

var mainDBSchema = {
  SCHEMA_VERSION: Num,
  knownChains: Arr(Num),
  recentNodes: Arr(Obj({
    id: Num,
    chainId: Num,
    firstBlockHash: Hash,
    name: NotEmptyString,
    type: Enum('privatenet', 'mainnet', 'testnet'),
    url: NotEmptyString,
  })),
  nodeId: Num,
  catalogNodeId: Optional(Num),
  locale: Optional(NotEmptyString),
}

function createDB(storage){
  storage.saveToMainDB('knownChains', [])
  storage.saveToMainDB('recentNodes', [])
}

// TODO add flag - localStorage or indexedDB
var chainDBSchema = {
  accounts: Accounts,
  accountKeys: AccountKeys,
  addressBook: AddressBook,
  tokens: Tokens,
  transactions: Transaction,
  pendingTransactions: Arr(Transaction),
}

function createChainDB(storage, chainId){
  storage.save({
    accounts: [],
    accountKeys: {},
    tokens: [],
    addressBook: [],
    pendingTransactions: [],
  })
  createChainIndexedDB(chainId)
}

function createChainIndexedDB(chainId){
  var request = indexedDB.open('chain_' + chainId, 1)
  request.onupgradeneeded = function(e){
    var db = e.target.result
    db.createObjectStore('transactions', { keyPath: 
      ['receipt.blockNumber', 'receipt.transactionIndex']
    })
  }
}

var migrations = [
  function(){
    // removed for being obsolete
  },

  function(){
    // obsolete, removed 
  },

  function(){
    if(localStorage.locale != null){
      localStorage.locale = JSON.stringify(localStorage.locale)
    }
  },
  
  function(){
    localStorage.isUsingGDrive = false
  },
  
  function(){
    delete localStorage.log_0
    delete localStorage.log_1
  },

  function(){
    var chains = localStorage.knownChains == null 
      ? []
      : JSON.parse(localStorage.knownChains) 
    chains.forEach(cid => {
      var k = 'chain_' + cid + '_' + 'transactions'
      var txs = JSON.parse(localStorage[k] || '[]')

      var pending = txs.filter(tx => tx.receipt == null)
      localStorage['chain_' + cid + '_' + 'pendingTransactions'] = 
        JSON.stringify(pending)

      var mined   = txs.filter(tx => tx.receipt != null)
      createChainIndexedDB(cid)
      var request = indexedDB.open('chain_' + cid, 1)
      request.onsuccess = function(e){
        var db = e.target.result
        var store = db
          .transaction(['transactions'], 'readwrite')
          .objectStore('transactions')
        mined.forEach((tx,i) => {
            store.put(tx)
        })
      }

      delete localStorage[k]
    })
  },

];

return {createDB, createChainDB, createChainIndexedDB, mainDBSchema, chainDBSchema, migrations}

});
