define(function(require){

var {validateFiles} = require('./importAccounts');
var fs = require('fs')

it('validate valid keystore file', () => {
  var content = fs.readFileSync('./src/testData/keyfile', 'utf8');
  var {validFiles, invalidFiles} = validateFiles(
    [
      {file: {name: 'keyfile'}, content}
    ],
    []
  );
  expect(invalidFiles).to.deep.equal([]);
  expect(validFiles).to.deep.equal([
    {
      fileName: 'keyfile', 
      address: "0x169f43001e5f1b01bba5d63d841f34fcf9016c09",
      isValid: true,
      keyData: JSON.parse(content)
    }
  ]);
});


it('validate already existing keystore file', () => {
  var content = fs.readFileSync('./src/testData/keyfile', 'utf8');
  var {validFiles, invalidFiles} = validateFiles(
    [
      {file: {name: 'keyfile'}, content}
    ],
    [{
      name: 'key file added earlier', 
      address: "0x169f43001e5f1b01bba5d63d841f34fcf9016c09"
    }]
  );
  expect(invalidFiles).to.deep.equal([{
    isValid: false,
    message: 'Account already exists',
    fileName: 'keyfile'
  }]);
  expect(validFiles).to.deep.equal([]);
});

})
