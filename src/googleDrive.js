define(function(require){

var {promisedProperties} = require('utils');
var config = require('config');

// TODO auth2
// https://developers.google.com/api-client-library/javascript/samples/samples#authorizing-and-making-authorized-requests

var readyPromise;

function callGAPI(fn){
  return readyPromise.then(() => {
    return new Promise(function(resolve, reject){
      fn().execute(function(result){
        if(result != null && result.error != null){
          reject({message: result.error.message});
        }else{
          resolve(result);
        }
      })
    })
  })
}

var authConf =  {
  // secret params that triggers account select dialog
  // see https://github.com/google/google-api-javascript-client/issues/13#issuecomment-96298960
  // "authuser": -1,

  'client_id': config.GOOGLE_DRIVE_CLIENT_ID,
  'scope': [
    'https://www.googleapis.com/auth/drive.appdata',
  ].join(' '),
}

function loadGapi(){
  var script = document.createElement('script');
  script.setAttribute('src', 
    "https://apis.google.com/js/client.js?onload=onGAPILoaded"
  );
  readyPromise = new Promise(function(readyResolve, readyReject){
    window.onGAPILoaded = function(){
      var driveLoadPromise = new Promise(function(resolve, reject){
        gapi.client.load('drive', 'v2', function(result){
          if(result != null && result.error != null){
            reject(result.error)
          } else {
            resolve(result)
          }
        })
      })
      var immediateAuthPromise = new Promise(function(resolveAuth){
        var conf = Object.assign({}, authConf, {immediate: true});
        gapi.auth.authorize(conf, function(token){
          resolveAuth({isAuthorized: token.status.signed_in})
        })
      })
      Promise.all([driveLoadPromise, immediateAuthPromise])
        .then(readyResolve)
        .catch(readyReject)
    }
  })
  document.head.appendChild(script);
}

function authorize(){
  return new Promise(function(resolve, reject){
    var conf = Object.assign({}, authConf, {immediate: false})
    gapi.auth.authorize(conf, function(token){
      resolve({isAuthorized: token.status.signed_in})
    })
  });
}

// list folder
function ls(folderId = 'appDataFolder'){
  return callGAPI(() => gapi.client.drive.files.list({
    spaces: 'appDataFolder',
    q: `"${folderId}" in parents`
  }))
  .then(({result}) => result.items)
}

function getFileByName(name, parentId = 'appDataFolder'){
  return callGAPI(() => gapi.client.drive.files.list({
    spaces: 'appDataFolder',
    q: `title = '${name}' and '${parentId}' in parents`,
  })).then(({result}) => {
    if(result.items.length == 0){
      return null;
    } else {
      return result.items[0];
      // TODO warn if two folders with same name found
    }
  })
}

// Code is copypasted from https://developers.google.com/drive/v2/reference/files/insert#examples
function insertFile(name, content, parentId) {
  const boundary = '-------314159265358979323846';
  const delimiter = "\r\n--" + boundary + "\r\n";
  const close_delim = "\r\n--" + boundary + "--";

  var contentType = 'application/json';
  var metadata = {
    'title': name,
    'mimeType': contentType,
    "parents": [{"id":parentId}],
  };

  var multipartRequestBody =
    delimiter +
    'Content-Type: application/json\r\n\r\n' +
    JSON.stringify(metadata) +
    delimiter +
    'Content-Type: ' + contentType + '\r\n' +
    '\r\n' +
    content +
    close_delim;

  var request = callGAPI(() => gapi.client.request({
    'path': '/upload/drive/v2/files',
    'method': 'POST',
    'params': {'uploadType': 'multipart'},
    'headers': {
      'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
    },
    'body': multipartRequestBody
  }))
  return request;
}

function downloadFile(id){
  return callGAPI(() => gapi.client.request({
    'path': '/drive/v2/files/'+id,
    'method': 'GET',
    'params': {'alt': 'media'},
  }))
}

// TODO page_size

// for debugging only
function clear(delayed = true){
  var delay = 0
  function callGAPIDelayed(fn){
    if(delayed){
      return new Promise(resolve => {
        delay += 1000
        setTimeout(resolve, delay)
      }).then(() => callGAPI(fn))
    } else {
      return callGAPI(fn)
    }
  }
  return ls().then(items => Promise.all(
    items.map((item) => 
      callGAPIDelayed(() => gapi.client.drive.files.delete({fileId: item.id}))
    )
  ))
}

// for debugging
function loadTree(id = 'appDataFolder', name){
  return promisedProperties({
    id,
    name,
    children: ls(id).then(items => 
      Promise.all(items.map(item => 
        item.mimeType == "application/vnd.google-apps.folder"
          ? loadTree(item.id, item.title)
          : promisedProperties({
              id: item.id,
              name: item.title,
              content: downloadFile(item.id),
            })
      ))
    )
  })
}

function readFile(name){
  return getFileByName(name).then(item => 
    item == null
      ? null
      : downloadFile(item.id)
  )
}

function saveFile(name, content){
  return insertFile(name, content, 'appDataFolder')
}

return {
  loadGapi, 
  authorize,
  loadTree,
  clear,
  saveFile,
  readFile
};

});
