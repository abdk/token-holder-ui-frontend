define(function(require){

var {messages} = require('i18n');
var {normalizeAddress} = require('utils');

function parseFile({file, content}){
  try{
    var data = JSON.parse(content.toLowerCase())
  }catch(e){
    return {
      fileName: file.name, 
      isValid: false,
      message: messages.importAccounts.notJSON
    }
  }
  if(
    data.address == null || 
    typeof(data.address) != 'string' ||
    !web3.isAddress(data.address)
  ){
    return {
      fileName: file.name, 
      isValid: false,
      message: messages.importAccounts.noAddress
    }
  }
  if(
    data.crypto == null || 
    data.crypto.ciphertext == null ||
    typeof(data.crypto.ciphertext) != 'string' ||
    data.crypto.ciphertext == ''
  ){
    return {
      fileName: file.name, 
      isValid: false,
      message: messages.importAccounts.noPrivate,
    }
  }
  if(data.version != '3'){
    return {
      fileName: file.name, 
      isValid: false,
      message: messages.importAccounts.unsupportedVersion
    }
  }
  return {
    fileName: file.name,
    address: normalizeAddress(data.address),
    isValid: true,
    keyData: data
  }
}

function parseFiles(files){
  return files.map(parseFile)
}

function validateFiles(files, existingAccounts, existingFiles = []){
  var parsedFiles = parseFiles(files)
  var invalidFiles = parsedFiles.filter((f) => !f.isValid)
  var validFileCandidates = parsedFiles.filter((f) => f.isValid)
  var validFiles = []
  validFileCandidates.forEach((f) => {
    if(
      existingFiles.find(
        (ef) => ef.address == f.address
      ) != null ||
      existingAccounts.find((acc) => acc.address == f.address) != null
    ){
      invalidFiles.push({
        fileName: f.fileName,
        isValid: false,
        message: messages.importAccounts.alreadyExists
      });
    }else{
      validFiles.push(f)
    }
  })
  return {validFiles, invalidFiles}
}

return {parseFile, parseFiles, validateFiles}

})
