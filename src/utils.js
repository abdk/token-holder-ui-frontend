define(function(require){

var Utils = {

  promisedProperties(object) {
    var props = [];
    var objectKeys = Object.keys(object);
    objectKeys.forEach((key) => props.push(object[key]));
    return Promise.all(props).then((resolvedValues) => {
      var result = {};
      for(var i = 0; i < objectKeys.length; i++){
        result[objectKeys[i]] = resolvedValues[i];
      }
      return result;
    });
  },

  fromHumanizedValue(amount,token){
    if(token == 'ether'){
      return web3.toWei(amount, 'ether')
    } else {
      var decimals = token.decimals
      decimals = decimals || 0
      return amount.shift(decimals)
    }
  },

  effectiveTokenAmount(amount, token){
    if(token == 'ether'){
      return web3.fromWei(amount)
    } else {
      var decimals = token.decimals
      decimals = decimals || 0
      return amount.shift(-decimals)
    }
  },

  toHumanizedValue(amount,token){
    var effectiveAmount = Utils.effectiveTokenAmount(amount, token)
    return effectiveAmount.toFixed()
  },

  normalizeAddress(address){
    return (address.indexOf('0x') === 0 ? address : '0x' + address)
      .toLowerCase()
  },
  
  formatWei(value){
    return web3.fromWei(value, 'ether')
  },

  timeout(promise, timeoutMs){
    var timeoutId
    return Promise.race(
      promise,
      new Promise((resolve, reject) => {
        timeoutId = setTimeout(() => reject({isTimeout: true}, timeoutMs))
      }).then((result) => {
        clearTimeout(timeoutId)
        return result
      })
    )
  },

}

return Utils

});
