define(function(require){

var storage = require('storage');
var {callWeb3} = require('utils/web3');
var {normalizeAddress} = require('utils');
var {MIN_PASSWORD_LENGTH, SCRYPT_N} = require('config');
var {getChainId} = require('ethNode');

function loadAccounts(){
  return storage.load('accounts') || [];
}

function loadAccountKeys(){
  return storage.load('accountKeys') || {};
}

function loadAccountsWithKeys(){
  var keys = loadAccountKeys();
  return loadAccounts().map(account => ({
    account, 
    keyData: keys[account.address],
  }))
}

function saveAccountKeys(keys){
  // force lower case because etherwallet capitalizes some json keys
  storage.save('accountKeys', 
    JSON.parse(JSON.stringify(keys).toLowerCase())
  )
}

function loadAddressBook(){
  return storage.load('addressBook') || [];
}

function loadAddress(address){
  return loadAddressBook().find(a => a.address == address)
}

function saveAddress({name, address}){
  var addressBook = loadAddressBook();
  addressBook.unshift({name, address, date: new Date()})
  storage.save('addressBook', addressBook)
}

function editAddress(prevAddress, {name, address}){
  var addressBook = loadAddressBook();
  addressBook = addressBook.map(addr => 
    addr.address == prevAddress
    ? {name, address, date: new Date()}
    : addr
  )
  storage.save('addressBook', addressBook)
}

function removeAddress(addr){
  var addressBook = loadAddressBook();
  addressBook = addressBook.filter(({address}) => 
    addr != address 
  )
  storage.save('addressBook', addressBook)
}

function loadAccountsAndAddresses(){
  var accounts = loadAccounts().map(acc => 
    Object.assign({}, acc, {isMine: true})
  )
  var addresses = loadAddressBook().map(acc =>
    Object.assign({}, acc, {isMine: false})
  )
  return [].concat(accounts).concat(addresses)
}

function fetchBalance(address){
  var getBalance = callWeb3(web3.eth.getBalance);
  return getBalance(address)
}

function findAccountByAddress(address){
  var accounts = loadAccounts(address)
  return _.find(accounts, (acc) => 
    acc.address == address
  )
}

function loadAccountDetails(address){
  var account = loadAccountsAndAddresses().find(acc => 
    acc.address == address
  )
  var isMineAccount = account != null && account.isMine
  return {
    isMineAccount,
    account,
  }
}

function removeAccount(address){
  var accounts = loadAccounts();
  var accountKeys = loadAccountKeys();
  accounts = accounts.filter((a) => a.address != address)
  delete accountKeys[address]
  storage.save('accounts', accounts);
  saveAccountKeys(accountKeys);
}

function createAccount(){
  return Wallet.generate(false);
}

function saveAccount(w, {name, password}){
  var keyData = w.toV3(password, {n: SCRYPT_N})
  var address = w.getAddressString()
  var accounts = loadAccounts();
  var accountKeys = loadAccountKeys();
  accounts.unshift({
    name, 
    address,
    date: new Date()
  });
  accountKeys[address] = keyData
  storage.save('accounts', accounts);
  saveAccountKeys(accountKeys);
  return address
}

function changeAccountName(address, name){
  var accounts = loadAccounts()
  accounts.forEach(acc => {
    if(acc.address == address){
      acc.name = name
    }
  })
  storage.save('accounts', accounts);
}

function changeAccountPassword(
  address, 
  currentPassword,
  newPassword
){
  var accountKeys = loadAccountKeys();
  var key = accountKeys[address]
  if(key == null){
    throw new Error('could not find key')
  }
  try{
    var w = Wallet.fromV3(JSON.stringify(key),currentPassword,true)
  }catch(e){
    return {isSuccess: false}
  }
  var keyData = w.toV3(newPassword, {n: SCRYPT_N})
  accountKeys[address] = keyData;
  saveAccountKeys(accountKeys)
  return {isSuccess: true}
}

function importAccounts(newAccs){
  var accounts = loadAccounts();
  var accountKeys = loadAccountKeys();
  newAccs.forEach(({name, keyData}) => {
    var address = normalizeAddress(keyData.address)
    accounts.push({
      name, 
      address,
      // TODO file date?
      date: new Date()
    })
    accountKeys[address] = keyData
  })
  storage.save('accounts', accounts);
  saveAccountKeys(accountKeys);
}

function keyFile(address){
  var acc = loadAccountKeys()[address];
  if(acc == null){
    throw new Error('account is not found', address);
  }
  var name = findAccountByAddress(address).name + '--' + address.slice(2)
  var contents = new Blob([JSON.stringify(acc, null, 2)]);
  return {name, contents}
}

function exportAccount(address){
  var {name, contents} = keyFile(address);
  saveAs(contents, name, true);
}

function exportAllAccounts(){
  var accs = loadAccounts();
  var zip = new JSZip();
  var keystore = zip.folder("keystore");
  loadAccounts().forEach((acc) => {
    var {name, contents} = keyFile(acc.address);
    keystore.file(name, contents);
  })
  zip.generateAsync({type:"blob"}).then(function(content) {
    saveAs(content, "keystore-" + new Date().toISOString().split('T')[0] + ".zip");
  });
}

function decipherPrivateKey(address, password){
  var accountKeys = loadAccountKeys();
  var key = accountKeys[address]
  if(key == null){
    throw new Error('could not find key')
  }
  try{
    var w = Wallet.fromV3(JSON.stringify(key),password,true)
  }catch(e){
    return {isValidPassword: false}
  }
  if(key.crypto.kdfparams.n > SCRYPT_N){
    log('enc is too hard, make it weaker');
    var keyData = w.toV3(password, {n: SCRYPT_N})
    accountKeys[address] = keyData;
    saveAccountKeys(accountKeys)
  }
  return {isValidPassword: true, privateKey: w.getPrivateKeyString()}
}

return {
  createAccount, 
  saveAccount,
  changeAccountName,
  changeAccountPassword,
  loadAccounts,
  loadAddressBook,
  findAccountByAddress,
  loadAccountDetails,
  fetchBalance,
  removeAccount,
  importAccounts,
  exportAccount,
  exportAllAccounts,
  decipherPrivateKey,
  saveAddress,
  editAddress,
  loadAddress,
  removeAddress,
  loadAccountsAndAddresses,
}

});
