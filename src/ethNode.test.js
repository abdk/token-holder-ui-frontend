define(function(require){

var {nextNodeInfo, saveNodeToRecents, loadNodeConfiguration} = require('./ethNode');

it('load and save new node', () => {
  expect(loadNodeConfiguration().isNodeConfigured).to.equal(false)
  saveNodeToRecents({url: 'http://foo.bar', chainId: 0})
  expect(loadNodeConfiguration().recentNodes).to.deep.equal([{
    id: 1,
    url:  'http://foo.bar',
    chainId: 0,
  }])
})

it('save existing node', () => {
  saveNodeToRecents({url: 'http://111', chainId: 0})
  saveNodeToRecents({url: 'http://222', chainId: 0})
  saveNodeToRecents({url: 'http://333', chainId: 0})
  saveNodeToRecents({url: 'http://111', chainId: 0})
  expect(loadNodeConfiguration().recentNodes[0].url).to.deep.equal(
    'http://111'
  )
})

it('nextNodeInfo', () => {
  var i1 = {
    isOnline: false,
    block: {number: null, time: null},
  }
  // go online immediate
  var i20 = nextNodeInfo(i1, {isOnline: true, blockNumber: 1}, 0)
  expect(i20.block.time).to.equal(null)
  expect(i20.isNewBlock).to.equal(false)

  // stay offline
  var i2 = nextNodeInfo(i1, {isOnline: false}, 0)
  expect(i2.block.time).to.equal(null)
  expect(i2.isNewBlock).to.equal(false)

  // go online
  var i3 = nextNodeInfo(i2, {isOnline: true, blockNumber: 10}, 2)
  expect(i3.block.time).to.equal(null)
  expect(i3.isNewBlock).to.equal(false)

  // staying online
  var i4 = nextNodeInfo(i3, {isOnline: true, blockNumber: 10}, 5)
  expect(i4.block.time).to.equal(null)
  expect(i4.isNewBlock).to.equal(false)

  // block number increase
  var i5 = nextNodeInfo(i4, {isOnline: true, blockNumber: 11}, 10)
  expect(i5.block.time).to.equal(10)
  expect(i5.isNewBlock).to.equal(true)

  // go offline
  var i6 = nextNodeInfo(i5, {isOnline: false}, 12)
  expect(i6.block.time).to.equal(10)
  expect(i6.isNewBlock).to.equal(false)

  // return online with same block
  var i7 = nextNodeInfo(i6, {isOnline: true, blockNumber: 11}, 13)
  expect(i7.block.time).to.equal(10)
  expect(i7.isNewBlock).to.equal(false)

  // return online with another block
  var i71 = nextNodeInfo(i6, {isOnline: true, blockNumber: 12}, 13)
  expect(i71.block.time).to.equal(13)
  expect(i71.isNewBlock).to.equal(true)

})


})
