define(function(require){

var i18n = require('i18n')
var render = require('ui/render')
var crashReport = require('crashReport')
var config = require('config')
var googleDrive = require('./googleDrive')
var ethNode = require('./ethNode')
var storage = require('storage')
var initEthNode = require('./initEthNode')
var {openStorageSettings} = require('ui/storageSettings')

crashReport.init()

if(config.IS_GOOGLE_DRIVE_ENABLED()){
  googleDrive.loadGapi();
}

state.ui = {};

Promise.resolve()
.then(() => {
  if(!storage.isInitialized()){
    var settingsPromise = new Promise(resolve => 
      openStorageSettings({
        canClose: false,
        onComplete: resolve, 
        isUsingGDrive: null,
        isFailed: false,
      })
    )
    // TODO make it possible to choose lang before
    // initialization
    i18n.initWithDefaultLang()
    render()
    return settingsPromise.then(({isUsingGDrive, dbDump}) => {
      storage.create({isUsingGDrive, dbDump})
    })
  } else {
    return
  }
})
.then(() => {
  storage.init()
  i18n.init()
  if(ethNode.isNodeConfigured()) {
    initEthNode()
    state.nodeInfo.fetchNodeInfo().then(render)
  } else {
    state.ui.isShowNodeAddress = true
    render()
  }
})

});
