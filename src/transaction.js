define(function(require){

var config = require('./config');
var {loadTokens} = require('token');
var {loadAccountsAndAddresses} = require('account');
var storage = require('./storage');
var {callWeb3, startWeb3Batching} = require('utils/web3');
var {promisedProperties} = require('utils');

function fetchTransactions(){
  return storage.fetchIDB('transactions').then(minedTransactions => {
    return []
      .concat(minedTransactions)
      .concat(storage.load('pendingTransactions'))
  })
}

function filterTransactionDetails(txsDetails, filter){
  var tokenFilter = filter.tokenAddress == null
    ? () => true
    : details => false 
      || details.token == 'ether' && filter.tokenAddress == 'ether'
      || details.token.address == filter.tokenAddress
  var addressFilter = filter.address == null
    ? () => true
    : details => false
      || details.from != null && details.from.address == filter.address
      || details.to != null && details.to.address == filter.address
      || details.via != null && details.via.address == filter.address
  var filterFn = details => tokenFilter(details) && addressFilter(details)
  return txsDetails.filter(filterFn)
}

function fetchTransactionsDetails(){
  var tokens = loadTokens()
  var accounts = loadAccountsAndAddresses()
  return fetchTransactions()
  .then(txs => txs.map(({transaction, receipt, metadata}) => {

      var details = transactionDetails(transaction, receipt, metadata)

      // populate with token data
      if(details.isTokenTx){
        var token = tokens.find(t => 
          t.address == details.token.address
        )
        if(token != null){
          details.token = token
        }
        if(metadata.type == 'withdraw_reward'){
          var dividendToken = tokens.find(t => 
            t.address == details.from.address
          )
          if(dividendToken != null){
            details.from = dividendToken
          }
        }
      }

      // populate with account data
      ['from','to','via'].forEach(key => {
        if(details[key] != null){
          var acc = accounts.find(acc => acc.address == details[key].address)
          if(acc != null){
            details[key] = acc
          }
        }
      })

      return details
    })
  )
}

function transactionDetails(tx, receipt, metadata){

  // confirmations
  var isConfirmed;
  var confirmationsCount;
  if(tx.blockNumber == null){
    isConfirmed = false;
    confirmationsCount = 0;
  }else{
    confirmationsCount = state.nodeInfo.get().blockNumber - tx.blockNumber + 1
    isConfirmed = confirmationsCount >= config.CONFIRMATION_COUNT
  }

  // type
  var type
  if(['transfer_token', 'transfer_ether'].indexOf(metadata.type) != -1){
    type = 'transfer'
    // tx.from == address
    //   ? 'debit'
    //   : 'credit'
  }
  if(metadata.type == 'approve'){
    type = 'approved'
    //tx.from == address
    //  ? 'approved'
    //  : 'was_allowed'
  }
  if(metadata.type == 'transfer_token_from'){
    type = 'transfer_allowed'
    // if(tx.from == address){
    //   type = 'transfer_allowed'
    // }
    // if(metadata.from == address){
    //   type = 'debit_allowed'
    // }
    // if(metadata.to == address){
    //   type = 'credit_allowed'
    // }
  }
  if(metadata.type == 'withdraw_reward'){
    type = 'withdraw_reward'
  }
  if(type == null){
    throw 'Unknown tx type'
  }

  // token
  var isTokenTx = 
    [
      'transfer_token',
      'transfer_token_from',
      'approve',
      'withdraw_reward',
    ]
    .indexOf(metadata.type) != -1
  var token = isTokenTx 
    ? metadata.type == 'withdraw_reward'
      ? {address: metadata.rewardPayedInTokenAddress}
      : {address: tx.to} 
    : 'ether'
  
  // value
  var value = metadata.type == 'transfer_ether'
    ? tx.value
    : metadata.tokenValue

  // from
  var from = metadata.type == 'withdraw_reward'
    ? tx.to // address of token
    : metadata.type == 'transfer_token_from'
      ? metadata.from
      : tx.from

  // to
  var to = isTokenTx
    ? metadata.type == 'withdraw_reward'
      ? tx.from
      : metadata.to
    : tx.to

  // via
  var via = metadata.type == 'transfer_token_from'
    ? tx.from
    : null

  return {
    hash: tx.hash,
    date: metadata.date,
    type,
    from: from == null ? null : {address: from},
    to:   {address: to},
    via:  via == null ? null : {address: via},
    isTokenTx,
    token,
    value,
    isConfirmed,
    confirmationsCount,
    isFailed: receipt != null && receipt.gasUsed == tx.gas
  }
}

function add(tx, meta){
  log('add transaction', tx, meta)
  var pending = storage.load('pendingTransactions')
  pending.push({
    transaction: tx,
    metadata: Object.assign({}, meta, {
      date: new Date(),
      // TODO remove field, not used
      status: 'sent',
    }),
  })
  storage.save('pendingTransactions', pending)
  return tx;
}

function updatePendingTransactions(){
  var pending = storage.load('pendingTransactions')
  log('pendingTransactions: ', pending)
  if(pending.length == 0){
    return Promise.resolve()
  }
  startWeb3Batching()
  return Promise.all(pending.map(tx => 
    promisedProperties(Object.assign(
      {}, 
      fetchTransaction(tx.transaction.hash),
      {metadata: tx.metadata}
    ))
  ))
  .then(txs => {
    var mined   = txs.filter(tx => tx.receipt != null)
    var pending = txs.filter(tx => tx.receipt == null)
    log('save pending txs', pending)
    // TODO lost transactions (web3.eth.getTransaction(hash) returns null, for
    // example because node was restarted just after getting tx. So tx is
    // present in local db but there is no info in blockchain about it)
    storage.save('pendingTransactions', pending.filter(tx => tx.transaction != null))
    log('got receipts for pending txs:', mined)
    return Promise.all(mined.map(tx => storage.addIDB('transactions', tx)))
  });
}

function fetchTransaction(hash){
  var getTransaction = callWeb3(web3.eth.getTransaction);
  var getTransactionReceipt = callWeb3(web3.eth.getTransactionReceipt);
  return {
    transaction: getTransaction(hash),
    receipt: getTransactionReceipt(hash),
  }
}

return {
  add,
  fetchTransactionsDetails,
  updatePendingTransactions,
  filterTransactionDetails,
}

});
