define(function(require){

var {callWeb3, wrapContractMethod} = require('utils/web3');
var {promisedProperties} = require('utils');
var tokenABI = require('data/token-abi');
var storage = require('./storage');

function loadTokens(){
  return storage.load('tokens') || [];
}

function contract(){
  return web3.eth.contract(tokenABI);
}

function fetchTotalSupply(tokenAddress){
  var tokenContract = contract().at(tokenAddress);
  return wrapContractMethod(tokenContract, 'totalSupply')()
    .then((totalSupply) => ({totalSupply}))
}

function saveTokens(tokens){
  storage.save('tokens', tokens);
}

function addToken(token){
  var tokens = storage.load('tokens')
  storage.save('tokens', [token].concat(tokens));
}

function addTokens(tokens){
  storage.save('tokens', [].concat(tokens).concat(storage.load('tokens')));
}

function editToken(prevToken, nextToken){
  var tokens = storage.load('tokens')
  saveTokens(tokens.map(t => 
    t.address == prevToken.address 
      ? nextToken
      : t
  ))
}

function removeToken(token){
  storage.save('tokens', loadTokens() 
    .filter((t) => t.address != token.address)
  )
}

function encodeMethodCall(methodName,...args){
  return contract().at()[methodName].getData(...args);
}

function fetchTokenDetails(address){
  var token = contract().at(address);
  return promisedProperties({
    address: Promise.resolve(address),
    // if contract ABI does not support name or decimals then return null
    name: wrapContractMethod(token, 'name')().catch(() => null),
    decimals: wrapContractMethod(token, 'decimals')()
      .then((decimals) => decimals.toNumber())
      .catch(() => null),
    symbol: wrapContractMethod(token, 'symbol')()
      .catch(() => null),
    totalSupply: wrapContractMethod(token, 'totalSupply')(),
  });
}

function isTokenValid(address){
  return callWeb3(web3.eth.getCode)(address).then((code) =>
    code != '0x'
  );
}

function checkIsTokenValidAndFetchDetails(address){
  return isTokenValid(address).then(isValid => {
    if(!isValid){
      return {isValid};
    }else{
      return fetchTokenDetails(address).then((token) => (
        {token, isValid}
      ))
    }
  });
}

function fetchTokenBalance(accountAddress, tokenAddress){
  var token = contract().at(tokenAddress)
  return wrapContractMethod(token,'balanceOf')(accountAddress)
}

function fetchOutstandingRewardOf(accountAddress, tokenAddress, timestamp){
  var token = contract().at(tokenAddress)
  timestamp = timestamp || (new Date().getTime())/1000
  return wrapContractMethod(token, 'outstandingRewardOf')(
    accountAddress, timestamp
  )
}

function fetchAllowance(tokenAddress, from, to){
  var tokenContract = contract().at(tokenAddress)
  return wrapContractMethod(tokenContract, 'allowance')(
    from, to
  )
}

return {
  encodeMethodCall,
  loadTokens,
  fetchTotalSupply,
  saveTokens,
  addToken,
  addTokens,
  editToken,
  removeToken,
  fetchTokenDetails,
  checkIsTokenValidAndFetchDetails,
  fetchTokenBalance,
  fetchAllowance,
  fetchOutstandingRewardOf,
};


});
