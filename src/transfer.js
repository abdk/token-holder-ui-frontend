define(function(require){

var config = require('config')
var {encodeMethodCall} = require('token')
var account = require('./account');
var transaction = require('./transaction');
var {callWeb3, catchWeb3Error} = require('utils/web3');
var {promisedProperties} = require('utils');
var ethNode = require('./ethNode');

function signTransaction(txParams, password){
  txParams = txToHex(txParams)
  txParams.chainId = ethNode.getChainId()
  log('txParams', txParams)
  var {isValidPassword, privateKey} = 
    account.decipherPrivateKey(txParams.from, password)
  if(!isValidPassword){
    throw {isInvalidPassword: true};
  }
  var keyBuffer = EthJS.Buffer.Buffer.from(privateKey, 'hex')
  var tx = new EthJS.Tx(txParams)
  tx.sign(keyBuffer)
  return '0x' + tx.serialize().toString('hex')
}

function txToHex(txParams){
  var tx = {
    from: txParams.from,
    to: txParams.to,
    nonce: web3.toHex(txParams.nonce),
    value: web3.toHex(txParams.value),
    gasPrice: web3.toHex(txParams.gasPrice),
  }
  if(txParams.gasLimit != null){
    tx.gasLimit = web3.toHex(txParams.gasLimit)
  }
  if(txParams.data != null){
    tx.data = web3.toHex(txParams.data)
  }
  return tx
}

function sendTransaction(tx, password){
  log('sendTransaction', tx, 'amount: ', tx.value && tx.value.toString())
  // TODO Check that we really need to estimate gas here and send price
  var gasEstimatePromise = tx.gasLimit == null
    ? callWeb3(web3.eth.estimateGas)(txToHex(tx))
    : Promise.resolve(tx.gasLimit)
  return promisedProperties({
    gasPrice: callWeb3(web3.eth.getGasPrice)(),
    estimatedGasLimit: gasEstimatePromise,
    nonce: callWeb3(web3.eth.getTransactionCount)(tx.from, 'pending')
  })
  .then(({gasPrice, estimatedGasLimit, nonce}) => {
    // TODO gas limit
    log('estimatedGasLimit', estimatedGasLimit)
    var gasLimit = config.getGasLimit(estimatedGasLimit)
    log('set gasLimit ', gasLimit)
    tx = Object.assign(tx, {gasPrice, gasLimit, nonce})
    var signedTx = signTransaction(tx, password);
    return callWeb3(web3.eth.sendRawTransaction)(signedTx)
  })
  .then((hash) => {
    log('tx accepted', hash)
    tx.hash = hash
    return tx
  })
}

function transferEther(tx, password){
  return sendTransaction(tx, password).then((tx) => {
    transaction.add(tx, {type: 'transfer_ether'})
  })
}

function transfer({method, type}, {tokenAddress, from, to, value}, password){
  log(type, {tokenAddress, from, to, value});
  var data = encodeMethodCall(method, to, value)
  log('data', data)
  var tx = {from, to: tokenAddress, data}
  // TODO should we call web3.eth.call before submitting tx?
  callWeb3(web3.eth.call)(tx)
    .then((res) => log('call result', res))
    .catch((ex) => log('call failed', ex))
  return sendTransaction(tx, password).then((tx) => {
    transaction.add(tx, {
      type,
      to,
      tokenValue: value
    })
  })
}

function transferTokensDirect({tokenAddress, from, to, value}, password){
  return transfer(
    {method: 'transfer', type: 'transfer_token'},
    {tokenAddress, from, to, value}, 
    password
  )
}

function approve({tokenAddress, from, to, value}, password){
  return transfer(
    {method: 'approve', type: 'approve'},
    {tokenAddress, from, to, value}, 
    password
  )
}

// TODO use callMethodAndSaveTransaction
function transferTokensFrom({tokenAddress, from, to, via, value}, password){
  var type = 'transfer_token_from'
  var method = 'transferFrom'
  log(type, {tokenAddress, from, to, value});
  var data = encodeMethodCall(method, from, to, value)
  log('data', data)
  var tx = {from: via, to: tokenAddress, data}
  // TODO should we call web3.eth.call before submitting tx?
  callWeb3(web3.eth.call)(tx)
    .then((res) => log('call result', res))
    .catch((ex) => log('call failed', ex))
  return sendTransaction(tx, password).then((tx) => {
    transaction.add(tx, {
      type,
      from,
      to,
      tokenValue: value
    })
  })
}

function transferTokens(params, password){
  if(params.via == null){
    return transferTokensDirect(params, password)
  }else{
    return transferTokensFrom(params, password)
  }
}

// TODO use for all transactions
function callMethodAndSaveTransaction({
  type,
  method,
  args,
  accountAddress,
  tokenAddress,
  tokenValue,
  meta,
  password,
}){
  var data = encodeMethodCall(method, ...args)
  log('type', 'data', data)
  var tx = {from: accountAddress, to: tokenAddress, data}
  // TODO should we call web3.eth.call before submitting tx?
  callWeb3(web3.eth.call)(tx)
    .then((res) => log('call result', res))
    .catch((ex) => log('call failed', ex))
  return sendTransaction(tx, password).then((tx) => {
    transaction.add(tx, Object.assign({type, tokenValue}, meta))
  })
}

function withdrawReward(params, password){
  var type = 'withdraw_reward'
  log(type, params)
  var {tokenAddress, rewardPayedInTokenAddress, accountAddress, value} = params
  return callMethodAndSaveTransaction({
    type,
    method: 'withdrawReward',
    args: [value],
    accountAddress,
    tokenAddress,
    tokenValue: value,
    meta: {rewardPayedInTokenAddress},
    password,
  })
}

return {
  sendTransaction,
  transferEther,
  transferTokens,
  approve,
  withdrawReward,
}

})
