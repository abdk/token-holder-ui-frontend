define(function(require){

var i18n = require('i18n')
var Select = require('widgets/select');

var langs = {ru: 'Russian', en: 'English'}

return function LocalSelect(){
  return Select({
    className: 'locale-select',
    simpleValue: true,
    placeholder: 'Select language',
    value: i18n.locale(),
    searchable: false,
    clearable: false,
    onChange: i18n.setLocale,
    options: i18n.locales().map(loc => ({
      value: loc, label: langs[loc],
    }))
  })
}

})
