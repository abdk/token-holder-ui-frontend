define(function(require){

var {messages} = require('i18n')
var config = require('config')
var Amount = require('widgets/amount')
var TokenLink = require('widgets/tokenLink')
var AccountLink = require('widgets/accountLink')

var {i, div, span, input, h1, button, table, tbody, tr, td} = React.DOM

function accountsBlock({type, from, to, via}){
  if(type != 'withdraw_reward'){
    return [
      {name: messages.transaction.from, value: from},
      {name: messages.transaction.to,   value: to},
      {name: messages.transaction.via,  value: via},
    ].map(({name,value}) => (
      value != null &&
        tr({key: name}
        , td({className: 'acc-name'}, name)
        , td({className: 'acc-value'}, AccountLink(value))
        )
    ))
  }else{
    return [
      tr({key: 'from'}
      , td({className: 'acc-name'}, messages.transaction.from)
      , td({className: 'acc-value'}, TokenLink(from))
      ),
      tr({key: 'to'}
      , td({className: 'acc-name'}, messages.transaction.to)
      , td({className: 'acc-value'}, AccountLink(to))
      ),
    ]
  }
}

return React.createFactory(function({
  details: {
    date,
    type,
    from,
    to,
    via,
    isTokenTx,
    token,
    value,
    isConfirmed,
    confirmationsCount,
    isFailed,
  }
}){
  var description = {
    approved: {message: messages.transaction.approvedToSpend},
    transfer: {message: messages.transaction.transfer},
    transfer_allowed: {message: messages.transaction.transferApprovedFunds},
    withdraw_reward: {message: messages.transaction.withdrawReward},

    //debit: {message: messages.transaction.debit, status: '-'},
    //credit: {message: messages.transaction.credit, status: '+'},
    //approved: {message: messages.transaction.approvedToSpend, status: '-'},
    //was_allowed: {message: messages.transaction.wasAllowedToSpend, status: '+'},
    //transfer_allowed: {message: messages.transaction.transferApprovedFunds, status: '='},
    //debit_allowed: {message: messages.transaction.debit, status: '-'},
    //credit_allowed: {message: messages.transaction.credit, status: '+'},
    //withdraw_reward: {message: messages.transaction.withdrawReward, status: '+'},
  }[type]

  //var statusClass = {
  //  '+' : 'status-plus',
  //  '-' : 'status-minus',
  //  '=' : 'status-eq',
  //}[description.status]

  return div({className: 'card transaction'},
    div({className: 'tx-description ' /*+ statusClass*/}, 
      description.message
    ),
    div({className: 'tx-amount '/* + statusClass*/}, 
      ' ',
      (description.status == '=' ? null : description.status), 
      Amount({amount: value, token, withTokenName: true})
    ),
    div({className: 'tx-status'}, 
      isFailed && span({className: 'tx-status-fail'},
        i({className: 'fa fa-times-circle-o'}),
        ' ',
        messages.transaction.failed
      ),
      (!isFailed && isConfirmed) && span({className: 'tx-status-success'},
        i({className: 'fa fa-check'}),
        ' ',
        messages.transaction.confirmed
      ),
      (!isFailed && !isConfirmed && !isNaN(confirmationsCount)) && span({className: 'tx-status-confirming'},
        i({className: 'fa fa-refresh fa-spin'}),
        ' ',
        messages.transaction.confirmations + ' ' +
        confirmationsCount + ' ' +
        messages.transaction.confirmationsOf + ' ' +
        config.CONFIRMATION_COUNT
      ),
      (!isFailed && isNaN(confirmationsCount)) && span(null,
        messages.transaction.confirmationsUnknown
      )
    ),
    table({className: 'tx-accounts'},
      tbody(null,
        accountsBlock({type, from, to, via})
      )
    ),
    div({className: 'tx-date'}, 
      moment(date).calendar()
    )
  )

});

});
