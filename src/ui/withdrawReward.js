define(function(require){

var core = require('core')
var {loadTokens, fetchOutstandingRewardOf} = require('token')
var {loadAccounts} = require('account')
var {messages} = require('i18n')
var Page = require('widgets/page')
var Modal = require('widgets/modal')
var FormGroup = require('widgets/formGroup')
var FormMessage = require('widgets/formMessage')
var AccountSelect = require('widgets/accountSelect')
var TokenSelect = require('widgets/tokenSelect')
var Amount = require('widgets/amount');
var AmountFormGroup = require('widgets/amountFormGroup')
var PasswordFormGroup = require('widgets/passwordFormGroup')
var AsyncValue = require('utils/asyncValue');
var {withdrawReward} = require('transfer')

var {form, span} = React.DOM

function WithdrawState(options){

  var {accountAddress, tokenAddress} = options
  var amount = null
  var password = null
  var isLoading = false
  var isInvalidPassword = false
  var transferErrorMessage = null
  var tokens = loadTokens()
  var dividendTokens = tokens.filter(token => token.isDividend)
  var accounts = loadAccounts()

  var reward = AsyncValue(stateChanged)(() => 
    fetchOutstandingRewardOf(accountAddress, tokenAddress)
  )
  reward.fetch()

  function stateChanged(){
    core.stateChanged('withdrawReward')
  }

  function withdraw(e){
    e.preventDefault()
    isLoading = true
    transferErrorMessage = null
    withdrawReward({
      accountAddress: accountAddress, 
      tokenAddress,
      value: amount,
    }, password)
    .then((res) => {
      closeWithdrawDialog()
      appHistory.push('transactions')
    })
    .catch(e => {
      if(e.isInvalidPassword){
        isInvalidPassword = true
      }
      if(e.isWeb3Error){
        transferErrorMessage = e.message
      }
    })
    .then(() => {
      isLoading = false
      stateChanged()
    })
    stateChanged()
  }

  function amountError(){
    if(amount == null){
      return null
    }
    if(reward.status() != 'fetched'){
      return null
    }
    if(amount.greaterThan(reward.value())){
      return messages.withdraw.insufficientReward
    }else{
      return null
    }
  }

  function canSubmit(){
    return true
      && accountAddress != null 
      && amount != null 
      && reward.status() == 'fetched'
  }

  function presentation(){
    return {
      accountAddress,
      amount,
      reward,
      amountError: amountError(),
      canSubmit: canSubmit(),
      transferErrorMessage,
      token: tokens.find(t => t.address == tokenAddress),
      amountLink: {
        value: amount,
        requestChange(val){
          amount = val
          transferErrorMessage = null
          stateChanged()
        }
      },
      passwordLink: {
        value: password,
        requestChange(val){
          password = val
          transferErrorMessage = null
          stateChanged()
        },
      },
      isInvalidPasswordLink: {
        value: isInvalidPassword,
        requestChange(val){
          isInvalidPassword = val
          transferErrorMessage = null
          stateChanged()
        }
      },
      tokenAddressLink: {
        value: tokenAddress,
        requestChange(val){
          tokenAddress = val
          reward.fetch()
          stateChanged()
        }
      },
      accountAddressLink: {
        value: accountAddress,
        requestChange(val){
          accountAddress = val
          reward.fetch()
          stateChanged()
        }
      },
      isLoading,
      dividendTokens,
      accounts,
    }
  }

  return {
    withdraw,
    presentation,
  }

}

var WithdrawDialog = React.createFactory(core.connect('withdrawReward', function(){
  var s = state.ui.page.withdraw 
  if(s == null){
    return null
  }
  var p = state.ui.page.withdraw.presentation()
  var buttons = [
    {
      type: 'button',
      btnType: 'default',
      onClick: closeWithdrawDialog,
      title: messages.form.cancel,
    },
    {
      disabled: !p.canSubmit,
      btnType: 'danger',
      title: messages.withdraw.withdraw,
      isLoading: p.isLoading,
    }
  ]
  return form({onSubmit: s.withdraw},
    Modal({
      onClose: closeWithdrawDialog,
      title: messages.withdraw.title,
      buttons,
    },
      FormGroup({
        labelMsg: messages.withdraw.account,
        input: AccountSelect({
          accounts: p.accounts,
          valueLink: p.accountAddressLink,
        }),
        noError: true,
      }),
      FormGroup({
        labelMsg: messages.form.token,
        input: TokenSelect({
          valueLink: p.tokenAddressLink,
          tokens: p.dividendTokens,
        }),
        noError: true,
      }),
      AmountFormGroup({
        valueLink: p.amountLink,
        error: p.amountError,
        status: p.reward.status() == 'fetching'
          ? 'loading'
          : p.amountError == null
            ? null
            : 'warning',
        note: p.reward.status() == 'fetched' &&
          span(null, 
            messages.withdraw.reward + ': ',
            Amount({
              amount: p.reward.value(),
              token: p.token,
              withTokenName: true,
            })
          ),
      }),
      PasswordFormGroup({
        valueLink: p.passwordLink,
        errorLink: p.isInvalidPasswordLink,
        address: p.accountAddress,
      }),
      FormMessage({
        message: p.transferErrorMessage,
        invisible: p.transferErrorMessage == null,
        status: 'error',
        style: 'large',
      })
    )
  )
}))

function showWithdrawDialog(options){
  state.ui.page.withdraw = WithdrawState(options)
  Page.showModal(WithdrawDialog)
}

function closeWithdrawDialog(){
  state.ui.page.withdraw = null
  Page.closeModal()
}

return {showWithdrawDialog}

})
