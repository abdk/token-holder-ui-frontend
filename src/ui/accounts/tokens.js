define(function(require){

var core = require('core');
var Button = require('widgets/button');
var {messages} = require('i18n');
var Preloader = require('widgets/preloader');
var Amount = require('widgets/amount');
var Price = require('widgets/price');
var SmartOverflow = require('widgets/smartOverflow');
var PriceChart = require('widgets/priceChart');
var {showTransfer} = require('ui/transferModal');
var {showApprove} = require('ui/approve');
var {showWithdrawDialog} = require('ui/withdrawReward');

var {sup, div, span, input, h1, h4, h2, button, table, tbody, tr, td, th} = React.DOM;

function showTransferToken(token){
  var s = state.ui.page
  showTransfer({
    token,
    fromAccount: s.account,
  })
}

function doShowApprove(token){
  showApprove({fromAddress: state.ui.page.address, token})
}

var mulSign = span({className: 'mulSign'}, '\u2715')
var plusSign = span({className: 'plusSign'}, '+')
var minusSign = span({className: 'plusSign'}, '-')
var eqSign = span({className: 'eqSign'}, '=')

function formatExp(val){
  var [digits, exp] = val.toExponential(2).split('e+')
  return span(null, '$' + digits + ' \u00d7 10 ' , sup(null, exp))
}

function AmountWithNote({isExponential, isUSD, amount, token, note}){
  return div({className: 'amount-note'},
    div({className: 'amount-note-amount'},
      isUSD 
        ? isExponential
          ? formatExp(amount)
          : Price({amount})
        : Amount({amount, token})
    ),
    div({className: 'amount-note-note'},
      note
    )
  )
}

function AccountingBlock(tokenData){
  var dividends
  var tokenName = tokenData.token == 'ether' 
    ? 'ETH'
    : tokenData.token.name
  if(tokenData.token.isDividend){
    var dividendPayedInTokenName = tokenData.token.dividendPayedIn == 'ether'
      ? 'ETH'
      : tokenData.dividends.dividendPayedInToken.name == null
        ? 'Unknown token'
        : tokenData.dividends.dividendPayedInToken.name 
    dividends = [
      plusSign,
      AmountWithNote({
        token: tokenData.dividends.dividendPayedInToken,
        amount: tokenData.dividends.outstandingReward,
        note: messages.account.tokens.dividends 
          + ' ('
          +  dividendPayedInTokenName
          + ')',
      }),
      mulSign,
      AmountWithNote({
        isUSD: true, 
        amount: tokenData.dividends.price,
        note: dividendPayedInTokenName + ' ' + messages.account.tokens.price,
      }),
    ]
  } else {
    dividends = []
  }
  var allowedToMe = tokenData.totalAllowance.toMe.equals(0)
    ? []
    : [
      plusSign,
      AmountWithNote({
        amount: tokenData.totalAllowance.toMe, 
        token: tokenData.token,
        note: tokenName + ' ' + messages.account.tokens.toMe,
      }),
    ]
  var allowedByMe = tokenData.totalAllowance.byMe.equals(0)
    ? []
    : [
      minusSign,
      AmountWithNote({
        amount: tokenData.totalAllowance.byMe, 
        token: tokenData.token,
        note: tokenName + ' ' + messages.account.tokens.byMe,
      }),
    ]
  return div({className: 'token-investment-row'},
    div({className: 'summary'},
      AmountWithNote({
        token: tokenData.token,
        amount: tokenData.balance, 
        note: tokenName,
      }),
      ...allowedToMe,
      ...allowedByMe,
      mulSign,
      AmountWithNote({
        isUSD: true, 
        amount: tokenData.tokenPrice, 
        note: tokenName + ' ' + messages.account.tokens.price,
      }),
      ...dividends,
      eqSign
    ),
    // see TW-63 - if overflows then render it in exp format
    SmartOverflow({
      container: div({className: 'total'}),
      briefView: () => AmountWithNote({
        isUSD: true,
        isExponential: true,
        amount: tokenData.totalUSD,
        note: messages.account.tokens.total,
      }),
      fullView: () => AmountWithNote({
        isUSD: true,
        amount: tokenData.totalUSD,
        note: messages.account.tokens.total,
      }),
    })
  )

}

function ChartBlock(tokenData){
  var s = state.ui.page
  var interval = s.historicalPricesInterval
  var isPricesLoaded = s.historicalPrices != null
  var token = tokenData.token
  var tokenAddress = token == 'ether' ? 'ether' : token.address
  var prices = isPricesLoaded && s.historicalPrices[tokenAddress]
  var isPricesAvailable = prices != null
  return div({className: 'graph'},
    isPricesLoaded
    ? isPricesAvailable
       ? PriceChart({interval, prices})
       : div({className: 'box-nocontent'},
           messages.account.tokens.noPriceDataAvailable
         )
    : Preloader()
  )
}

var Token = React.createFactory(function(tokenData){
  var s = state.ui.page

  return div({className: 'card token-investment'}, 
    AccountingBlock(tokenData), 
    div({className: 'token-investment-row'}, 
      ChartBlock(tokenData), 
      s.isAllowedAccountActions() && div({className: 'actions'}, 
        Button({
          btnType: 'primary',
          type: 'button',
          title: messages.account.transfer,
          onClick: showTransferToken.bind(null,tokenData.token),
        }), 
        tokenData.token != 'ether' &&
          Button({
            type: 'button',
            btnType: 'primary',
            title: messages.account.approve,
            onClick: doShowApprove.bind(null, tokenData.token),
          }), 
        tokenData.token != 'ether' && tokenData.token.isDividend &&
          Button({
            type: 'button',
            btnType: 'primary',
            title: messages.account.withdraw,
            onClick(){
              showWithdrawDialog({
                accountAddress: state.ui.page.address, 
                tokenAddress: tokenData.token.address,
              })
            }
          })
      )
    )
  )
})

var Tokens = React.createFactory(function(){
  var s = state.ui.page
  return s.tokens.length == 0
    ? div({className: 'box-nocontent'}, messages.accounts.noAccounts)
    : div(null, s.tokens.map(tokenData => 
        Token(Object.assign({
          key: tokenData.token == 'ether' ? 'ether' : tokenData.token.address
        },tokenData))
      ))
})

return Tokens

})
