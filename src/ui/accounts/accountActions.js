define(function(require){

var core = require('core')
var config = require('config')
var Page = require('widgets/page')
var {messages} = require('i18n')
var copyAddress = require('widgets/copyAddress');
var {exportAccount, exportAllAccounts, removeAccount,
  removeAddress} = require('account')

return function(isAllAccounts, account, address){
  if(!isAllAccounts && account == null){
    return [
      {
        name: messages.accounts.addToAddressBook,
        onClick: () => appHistory.push(
          'addressbook/new/' + address
        )
      }
    ]
  }

  function onRemove(){
    if(account.isMine){
      removeAccount(account.address)
    }else{
      removeAddress(account.address)
    }
    appHistory.push('accounts')
    core.dataChanged('accounts')
  }

  function onEdit(){
    var urlBase = account.isMine
      ? 'accounts'
      : 'addressbook'
    appHistory.push(urlBase+'/'+account.address+'/edit')
  }

  var actions = isAllAccounts
    ? []
    : [
        {
          name: messages.account.copyAddress,
          onClick: copyAddress.bind(null, account.address)
        },
        {
          name: messages.form.edit,
          onClick: onEdit,
        },
        {
          name: messages.form.remove,
          onClick: onRemove
        }
      ]

  if(!isAllAccounts && account.isMine){
    actions.push({
      name: messages.account.exportAccount,
      onClick: exportAccount.bind(null, account.address)
    })
  }

  actions.push({
    name: messages.accounts.exportToFile,
    onClick: exportAllAccounts
  })

  return actions
}

})
