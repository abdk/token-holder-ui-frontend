define(function(require){

var {messages} = require('i18n')
var {a, form, div, span, input, h1, button, label, table, tr, th, td, thead, tbody} = React.DOM;
var core = require('core')
var Page = require('widgets/page');
var PageForm = require('widgets/pageForm');
var Button = require('widgets/button');
var account = require('account')
var {validateFiles} = require('importAccounts');
var Identicon = require('widgets/identicon');
var trimInput = require('widgets/trimInput');
var trimmedInput = trimInput(input);
var {openFile, readFiles} = require('utils/file')
var Box = require('widgets/box');

function stateChanged(){
  core.stateChanged('page');
}

function link(o,p){
  return core.link(o,p).andThen(stateChanged);
}

function loadFiles(fileList, existingFiles = []){
  var existingAccounts = account.loadAccounts()
  return readFiles(fileList).then((fileContents) => {
    var files = []
    for(var i = 0; i < fileList.length; i++){
      var file = fileList[i]
      var content = fileContents[i]
      files.push({file, content});
    }
    var existingAccounts = account.loadAccounts()
    return validateFiles(files, existingAccounts, existingFiles);
  })
}

class ImportAccountsState{
  
  constructor(){
    this.validFiles = []
    this.invalidFiles = []
    //bind
    this.openFiles = this.openFiles.bind(this)
    this.importAccounts = this.importAccounts.bind(this)
    this.removeValidFile = this.removeValidFile.bind(this)
    this.removeErrors = this.removeErrors.bind(this)
  }

  openFiles(){
    openKeystoreFiles()
      .then((fileList) => loadFiles(fileList, this.validFiles)
      .then(({validFiles, invalidFiles}) => {
        this.validFiles = this.validFiles.concat(validFiles)
        this.validFiles.forEach((f) => f.name = '')
        this.invalidFiles = this.invalidFiles.concat(invalidFiles)
        stateChanged()
      })
    );
  }

  removeErrors(){
    log('remove', this);
    this.invalidFiles = [];
    stateChanged();
  }

  importAccounts(e){
    e.preventDefault()
    log('import')
    stateChanged();
    // TODO progress
    account.importAccounts(this.validFiles)
    appHistory.push('/')
  }

  removeValidFile(file){
    log('rem', file)
    this.validFiles = this.validFiles.filter((f) => f != file)
    stateChanged()
  }

  canSubmit(){
    return this.validFiles.length != 0 &&
      _.every(this.validFiles, (f) => f.name != '')
  }
}

function getImportState(){
  return state.ui.page
}

function onClose(){
  window.history.back();
}

function openKeystoreFiles(){
  return openFile({multiple: true})
}

var InvalidRow = React.createFactory(function({file}){
  return tr(null,
    td(null, 
      file.fileName
    ),
    td(null, 
      file.message
    )
  )
})

var ValidRow = React.createFactory(function({file}){
  var s = getImportState();
  return tr(null,
    td(null, 
      div({className: 'account'}, 
        Identicon({address: file.keyData.address}),
        web3.toChecksumAddress(file.keyData.address)
      )
    ),
    td(null, 
      trimmedInput({
        className: 'form-control',
        valueLink: link(file, 'name'),
        autoFocus: true
      })
    ),
    td(null, 
      a({style: {cursor: 'pointer'}, onClick: s.removeValidFile.bind(null,file)},
        messages.importAccounts.remove
      )
    )
  )
})

var Content = React.createFactory(function(){
  var s = getImportState();
  var onSubmit = s.importAccounts

  return div(null, 
    s.invalidFiles.length != 0 && Box({
      title: messages.importAccounts.errors,
      className: 'error',
      onClose: s.removeErrors,
    },
      table({className: 'table'}, 
        thead(null, tr(null,
          th(null, messages.importAccounts.fileName),
          th(null, messages.importAccounts.errorMessage)
        )),
        tbody(null, 
          s.invalidFiles.map((file,i) => 
            InvalidRow({key: i, file})
          )
        )
      )
    ),
    PageForm({
      onSubmit,
      buttons: [
        Button({
          type: 'button',
          btnType: 'default',
          onClick: onClose,
          title: messages.form.cancel,
        }),
        Button({
          type: 'button',
          btnType: 'primary',
          onClick: s.openFiles,
          title: messages.importAccounts.openFiles,
        }),
        Button({
          disabled: !s.canSubmit(),
          btnType: 'success',
          title: messages.importAccounts.import,
        })
      ]
    }, 
      s.validFiles.length == 0
        ? div({className: 'form-group'}, 
            messages.importAccounts.noFileSelected  
          )
        : table({className: 'table'}, 
          thead(null, tr(null,
            th(null, messages.importAccounts.address),
            th(null, messages.importAccounts.name),
            th(null, messages.importAccounts.remove)
          )),
          tbody(null, 
            s.validFiles.map((file,i) => 
              ValidRow({key: i, file})
            )
          )
        )
    )
  )
})

var ImportAccounts =  Page({
  header: () => messages.importAccounts.importAccounts,
  content: Content
});

return {
  path: 'accounts/import', 
  component: ImportAccounts,
  onEnter(){
    state.ui.page = new ImportAccountsState()
    // TODO only on flash
    state.ui.page.openFiles()
  }
};

})
