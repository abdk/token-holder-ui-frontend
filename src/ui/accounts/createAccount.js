define(function(require){

var {messages} = require('i18n');
var core = require('core');
var {createAccount, saveAccount} = require('account');
var Page = require('widgets/page');
var PageForm = require('widgets/pageForm');
var ConfirmedPassword = require('widgets/confirmedPassword');
var FormGroup = require('widgets/formGroup');
var trimInput = require('widgets/trimInput');
var Button = require('widgets/button');

function stateChanged(){
  core.stateChanged('page');
}

class CreateAccountState {
  constructor(){
    this.passwordState = {password: null, doNotMatch: false}
    this.isShowError = false
    this.name = ''
    this.isCreating = false
    this.account = createAccount()
    this.address = this.account.getAddressString()
    this.onSubmit = this.onSubmit.bind(this)
    this.onPasswordChange = this.onPasswordChange.bind(this)
  }

  nameLink(){
    return core.link(this, 'name').andThen(stateChanged)
  }

  isNameOk(){
    return this.name != ''
  }

  canSubmit(){
    return (
      this.passwordState.password != null ||
      this.passwordState.doNotMatch
    ) && this.isNameOk()
  }

  onPasswordChange(passwordState){
    this.passwordState = passwordState
    this.isShowError = false
    stateChanged()
  }

  onSubmit(){
    if(this.passwordState.doNotMatch){
      this.isShowError = true
      stateChanged();
    }else{
      this.isCreating = true
      stateChanged()
      var address = saveAccount(this.account, {
        password: this.passwordState.password, 
        name: this.name
      })
      // dummy http request to force browser to show remember password
      // dialog
      web3.net.getPeerCount(function(err,result){})
      appHistory.push('/accounts/' + address);
    }
  }
}

function cancel(){
  window.history.back();
}

var {label, div, span, input, h1, button, form} = React.DOM;

var nameInput = trimInput(input);

var Content = React.createFactory(React.createClass({

  render(){
    var s = state.ui.page;
    var buttons = [
      Button({
        type: 'button', 
        btnType: 'default', 
        onClick: cancel
      }, messages.form.cancel),
      Button({
        type: 'submit', 
        disabled: !s.canSubmit(),
        btnType: 'success', 
        title: messages.form.create,
        isLoading: s.isCreating
      })
    ]
    return PageForm({
      onSubmit: this.onSubmit,
      buttons
    }, 
      FormGroup({
        input: nameInput({
          className: 'form-control', 
          valueLink: s.nameLink(),
          autoFocus: true,
        }),
        status: s.isNameOk() ? 'success' : null,
        labelMsg: messages.createAccount.accountName,
        noError: true
      }),
      ConfirmedPassword({
        address: s.address,
        onChange: s.onPasswordChange,
        isShowError: s.isShowError,
      })
    )
  },

  onSubmit(e){
    e.preventDefault();
    state.ui.page.onSubmit();
  }
}))

var CreateAccount = Page({
  header: () => messages.accounts.create,
  content: Content
})

return {
  path: 'accounts/new', 
  component: CreateAccount, 
  onEnter(){
    state.ui.page = new CreateAccountState()
  }
};

});
