define(function(require){

var core = require('core');
var {catchWeb3Error} = require('utils/web3');
var {messages} = require('i18n');
var {fetchHistoricalPrices} = require('price');
var {fetchAllTokensData, fetchAllTokensDataAggOnAccounts} = require('chain');
var {promisedProperties} = require('utils');
var Page = require('widgets/page');
var {loadAccountsAndAddresses, loadAccountDetails} = require('account');
var {showTransfer} = require('ui/transferModal');
var Tokens = require('./tokens');
var Button = require('widgets/button');
var Link = React.createFactory(ReactRouter.Link);
var HeaderDropdown = require('widgets/headerDropdown');
var RichAccountSelect = require('widgets/richAccountSelect');
var IntervalSelect = require('widgets/intervalSelect');
var accountActions = require('./accountActions');

var {div, span, input, h1, h4, h2, button, table, tbody, tr, td, th} = React.DOM;

function stateChanged(){
  core.stateChanged('page')
}

function showTransferEther(){
  var s = state.ui.page
  showTransfer({
    token: 'ether',
    fromAccount: s.account,
  })
}

function navigateToAddress(address){
  if(address == null){
    appHistory.push('accounts')
  } else {
    appHistory.push('accounts/'+address)
  }
}

class AccountPageState {

  constructor(isAllAccounts, {address}){
    this.isAllAccounts = isAllAccounts
    this.historicalPricesInterval = localStorage.historyPricesInterval || 'month'
    this.address = address
    this.isValid = this.isAllAccounts || web3.isAddress(address)
  }

  setHistoricalPricesInterval(val){
    this.historicalPricesInterval = val
    localStorage.historicalPricesInterval = val
    this.historicalPrices = null
    this.fetchHistoricalPrices()
    stateChanged()
  }

  fetchHistoricalPrices(){
    return fetchHistoricalPrices(this.historicalPricesInterval)
    .then(historicalPrices => {
      this.historicalPrices = historicalPrices
      stateChanged()
    })
  }

  fetchInitial(){
    this.isLoading = true
    this.fetch()
      .then(() => {
        this.isLoading = false
        stateChanged()
      })
      .catch(catchWeb3Error(() => {
        this.isLoading = false
        this.isNetworkError = true
        stateChanged()
      }))
    this.fetchHistoricalPrices()
  }

  fetch(){
    if(!this.isValid){
      return Promise.resolve()
    }
    var address = this.address
    this.accounts = loadAccountsAndAddresses()
    var account, isMineAccount
    if(this.isAllAccounts){
      this.account = null
      this.isMineAccount = false
    } else {
      var accountDetails = loadAccountDetails(address)
      this.account = accountDetails.account
      this.isMineAccount = accountDetails.isMineAccount
    }
    stateChanged()
    return (
      this.isAllAccounts
        ? fetchAllTokensDataAggOnAccounts()
        : fetchAllTokensData(address)
    ).then(tokens => {
      this.tokens = tokens
      stateChanged();
    });
  }

  isAllowedAccountActions(){
    return this.isAllAccounts || this.isMineAccount
  }
}

var Content = function(props){
  var s = state.ui.page
  if(!s.isValid){
    return div({className: 'row'}, 'Invalid address ' + s.address);
  }
  return div({className: 'row'},
    Tokens()
  )
}

var Account = Page({
  header(){
    var s = state.ui.page
    return div(null, 
      div({className: 'header-container account-header-container'},
        RichAccountSelect({
          accounts: s.accounts,
          address: s.isAllAccounts ? null : s.address,
          onChange: navigateToAddress,
        }),
        s.isAllowedAccountActions() &&
          Button({
            btnType: 'primary',
            type: 'button',
            title: messages.account.transfer,
            onClick: showTransferEther,
          }),
        HeaderDropdown({
          style: 'large', 
          options: accountActions(s.isAllAccounts, s.account, s.address),
        }),
        IntervalSelect({
          value: s.historicalPricesInterval,
          onChange(val){
            s.setHistoricalPricesInterval(val)
          }
        })
      )
    )
  },
  content: Content,
})

function makeAccountRoute({path, isAllAccounts}){
  return core.route({
    path,
    dataKey: 'accounts',
    component: Account,
    createDataFetcher({params: {address}}){
      address = isAllAccounts ? null : address.toLowerCase()
      state.ui.page = new AccountPageState(isAllAccounts, {address})
      state.ui.page.fetchInitial()
      return state.ui.page.fetch.bind(state.ui.page)
    }
  })
}

return [
  makeAccountRoute({
    path: 'accounts',
    isAllAccounts: true,
  }),
  makeAccountRoute({
    path: 'accounts/:address',
    isAllAccounts: false,
  })
]

});
