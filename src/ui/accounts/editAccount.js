define(function(require){

var {messages} = require('i18n');
var {
  findAccountByAddress, 
  changeAccountName,
  changeAccountPassword
} = require('account');
var core = require('core');
var Page = require('widgets/page');
var PageForm = require('widgets/pageForm');
var Password = require('widgets/password');
var ConfirmedPassword = require('widgets/confirmedPassword');
var Button = require('widgets/button');
var FormGroup = require('widgets/formGroup');
var trimInput = require('widgets/trimInput');
var {div, input} = React.DOM;
var nameInput = trimInput(input);

class EditAccountState {

  constructor(account){
    this.account = account
    this.currentPassword = ''
    this.passwordState = {password: null, doNotMatch: false}
    this.name = account.name
    this.isInvalidPassword = false
    this.isShowDoNotMatchError = false
    this.stateChanged = () => core.stateChanged('page')
    //bind
    this.onPasswordChange = this.onPasswordChange.bind(this)
    this.onSubmitName = this.onSubmitName.bind(this)
    this.onSubmitPassword = this.onSubmitPassword.bind(this)
  }

  link(prop){
    return core.link(this,prop).andThen(this.stateChanged)
  }

  currentPasswordLink(){
    return core.link(this,'currentPassword')
      .andThen(() => this.isInvalidPassword = false)
      .andThen(this.stateChanged)
  }

  isNameOk(){
    return this.name != ''
  }

  onPasswordChange(passwordState){
    this.passwordState = passwordState
    this.isShowDoNotMatchError = false
    this.stateChanged()
  }

  isNameSubmitDisabled(){
    return !this.isNameOk() || this.name == this.account.name
  }

  onSubmitName(e){
    e.preventDefault()
    changeAccountName(this.account.address, this.name)
    appHistory.goBack()
  }

  isPasswordSubmitDisabled(){
    return false
      || this.currentPassword == '' 
      || (
        this.passwordState.password == null &&
        !this.passwordState.doNotMatch
      )
  }

  onSubmitPassword(e){
    e.preventDefault()
    if(this.passwordState.doNotMatch){
      this.isShowDoNotMatchError = true
      this.stateChanged()
      return
    }
    var {isSuccess} = changeAccountPassword(
      this.account.address,
      this.currentPassword,
      this.passwordState.password
    )
    if(isSuccess){
      appHistory.goBack()
    }else{
      this.isInvalidPassword = true
      this.stateChanged()
    }
  }
}

function cancel(){
  appHistory.goBack()
}

var Content = React.createFactory(function(){
  var s = state.ui.page
  return div(null,
    PageForm({
      onSubmit: s.onSubmitName,
      buttons: [
        Button({
          type: 'button', 
          btnType: 'default',
          title: messages.form.cancel,
          onClick: cancel,
        }),
        Button({
          btnType: 'success',
          title: messages.form.save,
          disabled: s.isNameSubmitDisabled(),
        }),
      ]
    },
      FormGroup({
        labelMsg: messages.editAccount.name,
        input: nameInput({
          className: 'form-control', 
          valueLink: s.link('name'),
        }),
        status: s.isNameSubmitDisabled() ? null : 'success',
        error: s.isNameOk() 
          ? null 
          : messages.form.cannotBeBlank,
      })
    )
    ,
    PageForm({
      onSubmit: s.onSubmitPassword,
      buttons: [
        Button({
          type: 'button', 
          btnType: 'default',
          title: messages.form.cancel,
          onClick: cancel,
        }),
        Button({
          btnType: 'danger',
          title: messages.form.save,
          disabled: s.isPasswordSubmitDisabled(),
        })
      ]
    }, 
      FormGroup({
        labelMsg: messages.editAccount.currentPassword,
        input: Password({valueLink: s.currentPasswordLink()}),
        error: s.isInvalidPassword 
          ? messages.form.password.invalid
          : null
      }),
      ConfirmedPassword({
        label: messages.form.password.newPassword,
        address: s.account.address,
        onChange: s.onPasswordChange,
        isShowError: s.isShowDoNotMatchError,
      })
    )
  )
})

var EditAccount = Page({
  header: () => messages.editAccount.editAccount,
  content: Content
})

return {
  path: 'accounts/:address/edit', 
  component: EditAccount, 
  onEnter({params}){
    var account = findAccountByAddress(params.address)
    state.ui.page = new EditAccountState(account)
  }
};

})
