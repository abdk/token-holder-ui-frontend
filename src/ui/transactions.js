define(function(require){

var core = require('core');
var transaction = require('transaction');
var {loadAccountsAndAddresses} = require('account')
var {messages} = require('i18n');
var Page = require('widgets/page');
var Transaction = require('ui/transaction');
var TokenSelect = require('widgets/tokenSelect');
var RichAccountSelect = require('widgets/richAccountSelect');

var {div, span, input, h1, h4, button, table, tbody, tr, td, th} = React.DOM;

function fetch(){
  transaction.fetchTransactionsDetails().then(txs => {
    state.ui.page = new TransactionsState(txs.reverse())
    core.stateChanged('page')
  })
}

function TransactionsState(transactions){
  var accounts = loadAccountsAndAddresses()
  var filteredTransactions = transactions
  var accountAddressToFilter = null
  var tokenAddressToFilter = null

  function stateChanged(){
    core.stateChanged('page')
  }

  function applyTransactionsFilter(){
    filteredTransactions = transaction.filterTransactionDetails(
      transactions,
      {
        address: accountAddressToFilter,
        tokenAddress: tokenAddressToFilter,
      }
    )
  }

  function setAccountToFilter(val){
    accountAddressToFilter = val
    applyTransactionsFilter()
    stateChanged()
  }

  function setTokenToFilter(val){
    tokenAddressToFilter = val
    applyTransactionsFilter()
    stateChanged()
  }

  function presentation(){
    return {
      accounts,
      transactions: filteredTransactions,
      accountAddressToFilter,
      tokenAddressToFilter,
    }
  }

  return {
    presentation,
    setTokenToFilter,
    setAccountToFilter,
  }
}

var Content = React.createFactory(function(){
  var p = state.ui.page.presentation()
  return  p.transactions.length == 0
    ? div({className: 'box-nocontent'},
        messages.transactions.noTransactions
      )
    : div({className: null},
        p.transactions.map(details => 
          Transaction({
            key: details.hash, 
            details,
          })
        )
      )
})

var Header = React.createFactory(function(){
  var s = state.ui.page
  if(s.isLoading){
    return null
  }
  var p = s.presentation()
  return div({className: 'txs-header-container header-container'},
    messages.transactions.title,
    RichAccountSelect({
      address: p.accountAddressToFilter, 
      onChange: s.setAccountToFilter,
      accounts: p.accounts,
    }),
    TokenSelect({
      canSelectEther: true,
      value: p.tokenAddressToFilter,
      onChange: s.setTokenToFilter,
      clearable: true,
    })
  )
})

var Transactions = Page({
  header: Header,
  content: Content,
})

return core.route({
  path: 'transactions',
  dataKey: 'transactions',
  component: Transactions,
  onEnter(){
    state.ui.page = {isLoading: true}
    fetch()
  },
  createDataFetcher(){
    return fetch
  },
})

})
