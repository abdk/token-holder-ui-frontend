define(function(require){

var core = require('core');
var {messages} = require('i18n');
var account = require('account')
var token = require('token')
var transfer = require('transfer')
var Modal = require('widgets/modal')
var Page = require('widgets/page')
var FormGroup = require('widgets/formGroup')
var Account = require('widgets/account')
var AccountSelect = require('widgets/accountSelect')
var DecimalsAwareAmountFormGroup = require('widgets/decimalsAwareAmountFormGroup');
var FormMessage = require('widgets/formMessage');
var PasswordFormGroup = require('widgets/passwordFormGroup');
var TokenSelect = require('widgets/tokenSelect');

var {label, div, span, input, form, button, table, tbody, tr, td, th} = React.DOM;

class ApproveState {
  constructor(options){
    this.fromAddress = options.fromAddress
    this.tokenAddress = options.token.address
    this.isSubmitting = false
    this.errorMessage = null
    //bind
    this.approve = this.approve.bind(this)
    this.stateChanged = this.stateChanged.bind(this);
    this.onTokenAddressChange = this.onTokenAddressChange.bind(this)
    this.myAccounts = account.loadAccounts()
    this.accounts = account.loadAccountsAndAddresses()
    this.tokens = token.loadTokens()
  }

  stateChanged(){
    core.stateChanged('approve')
  }

  link(prop){
    return core.link(this,prop).andThen(this.stateChanged)
  }

  onTokenAddressChange(value){
    this.tokenAddress = value
    this.stateChanged()
  }

  amountLink(){
    return this.link('amount')
      .andThen(this.stateChanged)
  }

  canApprove(){
    return true
      && this.fromAddress != null 
      && this.toAddress != null 
      && this.tokenAddress != null 
      && this.amount != null 
  }

  approve(e){
    e.preventDefault()
    this.isSubmitting = true
    this.stateChanged();
    transfer.approve(
      {
      tokenAddress: this.tokenAddress,
      from: this.fromAddress,
      to: this.toAddress,
      value: this.amount, 
      },
      this.password
    ).then(() => {
      closeApprove()
      appHistory.push('transactions')
    })
    .catch((e) => {
      if(e.isInvalidPassword){
        this.isInvalidPassword = true
      } else if(e.isWeb3Error){
        this.errorMessage = e.message
      } else {
        throw e
      }
    })
    .then(() => {
      this.isSubmitting = false
      this.stateChanged();
    })
  }
}

var ApproveModal = React.createFactory(core.connect('approve', function(){
  var s = state.ui.page.approveState
  if(s == null){
    return null
  }
  var buttons = [
    {
      type: 'button',
      btnType: 'default',
      onClick: closeApprove,
      title: messages.form.cancel,
    },
    {
      disabled: !s.canApprove(),
      btnType: 'danger',
      title: messages.approve.approve,
      isLoading: s.isSubmitting
    }
  ]
  return form({className: 'approve', onSubmit: s.approve}, 
    Modal({
      title: messages.approve.title,
      onClose: closeApprove,
      buttons
    },
      FormGroup({
        labelMsg: messages.transfer.what,
        input: TokenSelect({
          tokens: s.tokens,
          value: s.tokenAddress,
          onChange: s.onTokenAddressChange,
        }),
        noError: true,
      }),
      FormGroup({
        labelMsg: messages.transaction.from,
        input: AccountSelect({
          valueLink: s.link('fromAddress'),
          accounts: s.myAccounts,
        }),
        noError: true,
      }),
      FormGroup({
        labelMsg: messages.transaction.to,
        input: AccountSelect({
          valueLink: s.link('toAddress'),
          accounts: s.accounts,
        }),
        noError: true,
      }),
      DecimalsAwareAmountFormGroup({
        valueLink: s.amountLink(),
        token: _.findWhere(s.tokens, {address: s.tokenAddress}),
      }),
      PasswordFormGroup({
        valueLink: s.link('password'),
        errorLink: s.link('isInvalidPassword'),
        address: s.fromAddress,
      }),
      FormMessage({
        message: s.errorMessage,
        invisible: s.errorMessage == null,
        status: 'error',
        style: 'large',
      })
    )
  )
}))

function closeApprove(){
  delete state.ui.page.approveState
  Page.closeModal()
}

function showApprove({fromAddress, token}){
  state.ui.page.approveState = new ApproveState({fromAddress, token})
  Page.showModal(ApproveModal)
}

return {showApprove};

});
