define(function(require){

var core = require('core')

var {isNodeConfigured} = require('ethNode');
var AppComponent = require('./app');
var TokensRoute = require('./tokens/tokens');
var CreateAccountRoute = require('./accounts/createAccount');
var ImportAccountRoute = require('./accounts/importAccounts');
var EditAccountRoute = require('./accounts/editAccount');
var AddAddressRoute = require('./addAddress');
var AccountRoute = require('./accounts/account');
var TransactionsRoute = require('./transactions');

var routes = {
  path: '/',
  component: props => AppComponent(props),
  indexRoute: { onEnter: (nextState, replace) => replace('/accounts') },
  childRoutes: [ 
    TokensRoute,
    CreateAccountRoute,
    ImportAccountRoute,
    EditAccountRoute,
    ...AddAddressRoute,
    ...AccountRoute,
    TransactionsRoute,
  ]
}

appHistory = ReactRouter.hashHistory

var RouterComp = React.createFactory(function(){
  return React.createElement(ReactRouter.Router, {
    routes, history: appHistory
  });
});

var Root = React.createFactory(core.connect('all', function(){
  return isNodeConfigured()
    ? RouterComp()
    : AppComponent()
}))

function render(){
  document.body.removeChild(document.body.children[0])
  var container = document.createElement('div')
  document.body.appendChild(container)
  ReactDOM.render(Root(), container)
}

return render;

})
