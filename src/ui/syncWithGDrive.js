define(function(require){

var core = require('core')
var {messages} = require('i18n')
var storage = require('storage')
var googleDrive = require('googleDrive')
var {openStorageSettings} = require('./storageSettings')
var Button = require('widgets/button')

var {div, button, i} = React.DOM

core.on('syncGDrive', function(){
  if(storage.syncWithGDriveStatus() == 'synced'){
    localStorage.isIgnoreGDriveFail = false
  }
})

function closePopup(){
  localStorage.isIgnoreGDriveFail = true
  core.stateChanged('syncGDrive')
}

function tryAgain(){
  googleDrive.authorize().then(() => {
    storage.flush()
  })
}

return React.createFactory(core.connect('syncGDrive', function(){
  var status = storage.isInitialized()
    ? storage.syncWithGDriveStatus()
    : 'disabled'
  var isShowPopup = status == 'out_of_sync' && 
    localStorage.isIgnoreGDriveFail != 'true'
  var statusText = {
    disabled: messages.syncWithGDrive.disabled,
    syncing: messages.syncWithGDrive.syncing,
    synced: messages.syncWithGDrive.synced,
    out_of_sync: messages.syncWithGDrive.out_of_sync,
  }[status]
  return div({className: 'syncWithGDrive-wrapper'}, 
    button({
      className: 'syncWithGDrive ' + status,
      title: 'Google Drive: ' + statusText,
      onClick: () => openStorageSettings({
        canClose: true,
        onComplete: ({isUsingGDrive, dbDump}) => (
          storage.setUsingGDrive({isUsingGDrive, dbDump})
        ),
        isUsingGDrive: storage.isUsingGDrive(),
        isFailed: storage.isUsingGDrive() && !JSON.parse(localStorage.isSyncedWithGDrive)
      }),
    },
      div({className: 'icon'}),
      div({className: 'status'}, 
        status == 'syncing' && i({className: 'fa fa-refresh fa-spin'}),
        statusText
      )
    ),
    isShowPopup &&
      div({className: 'popup th-dropdown'},
        div({className: 'triangle'}),
        div({className: 'message'},
          messages.syncWithGDrive.failedToSync
        ),
        div({className: 'buttons'},
          Button({
            btnType: 'primary',
            onClick: tryAgain,
          },
            messages.form.tryAgain
          ),
          Button({
            btnType: 'default',
            onClick: closePopup,
          },
            messages.syncWithGDrive.ignore
          )
        )
      )
  )
}))

})
