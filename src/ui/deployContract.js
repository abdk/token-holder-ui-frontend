define(function(require){

var {loadAccountsAndAddresses} = require('account')
var core = require('core')
var i18n = require('i18n')
var crashReport = require('crashReport')
var storage = require('storage')
var initEthNode = require('initEthNode')
var {sendTransaction} = require('transfer')

var Page = require('widgets/page')
var PageForm = require('widgets/pageForm')
var Button = require('widgets/button')
var FormGroup = require('widgets/formGroup')
var numberInput = require('widgets/numberInput')
var bigNumberInput = require('widgets/bigNumberInput')
var AccountSelect = require('widgets/accountSelect')
var RichAccountSelect = require('widgets/richAccountSelect')
var PasswordFormGroup = require('widgets/passwordFormGroup')
var FormMessage = require('widgets/formMessage')
var {openFile, readFile} = require('utils/file')
var {callWeb3} = require('utils/web3')

var {div, input, h2, span} = React.DOM

class DeployContractState {
  constructor(){
    this.gasLimit = 3000000
    this.accounts = loadAccountsAndAddresses()
    core.onDataChanged('newBlock', () => {
      if(this.tx != null && this.receipt == null){
        callWeb3(web3.eth.getTransactionReceipt)(this.tx.hash).then(receipt => {
          this.receipt = receipt
          this.stateChanged()
        })
      }
    })
    //bind
    this.setAbiStr = this.setAbiStr.bind(this)
    this.deploy = this.deploy.bind(this)
  }

  stateChanged(){
    return core.stateChanged('deployContract')
  }

  link(prop){
    return {
      value: this[prop], 
      requestChange: (val) => {
        this[prop] = val
        this.stateChanged()
      },
    }
  }
  
  setAbiStr(abiStr){
    this.abiStr = abiStr
    this.args = null
    try {
      this.abi = JSON.parse(abiStr)
    }catch(e){
      this.abi = null
    }
    if(this.isValidABI()){
      this.args = this.constructorInputs().map(() => null)
    }
    this.stateChanged()
  }

  constructorInputs(){
    return this.abi.find(entry => entry.type == 'constructor').inputs
  }
  
  isValidABI(){
    if(!Array.isArray(this.abi)){
      return false
    }
    var constructor = this.abi.find(entry => entry.type == 'constructor')
    if(constructor == null){
      return false
    }
    if(!Array.isArray(constructor.inputs)){
      return false
    }
    return constructor.inputs
  }

  isDisabled(){
    var enabled = true
      && this.isValidABI() 
      && this.bytecode != null 
      && this.args.every(arg => arg != null)
      && this.gasLimit != null
      && this.from != null
      && this.password != null
    return !enabled
  }

  deploy(e){
    e.preventDefault()
    this.error = null
    this.tx = null
    this.receipt = null
    this.isDeploying = true
    var contract = web3.eth.contract(this.abi)
    var bytecode = this.bytecode.trim()
    if(!bytecode.startsWith('0x')){
      bytecode = '0x' + bytecode    
    }
    var data = contract.new.getData(...this.args, {
      from: this.from,
      data: bytecode,
    })
    return sendTransaction({
      from: this.from, 
      data, 
      gasLimit: this.gasLimit,
    }, this.password)
    .then(tx => {
      this.tx = tx
      this.isDeploying = false
      this.stateChanged()
    })
    .catch(e => {
      if(e.isInvalidPassword){
        this.error = 'Invalid password'
      } else if(e.isWeb3Error){
        this.error = e.message
      } else {
        this.error = 'Unknown error'
        this.isDeploying = false
        this.stateChanged()
        throw e
      }
      this.isDeploying = false
      this.stateChanged()
    })
  }

}

var FromFileOrPaste = React.createFactory(function(props){
  return div({
    style: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    }
  },
    Button({
      btnType: 'default',
      title: 'Load from file',
      type: 'button',
      onClick: () => openFile()
        .then(([file]) => readFile(file))
        .then(props.valueLink.requestChange)
    }),
    span({style: {marginLeft: '10px'}}),
    div(null, 'or just paste here'),
    span({style: {marginLeft: '10px'}}),
    input({valueLink: props.valueLink})
  )
})

function AddressArrayInput(props){
  return input({className: 'form-control', onChange: e => {
    var str = e.target.value.toLowerCase()
    var re = /\b0x[a-f0-9]{40}\b/g
    var result = []
    var match
    while((match = re.exec(str)) != null){
      result.push(match[0])
    }
    props.onChange(result)
  }})
}

function Arg({name, type, value, onChange}){
  var inp
  if(type == 'string'){
    inp = input({value, onChange})
  } else if(value == 'bool'){
    inp = input({
      type: 'checkbox', 
      checked: this.value,
      onChange(e){
        onChange(e.targe.value)
      }
    })
  } else if(type == 'address'){
    inp = RichAccountSelect({
      address: value,
      onChange,
      accounts: state.ui.deployContract.accounts,
    })
  } else if(
    ['int','uint','fixed','ufixed'].some(prefix => type.startsWith(prefix))
  ){
    inp = bigNumberInput({valueLink: {value, requestChange: onChange}})
  } else if(type == 'address[]'){
    inp = AddressArrayInput({onChange})
  } else {
    inp = 'Unsupported input type: ' + type
  }
  return FormGroup({
    labelMsg: name,
    input: inp,
  })
}

var Args = React.createFactory(function(props){
  return div(null,
    props.inputs.map(({name, type}, i) => 
      Arg({
        name, 
        type,
        value: props.valueLink.value[i],
        onChange(val){
          var nextValue = props.valueLink.value.slice(0)
          nextValue[i] = val
          props.valueLink.requestChange(nextValue)
        }
      })
    )
  )
})

var DeployContractForm = React.createFactory(core.connect('deployContract', () => {
  var s = state.ui.deployContract
  return PageForm({onSubmit: s.deploy, buttons: []},
    FormGroup({
      labelMsg: 'ABI',
      input: FromFileOrPaste({
        valueLink: {
          value: s.abiStr,
          requestChange: s.setAbiStr,
        }
      }),
    }),
    FormGroup({
      labelMsg: 'Bytecode',
      input: FromFileOrPaste({valueLink: s.link('bytecode')}),
    }),
    (s.isValidABI() && s.constructorInputs().length != 0) &&
      div({className: 'card'},
        div({className: 'header'}, 'Arguments'),
        div({className: 'body'},
          Args({
            valueLink: s.link('args'),
            inputs: s.constructorInputs(),
          })
        )
      ),
    FormGroup({
      labelMsg: 'Gas limit',
      input: numberInput({valueLink: s.link('gasLimit')}),
    }),
    FormGroup({
      labelMsg: 'Account',
      input: AccountSelect({
        accounts: s.accounts.filter(a => a.isMine),
        valueLink: s.link('from'),
      })
    }),
    PasswordFormGroup({
      address: s.from,
      valueLink: s.link('password'),
      errorLink: {value: false, requestChange(){} },
    }),
    Button({
      btnType: 'primary',
      disabled: s.isDisabled(),
      title: 'Deploy',
      isLoading: s.isDeploying,
    }),
    s.error &&
      FormMessage({
        status: 'error',
        message: s.error,
      }),
    (s.tx != null && s.receipt == null) && FormMessage({
      status: 'loading',
      message: 'Waiting for deploy',
    }),
    s.receipt != null && FormMessage({
      status: 'success',
      message: 'Contract deployed, address: ' + s.receipt.contractAddress,
    })
  )
}))

var DeployContractPage = React.createFactory(Page({
  header: 'Deploy contract',
  content: DeployContractForm,
}))

function DeployContract(){
  return div({className: 'container'},
    DeployContractPage()
  )
}

function run(){
  crashReport.init()
  i18n.initWithDefaultLang()
  storage.init()
  initEthNode()
  state.ui = {}
  state.ui.deployContract = new DeployContractState()
  ReactDOM.render(DeployContract(), document.body)
}

run()

})
