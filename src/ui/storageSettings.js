define(function(require){

var core = require('core')
var Modal = require('widgets/modal')
var Button = require('widgets/button')
var googleDrive = require('googleDrive')
var {messages} = require('i18n')
var storage = require('storage')

var {i, div, button, input, label} = React.DOM

class StorageSettingsState {

  constructor({isUsingGDrive, onComplete, canClose, isFailed}){
    this.onComplete = onComplete
    this.isUsingGDrive = isUsingGDrive
    this.canClose = canClose
    this.gdriveStatus = isFailed ? 'fail_auth' : 'none'
    this.authorize = this.authorize.bind(this)
    this.proceed = this.proceed.bind(this)
  }

  stateChanged(){
    core.stateChanged('storageSettings')
  }

  authorize(){
    this.gdriveStatus = 'authorizing'
    this.stateChanged()
    googleDrive.authorize().then(({isAuthorized}) => {
      if(isAuthorized){
        storage.loadFromGDrive()
          .then(dbDump => {
            this.gdriveStatus = 'ok'
            this.dbDump = dbDump
            this.stateChanged()
          })
          .catch(e => {
            this.gdriveStatus = 'fail_dl'
            this.stateChanged()
          })
      }else{
        this.gdriveStatus = 'fail_auth'
        this.stateChanged()
      }
    })
  }

  isFailed(){
    return this.gdriveStatus == 'fail_dl' || this.gdriveStatus == 'fail_auth'
  }

  useGDrive(isUsingGDrive){
    this.isUsingGDrive = isUsingGDrive
    this.stateChanged()
  }

  canProceed(){
    return false
      || this.isUsingGDrive == null 
      || (this.isUsingGDrive === true && this.gdriveStatus != 'ok')
  }

  proceed(){
    close()
    if(this.isUsingGDrive){
      this.onComplete({
        isUsingGDrive: true,
        dbDump: this.dbDump,
      })
    } else {
      this.onComplete({isUsingGDrive: false})
    }
  }

}

var StorageSettingsDialog = React.createFactory(core.connect('storageSettings', function(){
  var s = state.ui.storageSettings
  if(state.ui.storageSettings == null){
    return null
  }
  return Modal({
    title: messages.storageSettings.title,
    className: 'storageSettings',
    onClose: s.canClose ? close : null,
    buttons: [{
      title: messages.storageSettings.proceed,
      btnType: 'success',
      disabled: s.canProceed(),
      onClick: s.proceed,
    }],
  },
    div({className: 'group'},
      div({className: 'option'},
        input({
          type: 'radio', 
          id: 'storage_gdrive', 
          checked: s.isUsingGDrive === true,
          onChange: s.useGDrive.bind(s, true),
        }),
        label({htmlFor: 'storage_gdrive'},
          messages.storageSettings.useGDrive
        )
      ),
      div({className: 'auth-block'},
        Button({
          disabled: !s.isUsingGDrive,
          title: s.gdriveStatus == 'none'
            ? messages.storageSettings.authorize
            : messages.storageSettings.tryAgain,
          btnType: 'primary',
          onClick: s.authorize,
        }),
        s.gdriveStatus == 'authorizing' &&
          div({className: 'authorizing'},
            i({className: 'fa fa-refresh fa-spin'}),
            messages.storageSettings.authorizing
          ),
        s.gdriveStatus == 'fail_auth' &&
          div({className: 'fail'},
            messages.storageSettings.couldNotAuth
          ),
        s.gdriveStatus == 'fail_dl' &&
          div({className: 'fail'},
            messages.storageSettings.couldNotConnect
          ),
        s.gdriveStatus == 'ok' &&
          div({className: 'ok'},
            messages.storageSettings.success
          )
      )
    ),
    div({className: 'group'},
      div({className: 'option'},
        input({
          type: 'radio', 
          id: 'storage_local', 
          checked: s.isUsingGDrive === false,
          onChange: s.useGDrive.bind(s, false),
        }),
        label({htmlFor: 'storage_local'},
          messages.storageSettings.useLocalStorage
        )
      )
    )
  )
}))

function openStorageSettings({isUsingGDrive, onComplete, canClose, isFailed}){
  state.ui.storageSettings = new StorageSettingsState({
    isUsingGDrive, 
    onComplete,
    canClose,
    isFailed,
  })
  core.stateChanged('storageSettings')
}

function close(){
  delete state.ui.storageSettings
  core.stateChanged('storageSettings')
}

return {openStorageSettings, StorageSettingsDialog}

})
