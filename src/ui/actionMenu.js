define(function(require){

var {messages} = require('i18n')
var DropdownOptions = require('widgets/dropdownOptions')
var {openTokenDialog} = require('ui/tokens/tokenDialog')
var {openCatalogModal} = require('ui/tokens/catalogModal')
var {div,i} = React.DOM

var ACTIONS = () => [
  {
    title: messages.accounts.create,
    onSelect: () => appHistory.push('accounts/new'),
  },
  {
    title: messages.accounts.import,
    onSelect: () => appHistory.push('accounts/import'),
  },
  {
    title: messages.addressBook.addAddress,
    onSelect: () => appHistory.push('/addressbook/new'),
  },
  {
    title: messages.token.addFromCatalog,
    onSelect: openCatalogModal,
  },
  {
    title: messages.token.addFromAddress,
    onSelect: () => {
      openTokenDialog({action: 'create'})
    }
  }
]

return React.createFactory(function(){
  return DropdownOptions({options: ACTIONS()}, 
    div({className: 'actionButton', title: messages.form.create},
      i({className: 'fa fa-plus'})
    )
  )
})


})
