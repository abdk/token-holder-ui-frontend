define(function(require){

var core = require('core');
var {messages} = require('i18n');
var {loadAccountsAndAddresses, loadAccounts, fetchBalance} = require('account');
var {fetchTokenBalance, fetchAllowance, loadTokens} = require('token');
var {transferEther, transferTokens} = require('transfer');
var Page = require('widgets/page');
var Modal = require('widgets/modal');
var FormGroup = require('widgets/formGroup');
var FormMessage = require('widgets/formMessage');
var Account = require('widgets/account');
var Amount = require('widgets/amount');
var DecimalsAwareAmountFormGroup = require('widgets/decimalsAwareAmountFormGroup');
var AccountSelect = require('widgets/accountSelect');
var PasswordFormGroup = require('widgets/passwordFormGroup');
var TokenSelect = require('widgets/tokenSelect');
var {catchWeb3Error} = require('utils/web3');
var AsyncValue = require('utils/asyncValue');

var {form, div, span, input, h1, button, label} = React.DOM;

function getTransferState(){
  return state.ui.page.transferState
}

function setTransferState(s){
  state.ui.page.transferState = s
}

function stateChanged(){
  core.stateChanged('transferModal')
}

class TransferState {
  constructor({
    token,
    fromAddress, 
    accounts,
    tokens,
    accountsAndAddresses,
  }){
    if(token == null){
      throw 'invalid state - token not selected'
    }
    this.token = token
    this.accounts = accounts
    this.tokens = tokens
    this.accountsAndAddresses = accountsAndAddresses
    this.amount = null
    this.isSubmitting = false
    this.clearTransferFailed()
    this.password = ''
    this.isInvalidPassword = false

    // from
    this.fromAddress = fromAddress
    this.fromBalance = AsyncValue(stateChanged)(() => 
      this.fetchBalance(this.fromAddress)
    )
    if(this.fromAddress != null){
      this.fromBalance.fetch()
    }

    // to
    this.toAddress = null
    this.toBalance = AsyncValue(stateChanged)(() => (
      this.fetchBalance(this.toAddress)
    ))
    if(this.toAddress != null){
      this.toBalance.fetch(this.toAddress)
    }

    // via
    this.viaAddress = null
    this.viaAllowance = AsyncValue(stateChanged)(() => {
      return fetchAllowance(
        this.token.address, 
        this.fromAddress, 
        this.viaAddress
      )
    })
    this.fetchAllowance()

    // bind
    this.transfer = this.transfer.bind(this)
    this.onTokenChange = this.onTokenChange.bind(this)
    this.onChangeViaAddress = this.onChangeViaAddress.bind(this)
  }

  accountByAddress(address){
    return _.findWhere(this.accountsAndAddresses, {address})
  }
  
  link(prop){
    return core.link(this, prop).andThen(stateChanged)
  }

  tokenValue(){
    if(this.token == null){
      throw 'invalid state - token not selected'
    }
    return this.token == 'ether' 
      ? this.token 
      : this.token.address
  }

  onTokenChange(value){
    this.clearTransferFailed()
    if(value == 'ether'){
      this.token = 'ether'
    }else{
      this.token = _.findWhere(this.tokens, {address: value})
    }
    this.fetchAllowance()
    if(this.fromAddress != null){
      this.fromBalance.fetch()
    }
    if(this.toAddress != null){
      this.toBalance.fetch(this.toAddress)
    }
    stateChanged()
  }

  isShowAllowance(){
    return true
      && this.viaAllowance.status() == 'fetched'
      && this.fromAddress != null
      && this.effectiveViaAddress() != null
  }

  fetchBalance(address){
    if(this.token == null){
      throw 'invalid state - token is not selected';
    }
    if(this.token == 'ether'){
      return fetchBalance(address)
    }else{
      return fetchTokenBalance(address, this.token.address)
    }
  }

  fetchAllowance(){
    if(this.token == null){
      throw 'invalid state - token is not selected'
    }
    if(true
      && this.token != 'ether'
      && this.fromAddress != null 
      && this.effectiveViaAddress() != null
    ){
      this.viaAllowance.fetch()
    }
  }

  fromAddressLink(){
    return core.link(this, 'fromAddress')
      .andThen(() => {
        this.clearTransferFailed()
        this.fromBalance.fetch()
        this.fetchAllowance()
      })
      .andThen(stateChanged)
  }

  toAddressLink(){
    return core.link(this, 'toAddress')
      .andThen(() => {
        this.clearTransferFailed()
        this.toBalance.fetch()
      })
      .andThen(stateChanged)
  }

  effectiveViaAddress(){
    return this.token == 'ether' 
      ? null
      : this.viaAddress
  }

  onChangeViaAddress(viaAddress){
    this.viaAddress = viaAddress
    this.clearTransferFailed()
    this.fetchAllowance()
    stateChanged()
  }

  txSignerAddress(){
    return this.effectiveViaAddress() == null
      ? this.fromAddress
      : this.effectiveViaAddress()
  }

  amountLink(){
    return core.link(this, 'amount')
      .andThen(() => {
        this.clearTransferFailed()
      })
      .andThen(stateChanged)
  }

  canValidateAmount(){
    if(this.token == null){
      throw 'invalid state - token is not selected'
    }
    return true
      && this.fromBalance.status() == 'fetched'
      && (this.effectiveViaAddress() == null
          ||
          this.viaAllowance.status() == 'fetched'
         )
  }

  isAmountValid(){
    if(this.token == null){
      throw 'invalid state - token is not selected'
    }
    if(this.amount == null){
      throw 'invalid state - amount is null'
    }
    if(this.canValidateAmount() == false){
      throw 'invalid state - cannot be validated'
    }
    var isInvalid = (value) => {
      return value.lessThan(this.amount)
    }
    if(isInvalid(this.fromBalance.value())){
      return {
        isValid: false, 
        message: messages.transfer.insufficienBalance,
      }
    }
    if(
      this.effectiveViaAddress() != null && 
      isInvalid(this.viaAllowance.value())
    ){
      return {
        isValid: false, 
        message: messages.transfer.insufficientAllowance
      }
    }
    return {isValid: true}
  }

  amountError(){
    if(this.amount == null){
      return null
    }
    if(!this.canValidateAmount()){
      return null
    }
    var {isValid, message} = this.isAmountValid()
    return isValid
      ? null
      : message
  }
  
  hasDirectTransferForbiddenError(){
    return true 
      && this.fromAddress != null 
      && this.accounts.find(acc => acc.address == this.fromAddress) == null
      && this.effectiveViaAddress() == null
  }

  formError(){
    if(this.hasDirectTransferForbiddenError()){
      return messages.transfer.directTransferForbidden
    }
    if(this.isTransferFailed){
      return this.transferErrorMessage
    }
    return null
  }

  canSend(){
    return true
      && this.amount != null
      && this.token != null
      && this.fromAddress != null 
      && this.toAddress != null 
      && this.canValidateAmount()
      && !this.hasDirectTransferForbiddenError()
  }

  clearTransferFailed(){
    this.isTransferFailed = false
    this.transferErrorMessage = null
  }

  doTransfer(token,params,password){
    if(this.token == null){
      throw 'invalid state - token is not selected'
    }
    if(token == 'ether'){
      return transferEther(params,password)
    } else {
      return transferTokens(
        Object.assign({tokenAddress: token.address}, params),
        password
      )
    }
  }

  transfer(e){
    e.preventDefault()
    this.isSubmitting = true
    this.isTransferFailed = false
    this.transferErrorMessage = null
    stateChanged()
    this.doTransfer(
      this.token,
      {
        from: this.fromAddress,
        to: this.toAddress,
        via: this.effectiveViaAddress(),
        value: this.amount,
      },
      this.password
    ).then((res) => {
      setTransferState(null)
      var addressToRedirect = this.effectiveViaAddress() || this.fromAddress
      appHistory.push('transactions')
    }).catch(e => {
      if(e.isInvalidPassword){
        this.isInvalidPassword = true
      }else if(e.isWeb3Error){
        this.isTransferFailed = true
        this.transferErrorMessage = e.message
      } else {
        throw e
      }
    })
    .then(() => {
      this.isSubmitting = false
      stateChanged()
    })
  }
}

var AccountGroup = React.createFactory(function(props){
  var {labelMsg, balance, token, valueLink, accounts} = props
  return FormGroup({
    labelMsg,
    status: balance.status() == 'fetching'
      ? 'loading'
      : null,
    note: balance.status() == 'fetched' &&
      span(null, 
        messages.transfer.balance + ': ',
        Amount({
          amount: balance.value(),
          token,
          withTokenName: true,
          withDecimals: false,
        })
      ),
    input: AccountSelect({
      valueLink,
      accounts,
    }),
    noError: true,
  })
})

var TransferModal = React.createFactory(core.connect('transferModal', function(props){
  var s = getTransferState()
  if(s == null){
    return null
  }
  var buttons = [
    {
      type: 'button',
      btnType: 'default',
      onClick: onClose,
      title: messages.form.cancel,
    },
    {
      disabled: !s.canSend(),
      btnType: 'danger',
      title: messages.transfer.send,
      isLoading: s.isSubmitting
    }
  ]
  return form({onSubmit: s.transfer}, 
    Modal({
      title: messages.transfer.title,
      onClose, 
      buttons
    }, 
      FormGroup({
        labelMsg: messages.transfer.what,
        input: TokenSelect({
          tokens: s.tokens,
          onChange: s.onTokenChange,
          value: s.tokenValue(),
          canSelectEther: true,
        }),
        noError: true,
      }),
      AccountGroup({
        labelMsg: messages.transaction.from,
        balance: s.fromBalance, 
        token: s.token, 
        valueLink: s.fromAddressLink(), 
        accounts: s.accountsAndAddresses
      }),
      AccountGroup({
        labelMsg: messages.transaction.to, 
        balance: s.toBalance, 
        token: s.token, 
        valueLink: s.toAddressLink(), 
        accounts: s.accountsAndAddresses,
      }),
      FormGroup({
        disabled: s.token == 'ether',
        labelMsg: messages.transaction.via, 
        status: s.viaAllowance.status() == 'fetching'
          ? 'loading'
          : null,
        note: s.isShowAllowance() &&
          span(null, 
            messages.transfer.allowance + ': ',
            Amount({
              amount: s.viaAllowance.value(),
              token: s.token,
              withTokenName: true,
              withDecimals: false,
            })
          ),
        input: AccountSelect({
          disabled: s.token == 'ether',
          placeholder: messages.transfer.directly,
          addNullOption: true,
          accounts: s.accounts,
          value: s.effectiveViaAddress(),
          onChange: s.onChangeViaAddress,
        }),
        noError: true,
      }),
      DecimalsAwareAmountFormGroup({
        valueLink: s.amountLink(),
        token: s.token,
        status: s.amountError() == null
          ? null
          : 'warning',
        error: s.amountError(),
      }),
      PasswordFormGroup({
        valueLink: s.link('password'),
        errorLink: s.link('isInvalidPassword'),
        address: s.txSignerAddress(),
      }),
      FormMessage({
        message: s.formError(),
        invisible: s.formError() == null,
        status: 'error',
        style: 'large',
      })
    )
  )
}))

function showTransfer({
  token,
  fromAccount, 
}){
  state.ui.page.isShowTransfer = true
  state.ui.page.transferState = new TransferState({
    token,
    fromAddress: fromAccount && fromAccount.address, 
    accounts: loadAccounts(),
    tokens: loadTokens(),
    accountsAndAddresses: loadAccountsAndAddresses(),
  })
  Page.showModal(TransferModal)
}

function onClose(){
  state.ui.page.isShowTransfer = false
  state.ui.page.transferState = null
  Page.closeModal()
}

return {
  showTransfer,
}

});
