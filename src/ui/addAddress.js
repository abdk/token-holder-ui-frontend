define(function(require){

var {messages} = require('i18n');
var core = require('core');
var {saveAddress, loadAddress, editAddress} = require('account');
var Page = require('widgets/page');
var Button = require('widgets/button');
var FormGroup = require('widgets/formGroup');
var PageForm = require('widgets/pageForm');
var AddressInput = require('widgets/addressInput');
var trimInput = require('widgets/trimInput');
var nameInput = trimInput(React.DOM.input);

var {span, form} = React.DOM;

class AddAddressState {
  constructor({action, address}){
    this.action = action
    if(action == 'edit'){
      this.prevAddress = address.address
      this.address = address.address
      this.name = address.name
    }else{
      this.address = address
      this.name = ''
    }
    this.hasAddressChanged == false
    //bind
    this.onSubmit = this.onSubmit.bind(this)
  }

  canSubmit(){
    return this.address != null && this.name != ''
  }

  onSubmit(e){
    e.preventDefault();
    var data = {name: this.name, address: this.address}
    if(this.action == 'create'){
      saveAddress(data);
    }else{
      editAddress(this.prevAddress, data)
    }
    appHistory.push('accounts/' + this.address)
  }

  cancel(){
    window.history.back();
  }

  addressLink(){
    return core.link(this,'address')
      .andThen(() => this.hasAddressChanged = true)
      .andThen(this.stateChanged)
  }

  addressError(){
    return this.hasAddressChanged && this.address == null
      ? messages.form.invalidAddress
      : null
  }

  stateChanged(){
    return core.stateChanged('page');
  }

  link(prop){
    return core.link(this,prop).andThen(this.stateChanged)
  }
}

var Content = React.createFactory(function(props){
  var s = state.ui.page;
  var buttons = [
    Button({
      type: 'button', 
      btnType: 'default', 
      onClick: s.cancel,
      title: messages.form.cancel,
    }),
    Button({
      type: 'submit', 
      disabled: !s.canSubmit(),
      btnType: 'success', 
      title: s.action == 'create' 
        ? messages.form.create
        : messages.form.save,
    }),
  ]
  return PageForm({onSubmit: s.onSubmit, buttons}
    // TODO address form group
  , FormGroup({
      labelMsg: messages.addressBook.address,
      input: AddressInput({
        autoFocus: s.address == null,
        valueLink: s.addressLink(),
        className: 'form-control',
      }),
      error: s.addressError()
    })
  , FormGroup({
      labelMsg: messages.addressBook.name,
      input: nameInput({
        autoFocus: s.address != null,
        className: 'form-control',
        valueLink: s.link('name'),
      }),
    })
  )
});

var AddAddress = Page({
  header: () => messages.addressBook.addAddress,
  content: Content
})

var EditAddress = Page({
  header: () => messages.addressBook.editAddress,
  content: Content
})

return [
  {
    path: 'addressbook/new(/:address)', 
    component: AddAddress,
    onEnter({params}){
      state.ui.page = new AddAddressState({
        action: 'create',
        address: params.address,
      })
    }
  },
  {
    path: 'addressbook/:address/edit', 
    component: EditAddress,
    onEnter({params}, replace){
      var address = loadAddress(params.address)
      if(address == null){
        replace('addressbook')
      }else{
        state.ui.page = new AddAddressState({action: 'edit', address})
      }
    }
  },

]

})
