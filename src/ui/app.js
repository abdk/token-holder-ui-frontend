define(function(require){

var {isNodeConfigured} = require('ethNode');
var {messages} = require('i18n');
var core = require('core');
var Link = React.createFactory(ReactRouter.Link);
var NodeInfo = require('./nodeInfo');
var {NodeAddress} = require('./nodeAddress');
var SyncWithGDrive = require('./syncWithGDrive');
var LocaleSelect = require('ui/localeSelect');
var ActionMenu = require('ui/actionMenu');
var {StorageSettingsDialog} = require('ui/storageSettings');

var {div, section, span, input, b, ul, li, label, i} = React.DOM;

var MENU_ITEMS = () => [
  {
    to: 'accounts', 
    text: messages.appMenu.balance,
  },
  {
    to: 'tokens', 
    text: messages.appMenu.tokens,
  },
  {
    to: 'transactions', 
    text: messages.appMenu.transactions,
  }
]

function Menu(){
  return div({className: 'app-menu'},
    ...MENU_ITEMS().map(({to,text}) => {
      var isActive = window.location.hash.slice(2).indexOf(to) == 0
      return Link({
        className: 'app-menu-item ' + (isActive ? 'active' : ''), 
        to
      }, text)
    }),
    ActionMenu()
  )
}

var AppComponent = function(props){
  return div({className: 'app'},
    div({className: 'app-header'},
      div({className: 'container'},
        div({className: 'logo'}
        , span({className: 'prefix'}, 'token')
        , span({className: 'suffix'}, 'holder')
        ),
        Menu(),
        LocaleSelect(),
        NodeAddress(),
        SyncWithGDrive(),
        NodeInfo(),
        StorageSettingsDialog()
      )
    ),
    div({className: 'app-content'},
      div({className: 'placeholder'}),
      div({className: 'container'},
        props.children
      )
    )
  )
}

return React.createFactory(AppComponent)

});
