define(function(require){

var core = require('core');
var {messages} = require('i18n');
var token = require('token');
var chain = require('chain');
var {catchWeb3Error} = require('utils/web3');

var Page = require('widgets/page');
var Card = require('widgets/card');
var Amount = require('widgets/amount');
var Price = require('widgets/price');
var {openTokenDialog} = require('./tokenDialog');
var copyAddress = require('widgets/copyAddress');

var {div, span, input, h2, button, label} = React.DOM;
var Link = React.createFactory(ReactRouter.Link);

function stateChanged(){
  core.stateChanged('page')
}

function removeToken(t){
  token.removeToken(t)
  core.dataChanged('tokens')
}

function fetchTokensInitial(){
  state.ui.page = {isLoading : true}
  return fetchTokens()
    .then(() => {
      state.ui.page.isLoading = false
      stateChanged()
    })
    .catch(catchWeb3Error(e => {
      state.ui.page = {isNetworkError: true}
      stateChanged()
    }))
}

function fetchTokens(){
  return chain.fetchTokens().then(tokens => {
    state.ui.page.tokens = tokens
    stateChanged()
  })
}

var Token = React.createFactory(function({token}){
  return Card({
    title: token.name,
    color: 'orange',
    subTitle: web3.toChecksumAddress(token.address),
    entries: [
      {
        key: messages.token.totalSupply,
        value: Amount({amount: token.totalSupply, token: token}),
      },
      isNaN(token.price)
        // empty space
        ? {
            key: '\u200b',
            value: '\u200b',
          }
        : {
            key: messages.token.price,
            value: Price({amount: token.price}),
          }
    ],
    options: [
      {
        name: messages.token.copyAddress,
        onClick: copyAddress.bind(null, token.address)},
      {
        name: messages.form.edit,
        onClick: openTokenDialog.bind(null, {
          action: 'edit',
          token,
        })
      },
      {
        name: messages.form.remove,
        onClick: removeToken.bind(null, token)
      },
    ]
  });
});

var Content = React.createFactory(function(){
  return (state.ui.page.tokens.length === 0
    ? div({className: 'box-nocontent'}, messages.token.noTokens)
    : div({className: 'row'},
        state.ui.page.tokens.map((token) => 
          Token({token, key: token.address})
        )
      )
  );
});

var Tokens = Page({
  header: () => messages.appMenu.tokens,
  content: Content,
});

return core.route({
  path: 'tokens', 
  dataKey: 'tokens',
  component: Tokens, 
  createDataFetcher(){
    fetchTokensInitial()
    return fetchTokens
  },
});

});
