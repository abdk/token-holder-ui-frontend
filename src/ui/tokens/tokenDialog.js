define(function(require){

var core = require('core');
var {messages} = require('i18n')
var {tokenCatalogPromise} = require('tokenCatalog')
var {loadTokens, addToken, editToken} = require('token');
var {form, div, span, h1, button, label, input, i} = React.DOM;
var trimInput = require('widgets/trimInput');
var numberInput = require('widgets/numberInput');
var addressInput = require('widgets/addressInput');
var Page = require('widgets/page');
var Modal = require('widgets/modal');
var FormGroup = require('widgets/formGroup');
var Checkbox = require('widgets/checkbox');
var Select = require('widgets/select');
var TokenSelect = require('widgets/tokenSelect');
var {checkIsTokenValidAndFetchDetails} = require('token');
var {getExchangeSymbols} = require('price');

var nameInput = trimInput(input);

class TokenState {
  constructor({action, token}){
    Object.assign(this, {
      action,
      isAddressEdited: false,
      isShowNotFound: false,
      isChecking: false,
      allTokens: loadTokens(),
      isShowNameEmpty: false,
      exchangeSymbolOptions: getExchangeSymbols().map(symbol => (
        {label: symbol, value: symbol}
      )),
    })

    if(action == 'create'){
      Object.assign(this, {
        isChecked: false,
        token: null,
        address: null,
        name: '',
        decimals: 0,
        isDividend: false,
        dividendPayedIn: null,
        exchangeSymbol: null,
      })
    } else {
      Object.assign(this, {
        isChecked: true,
        address: token.address,
        name: token.name,
        decimals: token.decimals || 0,
        isDividend: !!token.isDividend,
        dividendPayedIn: token.dividendPayedIn,
        exchangeSymbol: token.exchangeSymbol,
        token,
        originalToken: token,
      })
    }

    this.stateChanged = core.stateChanged.bind(null, 'tokenDialog')
    this.checkAddress = _.debounce(this.checkAddress, 300)
    // bind
    this.onAddressChange = this.onAddressChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  link(p){
    return core.link(this,p).andThen(this.stateChanged)
  }

  onAddressChange(address){
    this.isChecked = false
    this.token = null
    this.isAddressEdited = true
    if(address == null || this.shouldShowAlreadyExistsError()){
      return;
    }
    Object.assign(this, {
      isShowNotFound: false,
      isChecking: true,
    });
    this.stateChanged();
    this.checkAddress(address);
  }

  checkAddress(address){
    tokenCatalogPromise.then(tokenCatalog => {
      checkIsTokenValidAndFetchDetails(address)
      .then(({isValid, token}) => {
        if(address != this.address){
          // address in input changed since request completed
          return;
        }
        if(isValid){
          Object.assign(this, {isChecked: true, token});
          if(token.name != null && token.name.trim() != ''){
            this.name = token.name;
          }
          if(token.decimals != null){
            this.decimals = token.decimals;
            this.decimalInput.setValue(token.decimals);
          }
          var tokenFromCatalog = tokenCatalog.find(t => t.address == address)
          if(tokenFromCatalog != null){
            this.exchangeSymbol = tokenFromCatalog.exchangeSymbol
          }
        }else{
          Object.assign(this, {isShowNotFound: true});
        }
        this.isChecking = false;
      })
      .then(this.stateChanged);
    })
  }

  addressLink(){
    return this.link('address').andThen(this.onAddressChange)
  }

  shouldShowAlreadyExistsError(){
    var address = this.address
    return this.allTokens.find((token) => 
      token.address == address && (
        this.originalToken == null ||
        address != this.originalToken.address
      )
    ) != null;
  }

  canSubmit(){
    return true && 
      this.isChecked && 
      !this.isShowNotFound &&
      (!this.isDividend || this.dividendPayedIn != null)
  }

  onSubmit(e){
    e.preventDefault()
    var {token, name, decimals, exchangeSymbol, isDividend, dividendPayedIn} = this;
    if(!isDividend){
      dividendPayedIn = null
    }
    if(name == ''){
      this.isShowNameEmpty = true
      this.stateChanged();
      return
    }
    var nextToken = {
      address: token.address, 
      name, 
      decimals, 
      exchangeSymbol,
      isDividend, 
      dividendPayedIn
    }
    if(this.action == 'create'){
      addToken(nextToken)
    }else{
      editToken(token, nextToken)
    }
    onClose();
    appHistory.push('tokens')
    core.dataChanged('tokens')
  }

}

function AddressBlock(s){
  var error = null
  if(s.isShowNotFound){
    error = messages.token.tokenDialog.couldNotFindContract
  }
  if(s.address == null && s.isAddressEdited){
    error = messages.form.invalidAddress
  }
  if(s.shouldShowAlreadyExistsError()){
    error = messages.token.tokenDialog.alreadyExists
  }
  var status = null
  if(s.isChecking){
    status = 'loading'
  }
  if(s.isChecked){
    status = 'success'
  }
  return FormGroup({
    labelMsg: messages.token.tokenDialog.address,
    input: addressInput({
      className: 'form-control', 
      valueLink: s.addressLink(),
      autoFocus: true
    }),
    status,
    error
  })
}

function NameBlock(s){
  return FormGroup({
    labelMsg: messages.token.tokenDialog.name,
    input: nameInput({
      className: 'form-control', 
      valueLink: s.link('name')
    }),
    status: s.name != '' ? 'success' : null,
    error: (s.isShowNameEmpty && s.name == '')
      ? messages.form.cannotBeBlank
      : null
  })
}

function DecimalsBlock(s){
  return FormGroup({
    labelMsg: messages.token.tokenDialog.decimals,
    input: numberInput({
      min: 0,
      step: 1,
      className: 'form-control', 
      valueLink: s.link('decimals'),
      ref: (decimalInput) => {
        s.decimalInput = decimalInput
      }
    }),
    status: s.decimals != null ? 'success' : null,
    error: (s.decimals == null)
      ? messages.form.invalidNumber
      : null
  })
}

function ExchangeSymbols(s){
  return FormGroup({
    labelMsg: messages.token.tokenDialog.exchangeSymbol,
    input: Select({
      simpleValue: true,
      clearable: true,
      searchable: true,
      placeholder: messages.token.tokenDialog.exchangeSymbol,
      options: s.exchangeSymbolOptions,
      valueLink: s.link('exchangeSymbol'),
    }),
  })
}

var TokenDialog = React.createFactory(
core.connect('tokenDialog', React.createClass({
  render(){
    var s = dialogState();
    var buttons = [
      {
        type: 'button',
        btnType: 'default',
        onClick: onClose,
        title: messages.form.cancel,
      },
      {
        disabled: !s.canSubmit(),
        btnType: 'success',
        title: messages.form.save
      }
    ]
    return form({onSubmit: s.onSubmit},
      Modal({
        title: s.action == 'create' 
          ? messages.token.tokenDialog.addToken
          : messages.token.tokenDialog.editToken
        ,
        onClose: onClose,
        buttons
      },
        AddressBlock(s),
        NameBlock(s),
        DecimalsBlock(s),
        ExchangeSymbols(s),
        div({className: 'form-group'},
          label({className: 'control-label'},
            Checkbox({
              label: messages.token.tokenDialog.isDividend,
              checkedLink: s.link('isDividend'),
            })
          )
        ),
        FormGroup({
          labelMsg: messages.token.tokenDialog.dividendPayedIn,
          disabled: !s.isDividend,
          input: TokenSelect({
            tokens: s.allTokens,
            canSelectEther: true,
            disabled: !s.isDividend,
            valueLink: s.link('dividendPayedIn'),
          }),
          noError: true,
        })
      )
    )
  }
})));

function dialogState(){
  return state.ui.tokenDialog;
}

function setDialogState(_state){
  state.ui.tokenDialog = _state;
}

function openTokenDialog({action, onSubmit, token}){
  setDialogState(new TokenState({action, onSubmit, token}))
  Page.showModal(TokenDialog)
}

function onClose(){
  setDialogState(null)
  Page.closeModal()
}

return {openTokenDialog}

});
