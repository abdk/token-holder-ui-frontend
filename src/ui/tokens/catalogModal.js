define(function(require){

var core = require('core')
var {messages} = require('i18n');
var debounce = require('widgets/debounce');
var Modal = require('widgets/modal');
var Preloader = require('widgets/preloader');
var trimInput = require('widgets/trimInput');
var Checkbox = require('widgets/checkbox');
var Page = require('widgets/page');
var {fetchTokenCatalogWithPrices} = require('price');
var {addTokens, loadTokens} = require('token');

var {form, div, span, input, h1, button, label} = React.DOM;

var searchInput = debounce(trimInput(input), {interval: 300})

function findTokens(searchTerm, tokens, existingTokens){
  searchTerm = searchTerm.toLowerCase();
  var matchedTokens;
  if(searchTerm == ''){
    matchedTokens = tokens;
  }else{
    matchedTokens = tokens.filter(({name, description}) => 
      (name || '').toLowerCase().indexOf(searchTerm) != -1
      ||
      (description || '').toLowerCase().indexOf(searchTerm) != -1
    );
  }
  return matchedTokens.filter(({address}) => (
    _.findWhere(existingTokens, {address}) == null
  ));
}

function close(){
  Page.closeModal()
}

var CatalogToken = React.createFactory(function(props){
  return div({className: 'token-catalog-option'}, 
    Checkbox({
      checkedLink: props.selectedLink,
      label: props.token.name
    }),
    (props.token.price != null) && 
      div({style: {float: 'right'}},
        '$' + props.token.price
      )
  )
});


var CatalogModal = React.createFactory(React.createClass({
  
  mixins: [React.addons.LinkedStateMixin],

  getInitialState(){
    fetchTokenCatalogWithPrices().then(tokens => {
      if(this.isMounted()){
        this.setState({tokens, isLoading: false})
      }
    })
    return {
      isLoading: true,
      searchTerm: '',
      selectedTokens: [],
      allTokens: loadTokens(),
    }
  },

  renderModalContent(){
    if(this.state.isLoading){
      return Preloader()
    } else {
      var foundTokens = findTokens(
        this.state.searchTerm, 
        this.state.tokens,
        this.state.allTokens
      )
      return foundTokens.map(token =>
        CatalogToken({
          key: token.address,
          token,
          selectedLink: this.selectedLink(token),
        })
      )
    }
  },

  render(){
    var buttons = [
      {
        type: 'button',
        btnType: 'default',
        onClick: close,
        title: messages.form.cancel,
      },
      {
        disabled: ! this.hasSelectedTokens(),
        btnType: 'success',
        onClick: this.onAdd,
        title: messages.form.add,
      }
    ]
    return form(null, 
      Modal({
        title: messages.token.catalog.selectFromCatalog,
        onClose: close,
        buttons
      }, 
        searchInput({
          autoFocus: true,
          className: 'form-control', 
          valueLink: this.linkState('searchTerm'),
          placeholder: messages.form.search
        }),
        div({className: 'form-group token-catalog-options'}, 
          this.renderModalContent()
        )
      )
    )
  },

  hasSelectedTokens(){
    return this.state.selectedTokens.length !== 0;
  },

  selectedLink(token){
    return {
      value: this.state.selectedTokens.indexOf(token) != -1,
      requestChange: (val) => {
        if(val){
          this.state.selectedTokens.push(token);
        }else{
          this.state.selectedTokens.splice(
            this.state.selectedTokens.indexOf(token), 
            1
          );
        }
        this.forceUpdate();
      }
    }
  },

  onAdd(e){
    e.preventDefault()
    addTokens(this.state.selectedTokens.map(token => 
      _.pick(token, 'address', 'name', 'decimals', 'exchangeSymbol')
    ))
    appHistory.push('tokens')
    core.dataChanged('tokens')
    close()
  }

}));


return {
  openCatalogModal(){
    Page.showModal(CatalogModal)
  }
}


});
