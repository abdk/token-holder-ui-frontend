define(function(require){

var core = require('core')
var storage = require('storage')
var config = require('config')
var messages = require('messages')

var currentLocale
var currentMessages = {}

function getMessages(msgs = messages){
  var result = {}
  for(var key in msgs){
    var value = msgs[key]
    if(typeof(value) == 'object'){
      if(currentLocale in value){
        result[key] = value[currentLocale]
      } else {
        result[key] = getMessages(value)
      }
    } else {
      throw new Error('Invalid state')
    }
  }
  return result
}

function setLocale(loc){
  currentLocale = loc
  moment.locale(loc)
  Object.assign(currentMessages, getMessages())
}

function init(){
  setLocale(
    storage.loadFromMainDB('locale') || 
    config.DEFAULT_LOCALE
  )
}

function initWithDefaultLang(){
  setLocale(config.DEFAULT_LOCALE)
}

return {

  init,

  initWithDefaultLang,

  locales(){
    return ['en', 'ru']
  },

  locale(){
    return currentLocale
  },

  format(msg, value){
    return msg.replace('%', value)
  },
  
  setLocale(loc){
    storage.saveToMainDB('locale', loc)
    setLocale(loc)
    core.stateChanged('localeChanged')
    core.stateChanged('all')
    core.stateChanged('page')
  },

  messages: currentMessages,

}

})
