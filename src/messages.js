define(function(){

return {

  appMenu: {
    balance: {
      en: 'Balance',
      ru: 'Балансы',
    },
    tokens: {
      en: 'Tokens',
      ru: 'Токены',
    },
    transactions: {
      en: 'Transactions',
      ru: 'Транзакции',
    },
  },

  nodeInfo: {
    online: {
      en: 'online',
      ru: 'в сети',
    },
    offline: {
      en: 'offline',
      ru: 'не в сети',
    },
    syncing: {
      en: 'out of sync',
      ru: 'синхронизируется',
    },
    peers: {
      en: 'Peers',
      ru: 'Пиры',
    },
    sinceLastBlock: {
      en: 'Sec since last block',
      ru: 'Секунд со времени последнего блока',
    },
    blocksLeftToSync: {
      en: 'Blocks left to sync',
      ru: 'Осталось синхронизировать блоков',
    },
    completed: {
      en: 'Completed',
      ru: 'Завершено',
    },
    nodeIsOffline: {
      en: 'Node is offline',
      ru: 'Нет связи с сервером',
    },
    networkName: {
      en: 'Network',
      ru: 'Сеть',
    },
    unknown: {
      en: 'Unknown',
      ru: 'Неизвестно',
    },
    blockNumber: {
      en: 'Block number',
      ru: 'Номер блока',
    },
  },

  nodeSettings: {
    nodeSettings: {
      en: 'Node settings',
      ru: 'Настройки сети',
    },
    recentNodes: {
      en: 'Recent nodes',
      ru: 'Недавние сервера',
    },
    selectRecentNode: {
      en: 'Select from recent nodes',
      ru: 'Выберите недавно используемый сервер',
    },
    couldNotConnectToNode: {
      en: 'Could not connect to node',
      ru: 'Нет связи с сервером',
    },
    nodeURL: {
      en: 'Node URL',
      ru: 'URL сервера',
    },
    chainId: {
      en: 'Chain ID',
      ru: 'Chain ID',
    },
  },

  account: {
    copyAddress: {
      en: 'Copy address', 
      ru: 'Скопировать адрес',
    },
    exportAccount: {
      en: 'Export account',
      ru: 'Выгрузить счет',
    },
    accountDetails : {
      en: 'Account details',
      ru: 'Информация о счете',
    },
    transfer: {
      en: 'Transfer',
      ru: 'Перевод средств',
    },
    approve: {
      en: 'Approve',
      ru: 'Позволить снятиe',
    },
    withdraw: {
      en: 'Withdraw',
      ru: 'Снять дивиденды',
    },
    tokens: {
      price: {
        en: 'Price',
        ru: 'Цена',
      },
      byMe: {
        en: 'Approved by me',
        ru: 'Одобрено к снятию у меня',
      },
      toMe: {
        en: 'Approved to me',
        ru: 'Одобрено к снятию для меня',
      },
      dividends: {
        en: 'Dividends',
        ru: 'Дивиденды',
      },
      total: {
        en: 'Total value',
        ru: 'Итого',
      },
      noPriceDataAvailable: {
        en: 'No historical price data available',
        ru: 'Нет данных по историческим ценам',
      },
    }
  },

  transactions: {
    title: {
      en: 'Transactions',
      ru: 'Транзакции',
    },
    noTransactions: {
      en: 'You have no transactions yet',
      ru: 'У вас пока что нет транзакций',
    },
  },

  transaction: {
    transfer: {
      en: 'Transfer funds',
      ru: 'Перевод средств',
    },
    debit: {
      en: 'Debit',
      ru: 'Снятие',
    },
    credit: {
      en: 'Credit',
      ru: 'Приход',
    },
    withdrawReward: {
      en: 'Withdraw reward',
      ru: 'Снятие дивидендов',
    },
    approvedToSpend: {
      en: 'Approved to spend',
      ru: 'Одобрил к снятию',
    },
    wasAllowedToSpend: {
      en: 'Was allowed to spend',
      ru: 'Получил одобрение к снятию',
    },
    transferApprovedFunds: {
      en: 'Transfer approved funds',
      ru: 'Перевел одобренные средства',
    },
    failed: {
      en: 'Transaction failed: out of gas',
      ru: 'Транзакция отвергнута: закончился газ',
    },
    confirmations: {
      en: 'Confirmations',
      ru: 'Подтверждений',
    },
    confirmationsUnknown: {
      en: 'Confimations: unknown',
      ru: 'Подтверждений: неизвестно',
    },
    confirmationsOf: {
      en: 'of',
      ru: 'из',
    },
    confirmed: {
      en: 'Confirmed',
      ru: 'Подтверждено',
    },
    from: {
      en: 'From',
      ru: 'От кого',
    },
    to: {
      en: 'To',
      ru: 'Кому',
    },
    via: {
      en: 'Via',
      ru: 'Через',
    }
  },

  transfer: {
    title: {
      en: 'Send funds',
      ru: 'Перевод средств',
    },
    what: {
      en: 'What',
      ru: 'Что',
    },
    account: {
      en: 'Account',
      ru: 'Счет',
    },
    directly: {
      en: 'Directly',
      ru: 'Напрямую',
    },
    balance: {
      en: 'Balance',
      ru: 'Баланс',
    },
    allowance: {
      en: 'Allowance',
      ru: 'Квота на снятие',
    },
    insufficienBalance: {
      en: "Account does not have enough funds",
      ru: "Недостаточно средств на счете",
    },
    insufficientAllowance: {
      en: "Account is not allowed to spend enough funds",
      ru: "Счету не позволено снять достаточную сумму средств",
    },
    directTransferForbidden: {
      en: "Direct transfer is not possible from account you don't control",
      ru: "Невозможен прямой перевод со счета который вам не принадлежит",
    },
    send: {
      en: 'Send',
      ru: 'Перевести',
    }
  },

  approve: {
    title: {
      en: 'Approve',
      ru: 'Одобрить',
    },
    approve: {
      en: 'Approve',
      ru: 'Одобрить',
    },
  },

  withdraw: {
    title: {
      en: 'Withdraw dividends',
      ru: 'Снять дивиденды',
    },
    insufficientReward: {
      en: "Amount is greater than reward",
      ru: "Сумма превышает дивиденды",
    },
    withdraw: {
      en: 'Withdraw',
      ru: 'Снять',
    },
    account: {
      en: 'Account',
      ru: 'Счет',
    },
    reward: {
      en: 'Reward',
      ru: 'Дивиденды',
    },
  },

  accounts: {
    unknownAddress: {
      en: 'Unknown address',
      ru: 'Неизвестный адрес',
    },
    noAccounts: {
      en: 'You have no accounts or addresses',
      ru: 'У вас пока нет счетов или адресов',
    },
    allAccounts: {
      en: 'All accounts',
      ru: 'Все счета',
    },
    create: {
      en: 'Create account', 
      ru: 'Создать счет',
    },
    import: {
      en: 'Import accounts',
      ru: 'Импортировать счета',
    },
    exportToFile: {
      en: 'Export all accounts to file', 
      ru: 'Выгрузить все счета в файл',
    },
    addToAddressBook: {
      en: 'Add to address book',
      ru: 'Добавить в адресную книгу',
    }
  },

  createAccount: {
    accountName: {
      en: 'Account name',
      ru: 'Название счета',
    },
  },

  editAccount: {
    editAccount: {
      en: 'Edit account',
      ru: 'Редактирование счета',
    },
    name: {
      en: 'Name',
      ru: 'Название',
    },
    currentPassword: {
      en: 'Current password',
      ru: 'Текущий пароль',
    },
  },

  importAccounts: {
    importAccounts: {
      en: 'Import accounts',
      ru: 'Загрузить счета',
    },
    accounts: {
      en: 'Accounts',
      ru: 'Счета',
    },
    notJSON: {
      en: 'Not a JSON file',
      ru: 'Не JSON файл',
    },
    noAddress: {
      en: 'Could not find valid account address',
      ru: 'Не найден адрес счета',
    },
    noPrivate: {
      en: 'Could not find private key',
      ru: 'Не найден закрытый ключ',
    },
    unsupportedVersion: {
      en: 'Unsupported keystore file version',
      ru: 'Неподдерживаемая версия формата файла',
    },
    alreadyExists: {
      en: 'Account already exists',
      ru: 'Счет уже существует',
    },
    openFiles: {
      en: 'Open files',
      ru: 'Открыть файлы',
    },
    import: {
      en: 'Import',
      ru: 'Загрузить',
    },
    errors: {
      en: 'Errors',
      ru: 'Ошибки',
    },
    fileName: {
      en: 'File name',
      ru: 'Имя файла',
    },
    errorMessage: {
      en: 'Error message',
      ru: 'Ошибка',
    },
    noFileSelected: {
      en: 'No files selected',
      ru: 'Не выбрано ни одного файла',
    },
    address: {
      en: 'Address',
      ru: 'Адрес',
    },
    name: {
      en: 'Name',
      ru: 'Имя',
    },
    remove: {
      en: 'Remove',
      ru: 'Удалить',
    }
  },

  addressBook: {
    editAddress: {
      en: 'Edit address',
      ru: 'Редактирование адреса',
    },
    addAddress: {
      en: 'Add address',
      ru: 'Добавить адрес',
    },
    address: {
      en: 'Address',
      ru: 'Адрес',
    },
    name: {
      en: 'Name',
      ru: 'Название',
    },
  },
  
  token: {
    noTokens: {
      en: 'You have no tokens yet',
      ru: 'У вас пока что нет токенов',
    },
    price: {
      ru: 'Цена',
      en: 'Price',
    },
    totalSupply: {
      en: 'Total supply',
      ru: 'Совокупный выпуск',
    },
    copyAddress: {
      en: 'Copy address', 
      ru: 'Скопировать адрес',
    },
    addFromAddress: {
      en: 'Add token from address',
      ru: 'Добавить токен по адресу',
    },
    addFromCatalog: {
      en: 'Add token from catalog',
      ru: 'Добавить токены из каталога',
    },
    tokenDialog: {
      editToken: {
        en: 'Edit token',
        ru: 'Редактирование токена',
      },
      addToken: {
        en: 'Add token',
        ru: 'Добавление токена',
      },
      address: {
        en: 'Address',
        ru: 'Адрес',
      },
      name: {
        en: 'Name',
        ru: 'Название',
      },
      decimals: {
        en: 'Decimals',
        ru: 'Знаков после запятой',
      },
      isDividend: {
        en: 'Is dividend token',
        ru: 'Выплачивает дивиденды',
      },
      dividendPayedIn: {
        en: 'Token dividends are payed in',
        ru: 'Дивиденды выплачиваются в',
      },
      token: {
        en: 'Token',
        ru: 'Токен',
      },
      couldNotFindContract: {
        en: 'Could not find valid token contract',
        ru: 'По указанному адресу не найден токенный контракт',
      },
      alreadyExists: {
        en: 'Token with this address already exists',
        ru: 'Токен с таким адресом уже добавлен',
      },
      exchangeSymbol: {
        en: 'Exchange symbol',
        ru: 'Биржевой символ',
      }
    },

    catalog: {
      selectFromCatalog: {
        en: 'Select tokens from catalog', 
        ru: 'Выбрать токен из каталога',
      }
    }
  },

  syncWithGDrive: {
    disabled: {
      en: 'disabled',
      ru: 'отключено',
    },
    syncing: {
      en: 'syncing',
      ru: 'синхронизируется',
    },
    synced: {
      en: 'synced',
      ru: 'синхронизировано',
    },
    out_of_sync: {
      en: 'out of sync',
      ru: 'не синхронизировано',
    },
    failedToSync: {
      en: 'Failed to synchronize data with Google Drive',
      ru: 'Не удалось сохранить данные в Google Drive',
    },
    ignore: {
      en: 'Ignore',
      ru: 'Закрыть',
    },
  },

  storageSettings: {
    title: {
      en: 'Google Drive settings',
      ru: 'Настройки Google Drive',
    },
    proceed: {
      en: 'Proceed',
      ru: 'Продолжить',
    },
    useGDrive: {
      en: 'Store data in your Google Drive',
      ru: 'Хранить данные в вашем Google Drive',
    },
    useLocalStorage: {
      en: 'Store data in your browser',
      ru: 'Хранить данные в веб-браузере',
    },
    authorize: {
      en: 'Authorize',
      ru: 'Авторизация',
    },
    tryAgain: {
      en: 'Try again',
      ru: 'Попробовать снова',
    },
    authorizing: {
      ru: 'Авторизируемся в Google Drive',
      en: 'Authorizing in Google Drive',
    },
    couldNotAuth: {
      en: 'Could not authorize in Google Drive',
      ru: 'Не удалось авторизоваться в Google Drive',
    },
    couldNotConnect: {
      en: 'Could not download data from Google Drive',
      ru: 'Не удалось получить данные из Google Drive',
    },
    success: {
      en: 'Successfully authorized in Google Drive',
      ru: 'Успешно авторизовались в Google Drive',
    },
  },

  form: {
    cancel: {
      en: 'Cancel',
      ru: 'Отмена',
    },
    ok: {
      en: 'Ok',
      ru: 'Ok',
    },
    apply: {
      en: 'Apply',
      ru: 'Применить',
    },
    create: {
      en: 'Create',
      ru: 'Создать',
    },
    save: {
      en: 'Save',
      ru: 'Сохранить',
    },
    edit: {
      en: 'Edit',
      ru: 'Редактировать',
    },
    add: {
      en: 'Add',
      ru: 'Добавить',
    },
    remove: {
      en: 'Remove',
      ru: 'Удалить',
    },
    tryAgain: {
      en: 'Try again',
      ru: 'Попробовать еще раз',
    },
    cannotBeBlank: {
      en: 'Cannot be blank',
      ru: 'Не должно быть пустым',
    },
    password: {
      accountPassword: {
        en: 'Account password',
        ru: 'Пароль',
      },
      password: {
        en: 'Password',
        ru: 'Пароль',
      },
      repeatPassword: {
        en: 'Repeat password',
        ru: 'Повторите пароль',
      },
      passwordsDoNotMatch: {
        en: 'Passwords do not match',
        ru: 'Пароли не совпадают',
      },
      minLength: {
        en: 'at least % characters',
        ru: 'как минимум % символов',
      },
      invalid: {
        en: 'Invalid password',
        ru: 'Неправильный пароль',
      },
      newPassword: {
        en: 'New password',
        ru: 'Новый пароль',
      }
    },
    invalidAddress: {
      en: 'Invalid address',
      ru: 'Неверный формат адреса',
    },
    invalidNumber: {
      en: 'Invalid number',
      ru: 'Введите число',
    },
    search: {
      en: 'Search',
      ru: 'Поиск',
    },
    token: {
      en: 'Token',
      ru: 'Токен',
    }
  },

  common: {
    loading: {
      en: 'Loading',
      ru: 'Загрузка',
    },
    networkError: {
      en: 'Network problem',
      ru: 'Проблемы с сетью',
    },
    pleaseWait: {
      en: 'Please wait',
      ru: 'Пожалуйста подождите',
    },
    filterByToken: {
      en: 'Filter by token',
      ru: 'Отфильтровать по токену',
    },
    nocontent: {
      en: 'No data found',
      ru: 'Ничего не найдено',
    },
    nodeIsOffline: {
      en: 'Node is offline',
      ru: 'Нет связи с сервером',
    },
    price: {
      unknown: {
        en: 'Unknown',
        ru: 'Неизвестно',
      }
    },
    amount: {
      en: 'Amount',
      ru: 'Сумма',
    },
    timeInterval: {
      title: {
        en: 'Data scale',
        ru: 'Временной интервал',
      },
      week: {
        en: 'Week',
        ru: 'Неделя',
      },
      month: {
        en: 'Month',
        ru: 'Месяц',
      },
      year: {
        en: 'Year',
        ru: 'Год',
      },
    },
    accountSelect: {
      accounts: {
        en: 'Accounts',
        ru: 'Счета',
      },
      addresses: {
        en: 'Addresses',
        ru: 'Адреса',
      },
    },
  },

}


})
