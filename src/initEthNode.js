define(function(require){

var core = require('core')
var storage = require('storage')
var transaction = require('transaction')
var ethNode = require('./ethNode')

function updatePendingTransactions(){
  transaction.updatePendingTransactions().then(() => {
    core.dataChanged('transactions')
  })
}

function onNewBlock(){
  updatePendingTransactions()
  core.dataChanged('all')
}

return function(){
  storage.initChain(ethNode.getChainId())
  ethNode.initWeb3()
  state.nodeInfo = ethNode.NodeInfo()
  core.dataChanged('all')
  if(state.nodeInfoUpdater == null){
    state.nodeInfoUpdater = ethNode.NodeInfoUpdater()
    // update pending txs on app start and subscribe for new
    // block
    updatePendingTransactions()
    core.onDataChanged('newBlock', onNewBlock)
    state.nodeInfoUpdater.runUpdatingNodeInfo()
  }
}

})
