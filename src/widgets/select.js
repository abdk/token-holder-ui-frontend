define(function(){

  var Select = this.Select

return React.createFactory(React.createClass({

  componentDidMount(){
    var toFocus = ReactDOM.findDOMNode(this).getElementsByClassName('Select-input')[0]
    if(this.props.autoFocus){
      toFocus.focus()
    }
  },

  render(){
    var props = this.props
    var value = props.valueLink == null 
      ? props.value
      : props.valueLink.value
    var onChange = props.valueLink == null 
      ? props.onChange
      : props.valueLink.requestChange
    props = Object.assign({}, props, {value, onChange})
    return React.createElement(Select, props)
  },
}))

})
