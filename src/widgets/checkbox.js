define(function(require){

var {form, div, span, input, h1, button, label} = React.DOM;

var checkboxCount = 0;

return React.createFactory(React.createClass({
  componentWillMount(){
    this.checkboxId = 'checkbox' + (++checkboxCount);
  },

  render(){
    return span({className: 'th-checkbox'},
      input({
        type: 'checkbox', 
        id: this.checkboxId,
        checkedLink: this.props.checkedLink
      }),
      label({htmlFor: this.checkboxId}, this.props.label)
    )
  }
}))

})
