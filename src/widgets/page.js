define(function(require){

var {connect, stateChanged, stateChangedAndExec} = require('core');
var {div, span, input, h2} = React.DOM;
var Preloader = require('widgets/preloader');
var HeaderDropdown = require('widgets/headerDropdown');
var {messages} = require('i18n');

var modalContainer = null

var Page = function({header, content, moreOptions}){
  var PageComp = React.createFactory(React.createClass({

    componentWillUnmount(){
      state.ui.modalRenderer = null
      if(modalContainer != null){
        ReactDOM.unmountComponentAtNode(modalContainer)
      }
    },

    componentDidUpdate(){
      if(state.ui.modalRenderer != null){
        ReactDOM.render(state.ui.modalRenderer(), modalContainer)
      } else {
        if(modalContainer != null){
          ReactDOM.unmountComponentAtNode(modalContainer)
        }
      }
    },

    render(){
      var isLoading = state.ui.page && state.ui.page.isLoading
      var isNetworkError = state.ui.page && state.ui.page.isNetworkError
      var hasContent = !isLoading && !isNetworkError
      return div({className: 'page'},
        header &&
          div({className: 'pg-header'},
            div({className: 'title'}, 
              header instanceof Function ? header() : header
            ),
            moreOptions && HeaderDropdown({style: 'large', options: moreOptions()})
          ),
        div({className: 'content'},
          isLoading
            ? Preloader()
            : isNetworkError 
              ? div({className: 'network-error'},
                  messages.common.networkError
                )
              : content(this.props)
        )
      );
    }
  }))

  return connect('page', PageComp) 
};

Page.showModal = function(modalRenderer){
  if(modalContainer == null){
    modalContainer = document.createElement('div')
    modalContainer.setAttribute('style',
      'position:absolute;width:0px;height:0px'
    )
    document.body.appendChild(modalContainer)
  }
  state.ui.modalRenderer = modalRenderer
  stateChanged('page')
}

Page.closeModal = function(){
  state.ui.modalRenderer = null
  stateChanged('page')
}

return Page

});
