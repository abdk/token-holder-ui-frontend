define(function(require){

var {form, div} = React.DOM;

return React.createFactory(function(props){
  var {onSubmit, buttons, children} = props
  return div({className: 'card'},
    form({className: 'body', onSubmit}
    , children 
    , div({className: 'page-form-footer'}
      , ...buttons
      )
    )
  )
});

});
