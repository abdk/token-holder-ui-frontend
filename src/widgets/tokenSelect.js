define(function(require){

var {messages} = require('i18n');
var Select = require('widgets/select')
var {loadTokens} = require('token')

return React.createFactory(React.createClass({
  getInitialState(){
    return {}
  },

  componentWillMount(){
    this.setState({
      tokens: this.props.tokens || loadTokens()
    })
  },

  render(){
    var {
      value, 
      onChange, 
      valueLink, 
      canSelectEther = false,
      disabled,
      clearable = false,
      className,
      placeholder = messages.form.token,
      autoFocus
    } = this.props
    value = valueLink == null 
      ? value 
      : valueLink.value
    onChange = onChange || valueLink.requestChange
    var options = this.state.tokens.map(t =>
      ({label: t.name, value: t.address})
    )
    if(canSelectEther){
      options.unshift({label: 'Ether', value: 'ether'})
    }
    return Select({
      simpleValue: true,
      className,
      disabled,
      clearable,
      searchable: false,
      placeholder,
      value,
      onChange,
      options,
      autoFocus,
    })
  }
}))

})
