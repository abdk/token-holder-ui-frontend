define(function(require){

var input = React.DOM.input
var validatedInput = require('./validatedInput')
var {normalizeAddress} = require('utils');

function isValidAddress(address){
  return address != '' && web3.isAddress(address);
}

return validatedInput(input, {

  toString(address){
    if(address == null){
      return ''
    } else {
      return normalizeAddress(address)
    }
  },

  fromString(str){
    str = str.trim()
    if(isValidAddress(str)){
      return normalizeAddress(str)
    } else {
      return null
    }
  }


});

})
