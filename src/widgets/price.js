define(function(require){

var {messages} = require('i18n')

var {span} = React.DOM

function format(val){
  if(val.constructor.name == 'BigNumber'){
    return val.toFormat(2)
  }else if(typeof(val) == 'number'){
    return val.toFixed(2)
  }else{
    throw 'value ' + val + ' is not number or bignumber'
  }
}

function formatUSD(amount){
  var s = format(amount)
  if(s.indexOf('-') == 0){
    return '- ' + '$' + s.slice(1)
  } else {
    return '$' + s
  }
}

return function({amount}){
  return isNaN(amount)
  ? span({style: {color: 'red'}},
      messages.common.price.unknown
    )
  : span(null, formatUSD(amount))
}

})
