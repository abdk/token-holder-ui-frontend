define(function(require){

var {form, div, span, input, h3, button, i, h2} = React.DOM;

return React.createFactory(function (props){
  return div({className: 'card box ' + (props.className || '')},
    div({className: 'header'},
      span({className: 'title'}, props.title),
      props.onClose != null &&
        button({type: 'button', onClick: props.onClose},
          '×'
        )
    ),
    div({className: 'body'},
      props.children
    )
  )
})

});
