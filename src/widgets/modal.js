define(function(require){

var {div, span, input, h4, button, p} = React.DOM
var Button = require('widgets/button')

// http://stackoverflow.com/a/21811463
function constraintTabStopToElementChildren(element){
  var inputs = $(element)
    .find('select, input, textarea, button, a, [role=combobox]')
    .filter(':not(.close)')
  var firstInput = inputs.first();
  var lastInput = inputs.last();

  /*set focus on first input*/
  firstInput.focus();

  /*redirect last tab to first input*/
  lastInput.on('keydown', function (e) {
   if ((e.which === 9 && !e.shiftKey)) {
     e.preventDefault();
     firstInput.focus();
   }
  });

  /*redirect first shift+tab to last input*/
  firstInput.on('keydown', function (e) {
    if ((e.which === 9 && e.shiftKey)) {
      e.preventDefault();
      lastInput.focus();
    }
  })
}

return React.createFactory(React.createClass({
  render(){
    return div({
      ref: (container) => this.container = container, 
      className: 'modal-container', 
      onClick: this.onClickOverlay
    },
      div({className: 'modal-content'}, 
        div({className: 'modal-header'},
          this.props.onClose != null &&
            button({type: 'button', className: 'close', onClick: this.props.onClose},
              span(null,'×')
            ),
          h4({className: 'modal-title'}, this.props.title)
        ),
        div({className: 'modal-body ' + (this.props.className || '')},
          this.props.children
        ),
        this.renderButtons()
      )
    )
  },

  renderButtons(){
    if(this.props.buttons == null){
      return null;
    }
    var buttons = this.props.buttons.map((p, i) => 
      Button(Object.assign(p, {key: i}))
    )
    return div({className: 'modal-footer'}, buttons)
  },

  componentDidMount() {
    document.addEventListener('keyup', this.onKey);
    constraintTabStopToElementChildren(ReactDOM.findDOMNode(this))
  },

  componentWillUnmount() {
    document.removeEventListener('keyup', this.onKey);
  },

  onClickOverlay(e){
    if(this.props.onClose != null && e.target === this.container){
      this.props.onClose();
    }
  },

  onKey(event) {
    if (this.props.onClose != null && event.keyCode == 27){
      this.props.onClose();
    }
  }

}));

});
