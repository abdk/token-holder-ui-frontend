define(function(require){

var inputWithIcon = require('widgets/inputWithIcon');

return React.createFactory(React.createClass({

  getInitialState(){
    return {isHidden: true};
  },

  render(){
    var p = Object.assign({
      type: this.state.isHidden ? 'password' : 'text',
      icon: 'eye',
      onIconClick: this.toggle,
      name: 'password'
    }, this.props);
    return inputWithIcon(p)
  },

  toggle(){
    this.setState({isHidden: !this.state.isHidden});
  }

}))

});
