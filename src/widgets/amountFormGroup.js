define(function(require){

var {messages} = require('i18n')
var FormGroup = require('widgets/formGroup')
var bigNumberInput = require('widgets/bigNumberInput')

return React.createFactory(React.createClass({

  getInitialState(){
    return {wasChanged: false}
  },

  onChange(val){
    this.setState({wasChanged: true})
    this.props.valueLink.requestChange(val)
  },

  isErrorShown(){
    return this.state.wasChanged && this.props.valueLink.value == null
  },

  error(){
    return this.props.error != null
      ? this.props.error
      : this.isErrorShown()
        ? messages.form.invalidNumber
        : null
      
  },

  render(){
    var {note, status} = this.props
    var valueLink = {
      value: this.props.valueLink.value,
      requestChange: this.onChange,
    }
    return FormGroup({
      labelMsg: messages.common.amount,
      input: this.props.input 
        ? this.props.input(valueLink)
        : bigNumberInput({
            className: 'form-control',
            valueLink,
          }),
      error: this.error(),
      note,
      status
    })
  },

}))

})
