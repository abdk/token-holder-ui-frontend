define(function(require){

var {div} = React.DOM

return React.createFactory(function(props){
  var {progress, text} = props
  var width = progress * 100 + '%'
  return div({className: 'progress'}
  , div({className: 'progress-bar', style: {width} }
    , text
    )
  )
});

})
