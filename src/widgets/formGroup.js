define(function(){

var {form, div, span, h1, button, label, input, i} = React.DOM;

return React.createFactory(function FormGroup({
  labelMsg, 
  error, 
  input, 
  status,
  noError,
  children,
  invisible = false,
  disabled,
  note
}){
  var icon = null
  if(error != null && status == null){
    status = 'error'
  }
  if(status == 'loading'){
    icon = 'fa-refresh fa-spin'
  }
  if(status == 'success'){
    icon = 'fa-check'
  }
  if(status == 'error'){
    icon = 'fa-times-circle-o'
  }
  if(status == 'warning'){
    icon = 'fa-exclamation-triangle'
  }
  var style = invisible ? {visibility: 'hidden'} : null;
  return div({
    style, 
    className: 'form-group ' + (disabled ? 'disabled ' : '')
      + (status == 'error' ? ' has-error' : '')
      + (status == 'warning' ? ' has-warning' : '')
      + (status == 'success' ? ' has-success' : '')
  },

    // used in passwordFormGroup
    children,

    label({className: 'control-label'},
      labelMsg
      , icon == null
        ? null
        : i({className: 'status-icon fa '+icon})
      , note && span({className: 'note'}
        , note
      )
    ),
    input,
    noError
      ? null
      : label({className: 'help-block'}, error || "\u200b")
  )
})

})
