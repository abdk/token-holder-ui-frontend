define(function(require){

var FormGroup = require('widgets/formGroup');
var {messages} = require('i18n')
var Password = require('widgets/password')
var {input} = React.DOM

var PasswordFormGroup = React.createFactory(function({
  valueLink, 
  errorLink,
  address
}){
  var passwordLink = {
    value: valueLink.value,
    requestChange(password){
      valueLink.requestChange(password)
      errorLink.requestChange(false)
    }
  }
  return FormGroup({
    labelMsg: messages.form.password.accountPassword,
    input: Password({valueLink: passwordLink}),
    error: errorLink.value
      ? 'Invalid password'
      : null
  }
    // provide invisible username input so browser can autocomplete password
  , input({name: 'username', value: address, tabIndex: 1000, style: {
      position: 'absolute',
      bottom: '9999px',
    }})
  )

})

var PasswordFormGroupRemountWrapper = React.createFactory(function(props){
  // using key we remount component whenever address change to
  // trigger autofill for new address
  return PasswordFormGroup(Object.assign({key: props.address}, props))
})

return PasswordFormGroupRemountWrapper;


});
