define(function(require){

var {form, div, span, h1, button, label, input, i} = React.DOM;

return React.createFactory(function(props){
  var {invisible = false, message, status, style} = props
  var icon = {
    loading: 'fa-refresh fa-spin',
    success: 'fa-check',
    error: 'fa-times-circle-o',
  }[status]
  var cssStyle = invisible ? {visibility: 'hidden'} : null
  var className = 'form-group '
    + (status == 'error' ? ' has-error' : '')
    + (status == 'success' ? ' has-success' : '')
  if(style == 'large'){
    className += ' form-message-large'
  }
  return div({style: cssStyle, className},
    div({className: 'help-block'}, 
      i({className: 'status-icon fa ' + icon}),
      message == null ? '\u200b' : message
    )
  );
});

});
