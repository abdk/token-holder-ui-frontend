define(function(require){

var Link = React.createFactory(ReactRouter.Link)
var {span} = React.DOM

return React.createFactory(function(token){
  // TODO uncomment after implement token page
  //  if(token.name == null){
  //    // unknown token, don't make link
  //    return 'Unknown token'
  //  } else {
  //    return Link({to: 'tokens/ ' + token.address}, token.name)
  //  }
  return span(null, token.name || token.address)
})

})
