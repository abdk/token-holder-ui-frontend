define(function(require){

var dropdownContainer = null

return React.createFactory(React.createClass({

  getInitialState(){
    return {isOpened: false}
  },

  componentDidMount() {
    document.addEventListener('keyup', this.onKey);
    document.addEventListener('click', this.onClickDocument);
  },

  componentWillUnmount() {
    document.removeEventListener('keyup', this.onKey);
    document.removeEventListener('click', this.onClickDocument);
    if(dropdownContainer != null){
      ReactDOM.unmountComponentAtNode(dropdownContainer)
    }
  },

  componentDidUpdate(prevProps, prevState){
    if(dropdownContainer == null && this.state.isOpened){
      dropdownContainer = document.createElement('div')
      dropdownContainer.setAttribute('style',
        'position:fixed;width:0px;height:0px;z-index:1'
      )
      document.body.appendChild(dropdownContainer)
    }
    if(this.state.isOpened){
      this.renderDropdown()
    }
    if(!prevState.isOpened && this.state.isOpened){
      var el = ReactDOM.findDOMNode(this.__child)
      var {left, top} = $(el).offset()
      top  = top + el.clientHeight
      if(this.props.direction == 'left'){
        left = left + el.clientWidth
      }
      $(dropdownContainer).css({left,top})
    }
    if(prevState.isOpened && !this.state.isOpened){
      ReactDOM.unmountComponentAtNode(dropdownContainer)
    }
  },

  onKey(event){
    if (event.keyCode == 27){
      this.close()
    }
  },

  onClickDocument(e){
    if(!e.__bubbledFromDropdown){
      this.close()
    }
  },

  onClickButton(e){
    e.nativeEvent.__bubbledFromDropdown = true
    this.setState({isOpened: !this.state.isOpened})
  },

  close(){
    if(this.state.isOpened){
      this.setState({isOpened: false})
      this.props.onClose && this.props.onClose()
    }
  },

  renderDropdown(){
    var drop = this.props.dropdown(this.close)
    var dd = React.cloneElement(
      drop,
      {
        className: 'th-dropdown ' + drop.props.className,
        onClick(e){
          e.nativeEvent.__bubbledFromDropdown = true
        },
        style: this.props.direction == 'left'
        ? Object.assign(
            {},
            drop.props.style, 
            {transform: 'translateX(-100%)'}
          )
        : drop.props.style
      }
    )
    ReactDOM.render(dd, dropdownContainer)
  },

  render(){
    var child = React.Children.only(this.props.children)
    return React.cloneElement(
      child,
      {
        onClick: this.onClickButton, 
        ref: (node) => (this.__child = node), 
      },
      child.props.children
    )
  }
}))

})
