define(function(require){

var {toHumanizedValue} = require('utils')
var TokenLink = require('widgets/tokenLink')
var Decimals = require('widgets/decimals')
var {span, div} = React.DOM

return React.createFactory(function(props){
  var {amount, token, withTokenName = false, withDecimals = true} = props
  return span({className: 'amount'}, 
    toHumanizedValue(amount, token),
    withDecimals && Decimals({token}),
    withTokenName && ' ',
    withTokenName && (
      token == 'ether'
      ? 'Ether'
      : TokenLink(token)
    )
  )
})

});
