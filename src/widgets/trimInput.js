define(function(require){

var validatedInput = require('./validatedInput')

return function trimInput(InputComp){
  return validatedInput(InputComp, {
    fromString(str){
      return str.trim()
    },
    toString(val){
      return val
    }
  })
}

});
