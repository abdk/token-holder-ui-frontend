define(function(require){

var AmountFormGroup = require('widgets/amountFormGroup')
var bigNumberInput = require('widgets/bigNumberInput')
var Decimals = require('widgets/decimals')
var {span, div} = React.DOM

return React.createFactory(React.createClass({

  getInitialState(){
    return {}
  },
  
  getDecimals(){
    return this.props.token == 'ether'
      ? 18
      : this.props.token.decimals || 0
  },

  hasDecimalsError(value){
    return value != null && value.decimalPlaces() > this.getDecimals()
  },

  error(){
    return this.props.error != null
      ? this.props.error
      : this.hasDecimalsError(this.state.value)
          ? "Too many decimal places"
          : null
      
  },

  render(){
    var {value, requestChange} = this.props.valueLink
    var decimals = this.getDecimals()
    var valueLink = {
      value: value && value.shift(-decimals),
      requestChange: value => {
        this.setState({value}, () =>
          requestChange(
            this.hasDecimalsError(value)
            ? null
            : value && value.shift(decimals)
          )
        )
      },
    }
    return AmountFormGroup(
      Object.assign({}, this.props, {
        valueLink, 
        token: null,
        error: this.error(),
        input: valueLink => div({style:{
          display: 'flex',
        }},
          bigNumberInput({
            className: 'form-control',
            valueLink,
          }),
          Decimals({token: this.props.token})
        )
      })
    )
  },

}))

})
