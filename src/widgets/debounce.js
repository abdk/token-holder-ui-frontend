define(function() {

return function debounce(ReactComponent, {interval}) {
  return React.createFactory(React.createClass({
    getInitialState() {
      return {
        value: this.props.valueLink.value,
        debouncedChange: _.debounce(this.props.valueLink.requestChange, interval)
      };
    },

    render() {
      var valueLink = {
        value: this.state.value,
        requestChange: this.requestChange
      };
      return ReactComponent(_.extend({}, this.props, {valueLink}));
    },

    requestChange(value) {
      this.setState({value});
      this.state.debouncedChange(value);
    }
  }));
};

});
