define(function(require){

function fromString(str){
  str = str.trim()
  if(str == ''){
    return null
  }
  try {
    var result = web3.toBigNumber(str)
    if(result.lessThan(web3.toBigNumber(0))){
      // TODO compare with props.min instead of 0
      return null
    }
    return result
  }catch(e){
    return null
  }
}

return React.createFactory(function(props){
  var p = Object.assign({
    type: 'number',
    step: 'any',
    min: '0'
  }, props)
  p.onChange = function(e){
    var num = fromString(e.target.value)
    props.valueLink.requestChange(num)
  }
  delete p.valueLink
  return React.DOM.input(p);
})

});
