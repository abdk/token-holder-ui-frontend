define(function(require){

var {form, div, span, input, h1, button, label} = React.DOM;

var radioCount = 0;

return React.createFactory(React.createClass({
  componentWillMount(){
    this.radioId = 'radio_' + (++radioCount);
  },

  render(){
    return span(null,
      input({
        type: 'radio', 
        id: this.radioId,
        name: this.props.name,
        checked: this.props.checked,
        onChange: this.props.onChange
      }),
      label({htmlFor: this.radioId}, this.props.label)
    )
  }
}))

})
