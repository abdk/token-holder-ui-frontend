define(function(require){

var Account = require('widgets/account');
var Link = React.createFactory(ReactRouter.Link);
var {form, div, span, input, h1, button, label} = React.DOM;

return React.createFactory(function(account){
  return account.name == null 
    // account was removed, don't make it link
    ? span(null,
        Account(account)
      )
    : Link({
        to: '/accounts/'+account.address
      },
        Account(account)
      )
})

});
