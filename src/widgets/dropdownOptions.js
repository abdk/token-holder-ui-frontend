define(function(require){

var Dropdown = require('widgets/dropdown');

var {div} = React.DOM

return React.createFactory(function(props){
  return Dropdown({
    dropdown: (close) => div(null,
      ...props.options.map(({onSelect, title}) => (
        div({
          className: 'th-dropdown-option', 
          onClick(){
            close()
            onSelect()
          }
        },
          title
        )
      ))
    )
  },
    props.children
  )


})

})
