define(function(require){

var {messages} = require('i18n');
var Dropdown = require('./dropdown');
var debounce = require('./debounce');
var {normalizeAddress} = require('utils');

var {form, div, span, input, button, table, tbody, tr, td, th} = React.DOM;
var debouncedInput = debounce(input, {interval: 200})

function renderAccount(account){
  return div({className: 'rich-acc-select'},
    span({className: 'rich-acc-select-name'}, account.name),
    span({className: 'rich-acc-select-address'}, account.address),
    span({className: 'rich-acc-select-drop'}, '\u25BC')
  )
}

function menuOption(name, address, isActive, onSelect, close){
  return div({
    onClick: () => {close(); onSelect(); }, 
    className: 'th-dropdown-option ' + (isActive && 'active')
  },
    span({className: 'rich-acc-select-name'}, name),
    span({className: 'rich-acc-select-address'}, address)
  )
}

function separator(title){
  return div({className: 'th-dropdown-option'}, 
    div({className: 'separator'}, 
      span({className: 'title'}, title)
    )
  )
}

function accountsGroup(title, accounts, selectedAddress, onChange, close){
  if(accounts.length == 0){
    return []
  }
  return [
    separator(title),
    ...accounts.map(acc => 
      menuOption(
        acc.name,
        acc.address,
        selectedAddress == acc.address,
        onChange.bind(null, acc.address),
        close
      )
    ),
  ]
}

var RichAccountSelect = React.createFactory(React.createClass({

  mixins: [React.addons.LinkedStateMixin],

  getInitialState(){
    return { query: ''}
  },

  presentation(){
    var {accounts, address} = this.props
    var account = accounts.find(acc => acc.address == address)
    var query = this.state.query.toLowerCase().trim()
    var queryAsAddress = normalizeAddress(query)
    var filteredAccounts = query == ''
      ? accounts
      : accounts.filter(acc => 
          false
          || acc.name.toLowerCase().indexOf(query) != -1
          || acc.address == queryAsAddress
        )
    var mineAccounts = filteredAccounts.filter(acc => acc.isMine)
    var addresses = filteredAccounts.filter(acc => !acc.isMine)
    var newAddress = 
      web3.isAddress(queryAsAddress) && filteredAccounts.length == 0
      ? queryAsAddress
      : null
    var addressToSelectOnSubmit = newAddress != null
      ? newAddress
      : filteredAccounts.length == 1
        ? filteredAccounts[0].address
        : null
    return {
      mineAccounts,
      addresses,
      isAllAccounts: address == null,
      account,
      address,
      newAddress,
      addressToSelectOnSubmit,
    }
  },

  render(){

    var onSubmitSearch = (address) => (e) => {
      e.preventDefault()
      if(address != null){
        this.props.onChange(address)
      }
    }

    var p = this.presentation()

    return Dropdown({
      onClose: () => this.setState({query: ''}),
      dropdown: (close) => {
        return div({className: 'th-dropdown rich-account-dropdown'}, 
          form({onSubmit: onSubmitSearch(p.addressToSelectOnSubmit)},
            debouncedInput({
              className: 'form-control',
              autoFocus: true,
              valueLink: this.linkState('query')
            })
          ),
            menuOption(
              messages.accounts.allAccounts,
              null,
              p.isAllAccounts,
              this.props.onChange.bind(null, null),
              close
            ),
            ...accountsGroup(
              messages.common.accountSelect.accounts,
              p.mineAccounts,
              p.address,
              this.props.onChange,
              close
            ),
            ...accountsGroup(
              messages.common.accountSelect.addresses,
              p.addresses,
              p.address,
              this.props.onChange,
              close
            ),
            p.newAddress != null &&
              menuOption(
                messages.accounts.unknownAddress, 
                p.newAddress, 
                false,
                this.props.onChange.bind(null, p.newAddress),
                close
              )
        )
      }
    },
      p.isAllAccounts
      ? renderAccount({name: messages.accounts.allAccounts, address: null})
      : p.account == null
        ? renderAccount({name: messages.accounts.unknownAddress, address: p.address})
        : renderAccount(p.account)
    )
  },
}))

return RichAccountSelect

})
