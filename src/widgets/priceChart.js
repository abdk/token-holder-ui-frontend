define(function(require){
 
var core = require('core')

var chartOptions = (dates, values, unit) => ({
  type: 'line',
  fill: false,
  data: {
    labels: dates,
    datasets: [{
      data: values,
      fill: false,
      borderColor: 'blue',
      borderWidth: 3,
      pointRadius: 0,
      lineTension: 0,
    }]
  },
  options: {
    maintainAspectRatio: false,
    animation: false,
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        type: "time",
        time: {
          format: 'MM/DD/YYYY HH:mm',
          tooltipFormat: 'll HH:mm',
          unit,
        },
        scaleLabel: {
          display: false,
        }
      }],
    }
  }
})

return React.createFactory(React.createClass({

  shouldComponentUpdate(){
    return false
  },

  update(){
    this.chart.update()
  },

  componentDidMount(){
    core.on('localeChanged', this.update)
    var dates = this.props.prices.map(d => new Date(d.time*1000))
    var values = this.props.prices.map(d => d.close)
    var unit = {
      week: 'day',
      month: 'week',
      year: 'month',
    }[this.props.interval]
    var options = chartOptions(dates, values, unit)
    this.chart = new Chart(ReactDOM.findDOMNode(this), options)
  },

  componentWillUnmount(){
    core.off('localeChanged', this.update)
  },

  render(){
    return React.DOM.canvas()
  }

}))

})
