define(function(require){

var {div, span, input, h2, button, i, label, small, ul, li, a} = React.DOM;

var dropDownCounter = 0;

return React.createFactory(React.createClass({

  componentWillMount(){
    this.dropdownId = 'dropdown' + (++dropDownCounter)
  },

  componentDidMount(){
    $(ReactDOM.findDOMNode(this)).find('[data-activates='+this.dropdownId+']')
      .dropdown({
        constrainWidth: false
      })
  },

  render(){
    var options = this.props.options;
    return div({className: "header-dropdown"},
      div({className: "dropdown"},
        a({"data-activates": this.dropdownId},
          this.props.style == 'large'
            ? div({className: 'actionButton'},
              i({className: 'fa fa-ellipsis-v'})
            )
            : i({className: "material-icons"}, 'more_vert')
        ),
        ul({className: "dropdown-menu", id: this.dropdownId},
          options.map((option, i) => (
            li({onClick: option.onClick, key: i}, 
              a(null, option.name)
            )
          ))
        )
      )
    )
  }
}));

});
