define(function(require){

var Account = require('./account');
var HeaderDropdown = require('./headerDropdown');

var {div, span, input, h2, button, i, label, small, ul, li, a} = React.DOM;

return React.createFactory(function(props){
  var {header, title, subTitle, color, options, entries, footer} = props
  header = header || h2(null, title, small(null, subTitle))
  var className = "col-lg-4 col-md-4 col-sm-6 col-xs-12 "
  if(props.className){
    className += props.className
  }
  return div({className},
    div({className: "card"},
      div({className: "header bg-" + color},
        header,
        options && HeaderDropdown({options})
      ),
      entries &&
        div({className: "body"},
          entries.map(({key, value}, i) => (
            div({className: 'card-info-block', key: i}, 
              div({className: 'card-entry'}, 
                div({className: 'key'},
                  key
                ),
                div({className: 'value'},
                  value
                )
              )
            )
          ))
        )
      , footer &&
        div({className: 'card-action'},
          ...footer
        )
    )
  )
})

});
