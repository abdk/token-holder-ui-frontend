define(function(require){

var Identicon = require('widgets/identicon');
var {form, div, span, input, h1, button, label} = React.DOM;

return React.createFactory(function(account){
  return div({className: 'account'}, 
    Identicon({address: account.address}),
    account.name != null &&
      span({className: 'account-name'}, account.name),
    account.name == null &&
      span({className: 'account-address'},
        web3.toChecksumAddress(account.address)
      )
  )
})

});
