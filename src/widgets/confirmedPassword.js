define(function(require){

var i18n = require('i18n');
var {messages} = require('i18n');
var {MIN_PASSWORD_LENGTH} = require('config');
var FormGroup = require('widgets/formGroup');
var Password = require('widgets/password');
var {label, div, span, input, h1, button, form} = React.DOM;

return React.createFactory(React.createClass({

  getInitialState(){
    return {
      password1: '',
      password2: '',
    }
  },

  passwordLink(prop){
    return {
      value: this.state[prop],
      requestChange:(val) => {
        var update = {}
        update[prop] = val
        this.setState(update, () => {
          this.props.onChange({
            password:
              this.isSecondPasswordOk()
              ?  this.state.password1
              : null
            ,
            doNotMatch: this.isFirstPasswordStrong() &&
              !this.isSecondPasswordOk()
          })
        })
      }
    }
  },

  isFirstPasswordStrong() {
    return this.state.password1.length >= MIN_PASSWORD_LENGTH
  },

  isSecondPasswordOk() {
    return this.isFirstPasswordStrong() && this.doPasswordsMatch()
  },

  doPasswordsMatch(){
    return this.state.password1 == this.state.password2
  },
  
  render(){
    var {
      label = messages.form.password.password, 
      isShowError, 
      address
    } = this.props
    return div(null,
      input({name: 'username', value: address, tabIndex: 1000, style: {
        position: 'absolute',
        bottom: '9999px',
      }}),
      FormGroup({
        input: Password({valueLink: this.passwordLink('password1')}),
        status: this.isFirstPasswordStrong() ? 'success' : null,
        labelMsg: 
          label + ' ('  +
          i18n.format(
            messages.form.password.minLength,
            MIN_PASSWORD_LENGTH
          ) + ')',
        noError: true
      }),
      FormGroup({
        input: Password({valueLink: this.passwordLink('password2')}),
        status: this.isSecondPasswordOk() ? 'success' : null,
        labelMsg: messages.form.password.repeatPassword,
        error: (!this.doPasswordsMatch() && isShowError) 
          ? messages.form.password.passwordsDoNotMatch
          : null
      })
    )
  }
}))

})
