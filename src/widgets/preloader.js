define(function(require){

var {messages} = require('i18n');
var {div, i, span} = React.DOM;

return React.createFactory(function(){
  return div({className: 'preloader'}, 
    i({className: 'fa fa-refresh fa-spin'}),
    span(null, messages.common.pleaseWait)
  )
})

});
