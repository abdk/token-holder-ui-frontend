define(function(require){

var {label, input, div} = React.DOM

var inputFieldIdCount = 0;

return React.createFactory(React.createClass({

  componentWillMount(){
    this.inputId = ++inputFieldIdCount;
  },

  render(){
    return div({className: 'input-field col s6'}
    , input({id: this.inputId})
    , label({htmlFor: this.inputId}, this.props.label)
    )
  }
}))

});
