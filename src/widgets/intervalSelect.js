define(function(require){

var {messages} = require('i18n')
var {span} = React.DOM

return React.createFactory(function(props){
  return span({className: 'interval-select'},
    span(null,
      messages.common.timeInterval.title + ':'
    ),
    ...['week', 'month', 'year'].map(interval =>
      span({
        className: 
          'interval-option ' 
          + (interval == props.value ? 'active' : null),
        onClick: 
          props.onChange.bind(null, interval),
      },
        messages.common.timeInterval[interval] 
      )
    )
  )
})

})
