define(function(require){

return React.createFactory(React.createClass({

  getInitialState(){
    return {hasOverflow: false}
  },

  componentDidMount(){
    var node = ReactDOM.findDOMNode(this)
    if(node.offsetWidth < node.scrollWidth){
      // overflowed
      this.setState({hasOverflow: true})
    }
  },

  render(){
    return React.cloneElement(this.props.container, null,
      this.state.hasOverflow
        ? this.props.briefView()
        : this.props.fullView()
    )
  },

}))

})
