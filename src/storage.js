define(function(require){

var config = require('config')
var core = require('core')
var browserStorage = require('browserStorage')
var googleDrive = require('googleDrive')

var storage = {
  isInitialized: browserStorage.isInitialized,
  loadFromMainDB: browserStorage.loadFromMainDB, 
  load: browserStorage.load, 
  fetchIDB: browserStorage.fetchIDB,
  addIDB: addIDB,
  saveToMainDB,
  create,
  init, 
  initChain, 
  save, 
  isUsingGDrive,
  setUsingGDrive,
  loadFromGDrive,
  syncWithGDriveStatus,
  flush,
};

var realm = 'production'

// override for integration tests, not to mess up real data
function setRealm(_realm){
  realm = _realm
}

var flushPromise
var isSyncingWithGDrive = false

function isUsingGDrive(){
  return config.IS_GOOGLE_DRIVE_ENABLED() && JSON.parse(localStorage.isUsingGDrive)
}

function isSyncedWithGDrive(){
  return JSON.parse(localStorage.isSyncedWithGDrive)
}

function syncWithGDriveStatus(){
  if(!isUsingGDrive()){
    return 'disabled'
  }
  if(isSyncingWithGDrive){
    return 'syncing'
  }
  if(isSyncedWithGDrive()){
    return 'synced'
  } else {
    return 'out_of_sync'
  }
}

// TODO queue flushes
function flush(){
  if(!isUsingGDrive()){
    return
  }
  // Don't issue request for save to GDrive immediately. Wait for other save
  // ops and flush them all
  if(flushPromise == null){
    localStorage.isSyncedWithGDrive = false
    isSyncingWithGDrive = true
    core.stateChanged('syncGDrive')
    flushPromise = Promise.resolve().then(() => {
      flushPromise = null
      saveToGDrive()
        .then(() => {
          localStorage.isSyncedWithGDrive = true
          isSyncingWithGDrive = false
          core.stateChanged('syncGDrive')
        })
        .catch(e => {
          error('error saving to gdrive', e)
          isSyncingWithGDrive = false
          core.stateChanged('syncGDrive')
        })
    })
  }
}

function saveToGDrive(){
  return browserStorage.dumpDB().then(db =>
    googleDrive.saveFile(realm + '_db', JSON.stringify(db))
  )
}

function loadFromGDrive(){
  log('loading from gdrive')
  return googleDrive.readFile(realm + '_db')
}

function init(){
  log('init storage')
  var isDataChanged = browserStorage.init()
  if(isDataChanged){
    flush()
  }
}

function create({isUsingGDrive, dbDump}){
  log('create storage', {
    isUsingGDrive,
    dbDumpIsNull: dbDump == null,
  })
  localStorage.isUsingGDrive = isUsingGDrive
  return Promise.resolve(
    dbDump != null
    ? browserStorage.restoreDB(dbDump)
    : null
  ).then(() => {
    localStorage.isSyncedWithGDrive = true
    init()
    core.dataChanged('all')
  })
}

function setUsingGDrive(options){
  var dbDump = options.dbDump
  var newIsUsingGDrive = options.isUsingGDrive
  var currentIsUsingGDrive = isUsingGDrive()
  log('setUsingGDrive', {
    newIsUsingGDrive,
    currentIsUsingGDrive,
    dbDumpIsNull: dbDump == null,
  })
  localStorage.isUsingGDrive = newIsUsingGDrive
  if(newIsUsingGDrive){
    if(currentIsUsingGDrive){
      if(!isSyncedWithGDrive()){
        // try flush again
        flush()
      }
    } else {
      create(options)
      if(dbDump == null){
        flush()
      }
    }
  } else {
    delete localStorage.isSyncedWithGDrive
    // choosed to use localStorage, do nothing
  }
  core.stateChanged('syncGDrive')
}

function saveToMainDB(prop, val){
  browserStorage.saveToMainDB(prop, val)
  flush()
}

function initChain(_chainId){
  var isDataChanged = browserStorage.initChain.apply(null, arguments)
  if(isDataChanged){
    flush()
  }
}

function save(prop, val){
  browserStorage.save.apply(null, arguments)
  flush()
}

function addIDB(prop, record){
  browserStorage.addIDB.apply(null, arguments)
    .then(flush)
}

return storage

})
