define(function(require){

var {tokenCatalogWithDetailsPromise} = require('./tokenCatalog')
var token = require('token')
var coinList = require('data/coinList')
var {promisedProperties} = require('utils')

var URL_BASE = 'https://min-api.cryptocompare.com/data'

function call(method, params){
  var paramStr = ''
  for(var k in params){
    var val = params[k]
    if(paramStr != ''){
      paramStr += '&'
    }
    paramStr += k + '=' + ((Array.isArray(val)) ? val.join(',') : val)
  }
  var url = URL_BASE + '/' + method + '?' + paramStr
  return fetch(url).then(resp => resp.json())
}

function fetchTokenCatalogWithPrices(){
  return tokenCatalogWithDetailsPromise().then(tokenCatalog => {
    return call('pricemulti', {
      fsyms: tokenCatalog.map(t => t.exchangeSymbol),
      tsyms: ['USD'],
      // tsyms: ['USD', 'ETH'],
    })
    .then(res => 
      tokenCatalog.map(t => {
        return Object.assign({}, t, {
          price: res[t.exchangeSymbol] == null
            ? null
            : res[t.exchangeSymbol]['USD']
        })
      })
    )
    .catch(() => tokenCatalog)
  })
}

function fetchEtherPrice(){
  return call('price', {fsym: 'ETH', tsyms: 'USD'})
    .then(price => 
      typeof(price.USD) == 'number'
        ? price.USD 
        : NaN
    )
    .catch(() => NaN)
}

function symbols(tokens){
  var symbols = tokens
    .map(token => token.exchangeSymbol)
    .filter(exchangeSymbol => exchangeSymbol != null)
  symbols.push('ETH')
  return _.uniq(symbols)
}

function fetchPrices(tokens = token.loadTokens()){
  return call('pricemulti', {
    fsyms: symbols(tokens),
    tsyms: 'USD',
  })
  .catch(() => ({}))
  .then(prices => {
    function getPriceOrNaN(symbol){
      return prices[symbol] == null || typeof(prices[symbol].USD) != 'number'
      ? NaN
      : prices[symbol].USD
    }
    var result = {'ether' : getPriceOrNaN('ETH')}
    tokens.forEach(token => {
      result[token.address] = token.exchangeSymbol == null
        ? NaN
        : getPriceOrNaN(token.exchangeSymbol)
    })
    return result
  })
}

function getExchangeSymbols(){
  var result = []
  for(var symbol in coinList.Data){
    if(coinList.Data.hasOwnProperty(symbol)){
      result.push(symbol)
    }
  }
  // sort, digits last
  return _.sortBy(result, symbol =>
    (/^\d/.test(symbol) ? '1' : '0') + symbol
  )
}

function fetchHistoricalPrices(interval){
  return tokenCatalogWithDetailsPromise().then(tokenCatalog => {
    var tokens = [].concat(token.loadTokens()).concat(tokenCatalog)
    var [method, limit] = {
      week:  ['histohour', 7*24],
      month: ['histoday' , 30  ],
      year:  ['histoday' , 265 ],
    }[interval]
    var symbolToPrices = {}
    return Promise.all(symbols(tokens).map(symbol =>
      call(method, {
        fsym: symbol,
        tsym: 'USD',
        limit,
      }).then(resp => {
        symbolToPrices[symbol] = Array.isArray(resp && resp.Data)
          ? resp.Data
          : null
      })
    ))
    .then(() => {
      var result = {}
      result['ether'] = symbolToPrices['ETH']
      tokens.forEach(t => 
        result[t.address] = t.exchangeSymbol == null
          ? null
          : symbolToPrices[t.exchangeSymbol]
      )
      return result
    })
  })
}

return {
  fetchPrices,
  fetchTokenCatalogWithPrices,
  getExchangeSymbols,
  fetchEtherPrice,
  fetchHistoricalPrices,
}

})
