define(function(require){

var {normalizeAddress} = require('utils')
var coinList = require('data/coinList')
var {TOKEN_CATALOG_URL} = require('config')
var {fetchTokenDetails} = require('token')

function fetchCatalog(){
  return fetch(TOKEN_CATALOG_URL).then(r => r.text())
}

function parse(str){
  var result = []
  var normalized = str
    .replace('\r\n', '\n')
    .replace('\r', '\n')
    .replace(/[^\S\r\n]+/g, ' ')
    .replace(/\s+$/gm, '')
    .replace(/^\s+/gm, '')
  var re = new RegExp('^((0x)?[\\da-fA-F]{40})( (.*))?\n(.*)$(\n(http.*))?', 'gm')
  var match
  while((match = re.exec(normalized)) != null){
    result.push({
      address: normalizeAddress(match[1]),
      exchangeSymbol: match[4],
      name: match[5],
      url: match[7],
    })
  }
  return result
}

function addUrls(tokens){
  return tokens.map(t => {
    if(t.url != null){
      return t
    } else {
      var coin = coinList.Data[t.exchangeSymbol]
      return Object.assign({}, t, {
        url: coin && coin.Url && ('https://cryptocompare.com' + coin.Url)
      })
    }
  })
}

var tokenCatalogPromise = fetchCatalog().then(parse).then(addUrls)

var tokenCatalogWithDetailsPromise = (function(){
  var tokenCatalogWithDetailsPromiseCached = null
  return function(){
    if(tokenCatalogWithDetailsPromiseCached){
      return tokenCatalogWithDetailsPromiseCached
    }
    tokenCatalogWithDetailsPromiseCached = tokenCatalogPromise.then(
      tokenCatalog => {
        /*
         We intentionally don't use web3 batching here, because errors in batch
         are not handled properly by web3. The errors are caused by web3 trying
         to decode output of calling messing contract method (name, symbol,
         etc)
        */
        //startWeb3Batching()
        return Promise.all(tokenCatalog.map(token => 
          fetchTokenDetails(token.address).then(({symbol, decimals}) => {
            var result = Object.assign({}, token, {decimals})
            if(token.exchangeSymbol == null && symbol != null){
              result.exchangeSymbol = symbol
            }
            return result
          })
        ))
      }
    )
    return tokenCatalogWithDetailsPromiseCached
  }
})()

return {tokenCatalogPromise, tokenCatalogWithDetailsPromise}

})
