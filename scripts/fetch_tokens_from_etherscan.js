var xpath = require('xpath');
var parse5 = require('parse5');
var xmlser = require('xmlserializer');
var dom = require('xmldom').DOMParser;
var Promise = require('bluebird');
var request = Promise.promisify(require('request'));
var fs = require('fs');

var URL_BASE = 'http://etherscan.io/';

function getTokens(){
  return request(URL_BASE + 'tokens').then(function(resp){
    var html = resp.body;
    var document = parse5.parse(html);
    var xhtml = xmlser.serializeToString(document);
    var doc = new dom().parseFromString(xhtml);
    var select = xpath.useNamespaces({"x": "http://www.w3.org/1999/xhtml"});
    var name = select("//x:td/x:h5/x:a/text()", doc).map(node => node.data);
    var desciption = select("//x:td/x:small/text()", doc).map(node => node.data);
    var url = select("//x:td/x:h5/x:a/@href", doc).map(node => node.nodeValue);
    var tokens = joinArrs({name, desciption, url});
    return Promise.all(tokens.map(token => 
      request(URL_BASE + token.url).then(resp => 
        // uncomment for complete details
        //Object.assign({}, parseTokenPage(resp.body), token)
        ({name: token.name, address: parseTokenPage(resp.body).address})
      )
    ))
  })
}

function joinArrs(arrs){
  var keys = Object.keys(arrs);
  var first = arrs[keys[0]];
  var result = [];
  for(var i = 0; i < first.length; i++){
    var item = {};
    for(var k in arrs){
      item[k] = arrs[k][i];
    }
    result.push(item);
  }
  return result;
}

function parseTokenPage(html){
  var document = parse5.parse(html);
  var xhtml = xmlser.serializeToString(document);
  var doc = new dom().parseFromString(xhtml);
  var select = xpath.useNamespaces({"x": "http://www.w3.org/1999/xhtml"});
  var totalSupply = select("//x:table[1]//x:tbody//x:tr[1]//x:td[2]/text()", doc).map(node => node.data.trim())[0];
  var tokenHolders = select("//x:table[1]//x:tbody//x:tr[2]//x:td[2]/text()", doc).map(node => node.data.trim())[0];
  var transactions = select("//x:table[1]//x:tbody//x:tr[3]//x:td[2]/text()", doc).map(node => node.data.trim())[0];
  var crowdSale = select("//x:table[1]//x:tbody//x:tr[4]//x:td[2]//text()", doc).map(node => node.data.trim())[0];
  var address = select("//x:table[2]//x:tbody//x:tr[1]//x:td[2]//text()", doc).map(node => node.data.trim()).join('');
  return {totalSupply, tokenHolders, transactions, crowdSale, address: address.toLowerCase()};
}

//console.log(parseTokenPage(fs.readFileSync('./tokenpage.html','utf8')));

getTokens()
  // filter out broken tokens
  .then(tokens => tokens.filter(({address}) => address != ''))
  .then(data => console.log(JSON.stringify(data, null, 2)))
  .catch((e) => {console.error(e); process.exit(1)});
