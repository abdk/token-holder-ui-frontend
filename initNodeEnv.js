global.EventEmitter = require('./lib/EventEmitter.js')
global.EthJS = require('./scripts/ethereumjs.js')
require('node-amd-require')({
  baseUrl: './src',
});
require('core')
global._ = require('./lib/underscore.js')
global.Web3 = require('web3');
global.Wallet = require('./lib/myetherwallet.js');
global.web3 = new Web3();
global.moment = require('./lib/moment');
global.expect = require('chai').expect

global.localStorage = {}
global.sessionStorage = {}

global.log = global.error = function(){
  var args = Array.prototype.map.call(arguments, (a) => {
    if(typeof(a) == 'object'){
      return JSON.stringify(a, null, 2)
    } else {
      return a
    }
  })
  console.log.apply(console, args);
}

var config = require('config')
config.IS_GOOGLE_DRIVE_ENABLED = () => false

var i18n = require('i18n')
i18n.initWithDefaultLang()
