var {properties} = require('./initTestEnv')
var {withMiner} = require('./minerUtils')
var storage = require('storage')
var account = require('account')
var token = require('token')
var transfer = require('transfer');
var initEthNode = require('initEthNode');
var {saveNodeToRecents} = require('ethNode');
var deployContract = require('./deployContract');
var utils = require('utils');
var {promisify} = require('utils/web3');
var miner = require('./miner');

// TODO dump test db on finish

function init(){
  storage.create({isUsingGDrive: false, dbDump: null})
  storage.init()
  saveNodeToRecents({url: properties.nodeUrl, chainId: properties.chainId})
  initEthNode()
  miner(web3)
  return state.nodeInfo.fetchNodeInfo().then(() => {
    if(state.nodeInfo.get().isOnline){
      log('node is online')
    } else {
      throw 'node is offline'
    }
  })
  .then(() => {
    account.importAccounts([{name: 'ledger', keyData: properties.ledgerAccount}])
  })
}

it('transfer ether', () => {
  return init().then(() => {
    var main = account.createAccount()
    var ledgerAddress = account.loadAccounts().find(acc => acc.name == 'ledger').address
    var address = account.saveAccount(main, {name: 'main', password: 'password'})
    return transfer.transferEther({
      from: ledgerAddress,
      to: address, 
      value: web3.toWei(web3.toBigNumber(0.01), 'ether'),
    }, properties.ledgerAccountPassword
  )
  .then(withMiner(() => 
    account.fetchBalance(address)
    .then(balance => {
      expect(balance).to.deep.equal(web3.toWei(web3.toBigNumber(0.01), 'ether'))
    })
  ))
})

})

it('deploy contract', () => {
  var totalSupply = 1000
  var name = 'TEST'
  var decimals = 1
  var symbol = 'TST'
  return init().then(() => {
    return deployContract({
      abiPath: './integration-tests/token-contract-abi.json',
      bytecodePath: './integration-tests/token-contract-code', 
      from: utils.normalizeAddress(properties.ledgerAccount.address),
      args: [
        totalSupply,
        name,
        decimals,
        symbol,
      ],
      password: properties.ledgerAccountPassword,
    })
  })
  .then(result => {
    console.log('tx hash', result.hash)
    return result.hash
  })
  .then(withMiner((hash) =>
    promisify(web3.eth.getTransactionReceipt)(hash)
      .then(receipt => {
        console.log('contract creation receipt', receipt)
        return token.checkIsTokenValidAndFetchDetails(receipt.contractAddress)
      })
      .then(({token, isValid}) => {
        console.log('token details', token)
        expect(isValid).to.equal(true)
        expect(token.totalSupply).to.deep.equal(web3.toBigNumber(totalSupply))
        expect(token.decimals).to.equal(decimals)
        expect(token.name).to.equal(name)
      })
  ))
})
