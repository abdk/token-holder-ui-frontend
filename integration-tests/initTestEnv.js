require('../initNodeEnv.js')

var fs = require('fs')
var utils = require('utils')

var sqlite3 = require('sqlite3')
var indexeddbjs = require('indexeddb-js')
var engine  = new sqlite3.Database(':memory:')
global.indexedDB = indexeddbjs.makeScope('sqlite3', engine).indexedDB

var properties = eval('(' + fs.readFileSync('integration-tests/.properties','utf8') + ')')

module.exports = {properties}
