var fs = require('fs')
var utils = require('utils')
var {sendTransaction} = require('transfer')

function stripTrailingNewLine(str){
  if(str[str.length - 1] == '\n'){
    str = str.substring(0, str.length - 1)
  }
  return str
}

module.exports = function({
  abiPath, 
  bytecodePath, 
  from, 
  args,
  password,
}){
  var abi = JSON.parse(fs.readFileSync(abiPath, 'utf-8'))
  var bytecode = stripTrailingNewLine(fs.readFileSync(bytecodePath, 'utf-8'))
  var contract = web3.eth.contract(abi)
  var data = contract.new.getData(...args,
    {
      data: '0x' + bytecode, 
      from,
    }
  )
  return sendTransaction({from, data, 
    // hardcode instead of estimate. If estimate then exceed block gas limit. TODO
    gasLimit: 3000000
  }, password)
}
