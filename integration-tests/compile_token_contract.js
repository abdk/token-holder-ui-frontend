var fs = require('fs')
var solc = require('solc')

let source = fs.readFileSync('./integration-tests/token-contract.sol', 'utf8');
let compiledContract = solc.compile(source, 1);
var contract = compiledContract.contracts[':MyToken']
var abi = JSON.parse(contract.interface)
var code = contract.bytecode
fs.writeFileSync(
  './integration-tests/token-contract-abi.json', 
  JSON.stringify(abi, null, 2), 
  'utf8'
)
fs.writeFileSync('./integration-tests/token-contract-code', code, 'utf8')
