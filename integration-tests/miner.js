module.exports = function(web3){

// see https://github.com/ethereum/mist/blob/master/modules/web3Admin.js

function insertMethod(name, call, params, inputFormatter, outputFormatter) {
  return new web3._extend.Method({ name, call, params, inputFormatter, outputFormatter });
}

function insertProperty(name, getter, outputFormatter) {
  return new web3._extend.Property({ name, getter, outputFormatter });
}

web3._extend({
  property: 'miner',
  methods:
  [
    insertMethod('start', 'miner_start', 1, [web3._extend.formatters.formatInputInt], web3._extend.formatters.formatOutputBool),
    insertMethod('stop', 'miner_stop', 1, [web3._extend.formatters.formatInputInt], web3._extend.formatters.formatOutputBool),
    insertMethod('setExtra', 'miner_setExtra', 1, [null], web3._extend.formatters.formatOutputBool),
    insertMethod('setGasPrice', 'miner_setGasPrice', 1, [web3._extend.utils.fromDecimal], web3._extend.formatters.formatOutputBool),
    insertMethod('startAutoDAG', 'miner_startAutoDAG', 0, [], web3._extend.formatters.formatOutputBool),
    insertMethod('stopAutoDAG', 'miner_stopAutoDAG', 0, [], web3._extend.formatters.formatOutputBool),
    insertMethod('makeDAG', 'miner_makeDAG', 1, [web3._extend.formatters.inputDefaultBlockNumberFormatter], web3._extend.formatters.formatOutputBool),
  ],
  properties: [
    insertProperty('hashrate', 'miner_hashrate', web3._extend.utils.toDecimal),
  ],
});

}
