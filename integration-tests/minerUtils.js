var {promisify} = require('utils/web3');

function stopMiner(){
  console.log('stopping miner')
  return promisify(web3.miner.stop)()
    .then(() => console.log('miner stopped'))
    .catch(e => {
      console.log('Could not stop miner', e)
      throw e
    })
}

function startMiner(){
  console.log('starting miner')
  process.on('SIGINT', stopMiner)
  process.on('uncaughtException', stopMiner)
  return promisify(web3.miner.start)()
    .catch(e => {
      console.log('Could not start miner', e)
      throw e
    })
    .then(() => console.log('miner started'))
}

function waitForBlockMined(){
  console.log('start to wait for block mined')
  return new Promise((resolve, reject) => {
    promisify(web3.eth.getBlock)('latest').then(currentBlock => {
      console.log('current block is ' + currentBlock.number)
      var intervalId = setInterval(() => 
        promisify(web3.eth.getBlock)('latest').then(block => {
          if(block.number > currentBlock.number){
            clearInterval(intervalId)
            console.log('new block mined with number', block.number)
            resolve()
          }
        })
      , 1000)
    })
  })
}

function withMiner(fn){
  return function(...args){
    return startMiner()
      .then(waitForBlockMined)
      .then(() => fn(...args))
      .then(stopMiner)
      .catch(e => {
        stopMiner()
        throw e
      })
  }
}

module.exports = {withMiner, startMiner, stopMiner, waitForBlockMined}
